use ax2five::{Ax25Frame, FrameContent};

#[allow(dead_code)]
pub enum Chaos {
    Initial(usize),
    Chance(f64),
    Matches(String, usize),
}

impl Default for Chaos {
    fn default() -> Self {
        Self::Initial(0)
    }
}

impl Chaos {
    fn apply(&mut self, frame: &Ax25Frame) -> Option<AppliedChaos> {
        match self {
            Chaos::Initial(ref mut i) => {
                if *i > 0 {
                    *i -= 1;
                    Some(AppliedChaos::Drop)
                } else {
                    None
                }
            }
            Chaos::Chance(chance) => {
                if rand::random::<f64>() < *chance {
                    Some(AppliedChaos::Drop)
                } else {
                    None
                }
            }
            Chaos::Matches(text, ref mut i) => match &frame.content {
                FrameContent::Information(c) => {
                    if *i > 0 {
                        let against = String::from_utf8_lossy(&c.info);
                        if against.contains(text.as_str()) {
                            *i -= 1;
                            Some(AppliedChaos::Drop)
                        } else {
                            None
                        }
                    } else {
                        None
                    }
                }
                _ => None,
            },
        }
    }
}

#[derive(Default)]
pub struct ChaosMonkey {
    pub sabm: Chaos,
    pub ua: Chaos,
    pub information: Chaos,
}

#[derive(Debug)]
pub enum AppliedChaos {
    Drop,
}

impl ChaosMonkey {
    pub fn apply(&mut self, frame: &Ax25Frame) -> Option<AppliedChaos> {
        match frame.content {
            FrameContent::Information(_) => self.information.apply(frame),
            FrameContent::SetAsynchronousBalancedMode(_) => self.sabm.apply(frame),
            FrameContent::SetAsynchronousBalancedModeExtended(_) => self.sabm.apply(frame),
            FrameContent::UnnumberedAcknowledge(_) => self.ua.apply(frame),
            _ => None,
        }
    }
}
