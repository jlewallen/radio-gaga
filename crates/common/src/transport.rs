use thiserror::Error;
use tokio::net::TcpStream;
use tokio_util::codec::Framed;

use agw::{AgwCodec, AgwError, AgwProtocol};
use ax2five::{Ax25Frame, TncError, TncTransport, Transport};

#[derive(Default)]
pub struct TransportOptions {
    agw: Option<String>,
    tnc: Option<String>,
}

impl TransportOptions {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn agw(self, agw: Option<String>) -> Self {
        Self { agw, ..self }
    }

    pub fn tnc(self, tnc: Option<String>) -> Self {
        Self { tnc, ..self }
    }

    pub async fn build(&self) -> Result<CommonTransport, Error> {
        match (self.agw.as_ref(), self.tnc.as_ref()) {
            (Some(agw), None) => {
                let mut agw =
                    AgwProtocol::new_framed(TcpStream::connect(&agw).await.map_err(AgwError::Io)?);
                agw.enable_raw().await.map_err(AgwError::Io)?;
                agw.enable_monitoring().await.map_err(AgwError::Io)?;
                agw.ask_ports().await.map_err(AgwError::Io)?;
                Ok(CommonTransport::Agw(agw))
            }
            (None, Some(tnc)) => {
                let tnc = TncTransport::new(tnc).await?;
                Ok(CommonTransport::Tnc(tnc))
            }
            (Some(_), Some(_)) => Err(Error::AmbiguousTransport),
            (None, None) => Err(Error::MissingTransport),
        }
    }
}

pub enum CommonTransport {
    Agw(AgwProtocol<Framed<TcpStream, AgwCodec>>),
    Tnc(TncTransport),
}

impl Transport<ax2five::Ax25Frame> for CommonTransport {
    type Error = Error;

    async fn send(&mut self, frame: ax2five::Ax25Frame) -> Result<(), Self::Error> {
        match self {
            CommonTransport::Agw(t) => Ok(t.send(frame).await?),
            CommonTransport::Tnc(t) => Ok(t.send(frame).await?),
        }
    }

    async fn receive(&mut self) -> Result<Option<Ax25Frame>, Self::Error> {
        match self {
            CommonTransport::Agw(t) => {
                Ok(<AgwProtocol<Framed<TcpStream, AgwCodec>> as ax2five::Transport<Ax25Frame>>::receive(t).await?)
            }
            CommonTransport::Tnc(t) => Ok(t.receive().await?),
        }
    }
}

#[derive(Error, Debug)]
pub enum Error {
    #[error("agw error")]
    Agw(#[from] AgwError),
    #[error("tnc error")]
    Tnc(#[from] TncError),
    #[error("ambiguous transport configuration")]
    AmbiguousTransport,
    #[error("missing transport configuration")]
    MissingTransport,
}
