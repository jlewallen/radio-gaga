use anyhow::Result;
use std::{collections::HashMap, net::SocketAddr, sync::Arc};
use tokio::sync::{mpsc::Sender, RwLock};
use tracing::*;

mod chaos;

use agw::{Packet, PacketKind};
use chaos::{AppliedChaos, Chaos, ChaosMonkey};

#[derive(Debug, Clone, PartialEq)]
enum ClientApplication {
    Unknown,
    App,
    Bpq,
}

pub struct Client {
    sender: Sender<Packet>,
    app: ClientApplication,
}

#[derive(Clone, Default)]
pub struct Hub {
    loopback: bool,
    chaos: Arc<RwLock<ChaosMonkey>>,
    clients: Arc<RwLock<HashMap<String, Client>>>,
}

impl Hub {
    pub async fn register(&self, addr: &SocketAddr, sender: Sender<Packet>) {
        let mut clients = self.clients.write().await;
        let key = format!("{:?}", addr);
        clients.insert(
            key,
            Client {
                sender,
                app: ClientApplication::Unknown,
            },
        );
    }

    pub async fn unregister(&self, addr: &SocketAddr) {
        let mut clients = self.clients.write().await;
        let key = format!("{:?}", addr);
        clients.remove(&key);
    }

    pub async fn publish(&self, addr: &SocketAddr, packet: &Packet) -> Result<()> {
        let clients = self.clients.write().await;
        let key = format!("{:?}", addr);
        let from = clients.get(&key).unwrap();
        let to = match from.app {
            ClientApplication::Unknown => ClientApplication::Unknown,
            ClientApplication::App => ClientApplication::Bpq,
            ClientApplication::Bpq => ClientApplication::App,
        };
        for (_, client) in clients.iter() {
            if client.app == to {
                client.sender.send(packet.clone()).await?;
            }
        }

        Ok(())
    }

    pub async fn route(&self, remote: &SocketAddr, received: Packet) -> Result<Vec<Packet>> {
        let client_app = {
            let clients = self.clients.write().await;
            let key = format!("{:?}", remote);
            clients.get(&key).unwrap().app.clone()
        };
        let ax25 = received.get_ax25()?;
        let applied_chaos = {
            let mut chaos = self.chaos.write().await;
            ax25.as_ref().and_then(|f| chaos.apply(f))
        };
        info!("{:?} {:?} {:?}", client_app, applied_chaos, received);

        if let Some(AppliedChaos::Drop) = applied_chaos {
            return Ok(vec![]);
        }

        match received.kind() {
            PacketKind::Login => {
                info!("{} application", &remote);
                self.mark_application(remote).await;
                Ok(vec![])
            }
            PacketKind::PortInformation => {
                info!("{} application", &remote);
                self.mark_application(remote).await;

                Ok(vec![Packet::new_ports()])
            }
            PacketKind::DataRaw => match self.publish(remote, &received).await {
                Ok(_) => {
                    if self.loopback {
                        Ok(vec![received.clone().to_application()])
                    } else if let Some(own_information) =
                        Packet::new_arbitrary_own_information(&received)
                    {
                        Ok(vec![own_information])
                    } else {
                        Ok(vec![])
                    }
                }
                Err(e) => {
                    warn!("{:?}", e);
                    Ok(vec![])
                }
            },
            _ => Ok(vec![]),
        }
    }

    pub async fn reset_chaos(&self) {
        let mut chaos = self.chaos.write().await;
        *chaos = ChaosMonkey {
            sabm: Chaos::default(),
            ua: Chaos::default(),
            information: Chaos::default(),
        };
    }

    async fn mark_application(&self, addr: &SocketAddr) {
        let mut clients = self.clients.write().await;
        let key = format!("{:?}", addr);
        clients.get_mut(&key).unwrap().app = ClientApplication::App;
        for (_, client) in clients.iter_mut() {
            if client.app == ClientApplication::Unknown {
                client.app = ClientApplication::Bpq;
            }
        }
    }
}
