use std::string::FromUtf8Error;
use thiserror::Error;

use super::Callsign;

pub const NODES_CALL: &str = "NODES";
pub const BEACON_CALL: &str = "BEACON";
pub const ID_CALL: &str = "ID";

#[derive(Debug)]
pub struct Nodes {
    pub alias: Callsign,
    pub entries: Vec<NodeEntry>,
}

#[derive(Debug)]
pub struct NodeEntry {
    pub call: Callsign,
    pub alias: Callsign,
    pub neighbor: Callsign,
    pub quality: u8,
}

impl NodeEntry {
    pub fn from_bytes(buf: &[u8]) -> Result<NodeEntry, FromUtf8Error> {
        let (call, buf) = buf.split_at(7);
        let call = Callsign::from_ax25_bytes(call)?;
        let (alias, buf) = buf.split_at(6);
        let alias = Callsign::from_bytes(alias);
        let (neighbor, buf) = buf.split_at(7);
        let neighbor = Callsign::from_ax25_bytes(neighbor)?;
        let quality = buf[0];

        Ok(NodeEntry {
            call,
            alias,
            neighbor,
            quality,
        })
    }
}

impl Nodes {
    pub fn from_bytes(buf: &[u8]) -> Result<Nodes, DecodeError> {
        let (first, buf) = buf.split_at(1);
        if first[0] != 0xff {
            return Err(DecodeError::UnexpectedData);
        }

        let (alias, buf) = buf.split_at(6);
        let alias = Callsign::from_bytes(alias);

        let entries = buf
            .chunks(21)
            .map(NodeEntry::from_bytes)
            .collect::<Result<Vec<_>, _>>()?;

        Ok(Nodes { alias, entries })
    }
}

#[derive(Debug)]
pub struct ConnectRequest {
    pub index: u8,
    pub id: u8,
    pub window_size: u8,
    pub flags: Flags,
    pub source: Callsign,
    pub destination: Callsign,
}

impl ConnectRequest {
    fn from_bytes(buf: &[u8]) -> Result<Self, DecodeError> {
        let (header, buf) = buf.split_at(6);
        let index = header[0];
        let id = header[1];
        let flags = Flags::from_byte(header[4]);
        let window_size = header[5];
        let (source, buf) = buf.split_at(7);
        let source = Callsign::from_ax25_bytes(source)?;
        let (destination, _buf) = buf.split_at(7);
        let destination = Callsign::from_ax25_bytes(destination)?;
        Ok(Self {
            index,
            id,
            window_size,
            flags,
            source,
            destination,
        })
    }
}

#[derive(Debug)]
pub struct ConnectAcknowledge {
    pub your_index: u8,
    pub your_id: u8,
    pub my_index: u8,
    pub my_id: u8,
    pub window_size: u8,
    pub flags: Flags,
}

impl ConnectAcknowledge {
    pub fn from_bytes(buf: &[u8]) -> Result<Self, DecodeError> {
        let your_index = buf[0];
        let your_id = buf[1];
        let my_index = buf[2];
        let my_id = buf[3];
        let flags = Flags::from_byte(buf[4]);
        let window_size = buf[5];
        Ok(Self {
            flags,
            your_index,
            your_id,
            my_index,
            my_id,
            window_size,
        })
    }
}

#[derive(Debug)]
pub struct DisconnectRequest {
    pub your_index: u8,
    pub your_id: u8,
    pub flags: Flags,
}

impl DisconnectRequest {
    pub fn from_bytes(buf: &[u8]) -> Result<Self, DecodeError> {
        let your_index = buf[0];
        let your_id = buf[1];
        let flags = Flags::from_byte(buf[4]);
        Ok(Self {
            your_index,
            your_id,
            flags,
        })
    }
}

#[derive(Debug)]
pub struct DisconnectAcknowledge {
    pub flags: Flags,
    pub your_index: u8,
    pub your_id: u8,
}

impl DisconnectAcknowledge {
    pub fn from_bytes(buf: &[u8]) -> Result<Self, DecodeError> {
        let your_index = buf[0];
        let your_id = buf[1];
        let flags = Flags::from_byte(buf[4]);
        Ok(Self {
            your_index,
            your_id,
            flags,
        })
    }
}

#[derive(Debug)]
pub struct Information {
    pub your_index: u8,
    pub your_id: u8,
    pub tx: u8,
    pub rx: u8,
    pub flags: Flags,
    pub info: Vec<u8>,
}

impl Information {
    pub fn from_bytes(buf: &[u8]) -> Result<Self, DecodeError> {
        let (header, buf) = buf.split_at(5);
        let your_index = header[0];
        let your_id = header[1];
        let tx = header[2];
        let rx = header[3];
        let flags = Flags::from_byte(header[4]);
        Ok(Self {
            your_index,
            your_id,
            tx,
            rx,
            flags,
            info: buf.to_owned(),
        })
    }
}

#[derive(Debug)]
pub struct InformationAcknowledge {
    pub your_index: u8,
    pub your_id: u8,
    pub rx: u8,
    pub flags: Flags,
}

impl InformationAcknowledge {
    pub fn from_bytes(buf: &[u8]) -> Result<Self, DecodeError> {
        let your_index = buf[0];
        let your_id = buf[1];
        let rx = buf[3];
        let flags = Flags::from_byte(buf[4]);
        Ok(Self {
            your_index,
            your_id,
            rx,
            flags,
        })
    }
}

#[derive(Debug)]
pub enum TransportHeader {
    ConnectRequest(ConnectRequest),
    ConnectAcknowledge(ConnectAcknowledge),
    DisconnectRequest(DisconnectRequest),
    DisconnectAcknowledge(DisconnectAcknowledge),
    Information(Information),
    InformationAcknowledge(InformationAcknowledge),
}

#[derive(Debug)]
pub struct Flags {
    pub choke: bool,
    pub nak: bool,
    pub more: bool,
}

impl Flags {
    pub fn from_byte(buf: u8) -> Flags {
        let choke = buf & 0b1000_0000 > 0;
        let nak = buf & 0b0100_0000 > 0;
        let more = buf & 0b0010_0000 > 0;
        Self { choke, nak, more }
    }
}

impl TransportHeader {
    pub fn from_bytes(buf: &[u8]) -> Result<TransportHeader, DecodeError> {
        let opcode_and_flags = buf[4];
        match opcode_and_flags & 0b1111 {
            1 => Ok(Self::ConnectRequest(ConnectRequest::from_bytes(buf)?)),
            2 => Ok(Self::ConnectAcknowledge(ConnectAcknowledge::from_bytes(
                buf,
            )?)),
            3 => Ok(Self::DisconnectRequest(DisconnectRequest::from_bytes(buf)?)),
            4 => Ok(Self::DisconnectAcknowledge(
                DisconnectAcknowledge::from_bytes(buf)?,
            )),
            5 => Ok(Self::Information(Information::from_bytes(buf)?)),
            6 => Ok(Self::InformationAcknowledge(
                InformationAcknowledge::from_bytes(buf)?,
            )),
            _ => panic!("unknown NET/ROM opcode"),
        }
    }
}

#[derive(Debug)]
pub struct NetRom {
    pub source: Callsign,
    pub destination: Callsign,
    pub ttl: u8,
    pub transport: TransportHeader,
}

impl NetRom {
    pub fn from_bytes(buf: &[u8]) -> Result<NetRom, DecodeError> {
        let (source, buf) = buf.split_at(7);
        let source = Callsign::from_ax25_bytes(source)?;
        let (destination, buf) = buf.split_at(7);
        let destination = Callsign::from_ax25_bytes(destination)?;
        let (ttl, transport) = buf.split_at(1);
        let ttl = ttl[0];
        let transport = TransportHeader::from_bytes(transport)?;

        Ok(Self {
            source,
            destination,
            ttl,
            transport,
        })
    }
}

#[derive(Error, Debug)]
pub enum DecodeError {
    #[error("unexpected data")]
    UnexpectedData,
    #[error("utf8 error")]
    Utf8Error(#[from] FromUtf8Error),
}
