use std::{
    pin::Pin,
    sync::{Arc, Mutex},
};
use thiserror::Error;
use tokio::{
    io::{AsyncRead, AsyncWrite},
    net::tcp::{OwnedReadHalf, OwnedWriteHalf},
};
use tracing::*;

use ax25::frame::{Ax25Frame, FrameParseError};
use kiss_tnc::Tnc;
use tokio_serial::{SerialPortBuilderExt, SerialStream};

use crate::Transport;

pub enum TncTransport {
    Network(Tnc<OwnedReadHalf, OwnedWriteHalf>),
    Serial(Tnc<OwnedReadSerial, OwnedWriteSerial>),
}

impl TncTransport {
    pub async fn new(uri: &str) -> Result<TncTransport, TncError> {
        match uri.split_once(':') {
            Some(("tcpkiss", address)) => {
                let tnc = Tnc::connect_tcp(address).await?;
                Ok(TncTransport::Network(tnc))
            }
            Some(("serial", path)) => {
                let tnc = new_serial_tnc(path, 57_600).await?;
                Ok(TncTransport::Serial(tnc))
            }
            _ => Err(TncError::UriParseError(uri.to_owned())),
        }
    }
}

impl<R: AsyncRead + Unpin, W: AsyncWrite + Unpin> Transport<Ax25Frame> for Tnc<R, W> {
    type Error = TncError;

    async fn send(&mut self, frame: Ax25Frame) -> Result<(), Self::Error> {
        let bytes = frame.to_bytes();
        self.send_frame(&bytes).await?;
        self.flush().await?;
        Ok(())
    }

    async fn receive(&mut self) -> Result<Option<Ax25Frame>, Self::Error> {
        let (_, bytes) = self.read_frame().await?;
        Ok(Some(Ax25Frame::from_bytes(&bytes)?))
    }
}

#[derive(Error, Debug)]
pub enum TncError {
    #[error("invalid uri: {0}")]
    UriParseError(String),
    #[error("io error")]
    Io(#[from] std::io::Error),
    #[error("serial error")]
    SerialError(#[from] tokio_serial::Error),
    #[error("read error")]
    Read(#[from] kiss_tnc::errors::ReadError),
    #[error("frame error")]
    FrameParse(#[from] FrameParseError),
}

async fn new_serial_tnc(
    path: &str,
    baud: u32,
) -> Result<Tnc<OwnedReadSerial, OwnedWriteSerial>, tokio_serial::Error> {
    let stream = Arc::new(Mutex::new(
        tokio_serial::new(path, baud).open_native_async()?,
    ));
    let read = OwnedReadSerial {
        stream: Arc::clone(&stream),
    };
    let write = OwnedWriteSerial { stream };

    Ok(Tnc::new(read, write))
}

pub struct OwnedReadSerial {
    stream: Arc<Mutex<SerialStream>>,
}

impl AsyncRead for OwnedReadSerial {
    fn poll_read(
        self: Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
        buf: &mut tokio::io::ReadBuf<'_>,
    ) -> std::task::Poll<std::io::Result<()>> {
        Pin::new(
            &mut *self
                .stream
                .lock()
                .expect("Serial AsyncWrite mutex lock error"),
        )
        .poll_read(cx, buf)
    }
}

pub struct OwnedWriteSerial {
    stream: Arc<Mutex<SerialStream>>,
}

impl AsyncWrite for OwnedWriteSerial {
    fn poll_write(
        self: Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
        buf: &[u8],
    ) -> std::task::Poll<Result<usize, std::io::Error>> {
        Pin::new(
            &mut *self
                .stream
                .lock()
                .expect("Serial AsyncWrite mutex lock error"),
        )
        .poll_write(cx, buf)
    }

    fn poll_flush(
        self: Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<Result<(), std::io::Error>> {
        Pin::new(
            &mut *self
                .stream
                .lock()
                .expect("Serial AsyncWrite mutex lock error"),
        )
        .poll_flush(cx)
    }

    fn poll_shutdown(
        self: Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<Result<(), std::io::Error>> {
        Pin::new(
            &mut *self
                .stream
                .lock()
                .expect("Serial AsyncWrite mutex lock error"),
        )
        .poll_shutdown(cx)
    }
}

impl Transport<Ax25Frame> for TncTransport {
    type Error = TncError;

    async fn send(&mut self, frame: Ax25Frame) -> Result<(), Self::Error> {
        match self {
            TncTransport::Network(t) => Ok(t.send(frame).await?),
            TncTransport::Serial(t) => Ok(t.send(frame).await?),
        }
    }

    async fn receive(&mut self) -> Result<Option<Ax25Frame>, Self::Error> {
        match self {
            TncTransport::Network(ref mut t) => Ok(t.receive().await?),
            TncTransport::Serial(ref mut t) => Ok(t.receive().await?),
        }
    }
}
