use ax25::frame::{Ax25Frame, FrameContent};

pub struct Pretty<'a, T> {
    value: &'a T,
}

impl<'a, T> Pretty<'a, T> {
    pub fn new(value: &'a T) -> Self {
        Self { value }
    }
}

impl std::fmt::Debug for Pretty<'_, Ax25Frame> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str("Ax25<")?;
        f.write_fmt(format_args!("{}", self.value.source))?;
        f.write_str(" - ")?;
        f.write_fmt(format_args!("{}", self.value.destination))?;
        f.write_str(match self.value.command_or_response.as_ref() {
            Some(ax25::frame::CommandResponse::Command) => " C ",
            Some(ax25::frame::CommandResponse::Response) => " R ",
            None => " ",
        })?;
        f.write_fmt(format_args!("{:?}", Pretty::new(&self.value.content)))?;
        f.write_str(">")
    }
}

impl std::fmt::Debug for Pretty<'_, FrameContent> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match &self.value {
            FrameContent::Information(c) => f.write_fmt(format_args!(
                "I<{:?} {} bytes r={} s={} poll={}>",
                c.pid,
                c.info.len(),
                c.receive_sequence,
                c.send_sequence,
                c.poll,
            )),
            FrameContent::ReceiveReady(c) => f.write_fmt(format_args!(
                "RR<r={} poll={}>",
                c.receive_sequence, c.poll_or_final
            )),
            FrameContent::ReceiveNotReady(c) => f.write_fmt(format_args!(
                "RnR<r={} poll={}>",
                c.receive_sequence, c.poll_or_final
            )),
            FrameContent::Reject(c) => f.write_fmt(format_args!(
                "REJ<r={} poll={}>",
                c.receive_sequence, c.poll_or_final
            )),
            FrameContent::SelectiveReject(c) => f.write_fmt(format_args!(
                "SREJ<r={} poll={}>",
                c.receive_sequence, c.poll_or_final
            )),
            FrameContent::SetAsynchronousBalancedMode(c) => {
                f.write_fmt(format_args!("SABM<poll={}>", c.poll))
            }
            FrameContent::SetAsynchronousBalancedModeExtended(c) => {
                f.write_fmt(format_args!("SABME<poll={}>", c.poll))
            }
            FrameContent::Disconnect(c) => f.write_fmt(format_args!("DISC<poll={}>", c.poll)),
            FrameContent::DisconnectedMode(c) => {
                f.write_fmt(format_args!("DiscMode<final={}>", c.final_bit))
            }
            FrameContent::UnnumberedAcknowledge(c) => {
                f.write_fmt(format_args!("UA<final={}>", c.final_bit))
            }
            FrameContent::FrameReject(c) => f.write_fmt(format_args!(
                "FRej<final={} rej_cfr={} z={} y={} x={} w={} r={} s={} cr={:?}>",
                c.final_bit,
                c.rejected_control_field_raw,
                c.z,
                c.y,
                c.x,
                c.w,
                c.receive_sequence,
                c.send_sequence,
                c.command_response
            )),
            FrameContent::UnnumberedInformation(c) => {
                f.write_fmt(format_args!("UI<{:?} {} bytes>", c.pid, c.info.len()))
            }
            FrameContent::UnknownContent(c) => {
                f.write_fmt(format_args!("Unknown<{} bytes>", c.raw.len()))
            }
            FrameContent::ExchangeIdentification(i) => f.write_fmt(format_args!(
                "Xid<p={:?} duplex={:?} {:?}, {:?} rx={:?} win={:?}, ack={:?}, retries={:?}>",
                i.poll_or_final,
                i.full_duplex,
                i.srej_mode,
                i.modulo,
                i.i_field_length_rx,
                i.window_size_rx,
                i.ack_timer,
                i.retries
            )),
            FrameContent::Test(c) => f.write_fmt(format_args!("Test<{:?} bytes>", c.info.len())),
        }
    }
}
