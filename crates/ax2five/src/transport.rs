#[allow(async_fn_in_trait)]
pub trait Transport<T> {
    type Error;

    async fn send(&mut self, frame: T) -> Result<(), Self::Error>;
    async fn receive(&mut self) -> Result<Option<T>, Self::Error>;
}
