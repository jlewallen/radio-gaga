use std::string::FromUtf8Error;

use ax25::frame::Address;

#[derive(Default, PartialEq, Eq, PartialOrd, Ord, Hash, Clone)]
pub struct Callsign(String);

impl Callsign {
    pub fn from_bytes(data: &[u8]) -> Callsign {
        let nul = data.iter().position(|&c| c == b'\0').unwrap_or(data.len());
        Self(String::from_utf8(data[0..nul].to_owned()).unwrap_or_default())
    }

    pub fn from_ax25_bytes(bytes: &[u8]) -> Result<Callsign, FromUtf8Error> {
        // I have no idea why the ax25 crate keeps this private.
        let mut dest_utf8: Vec<u8> = bytes[0..6]
            .iter()
            .rev()
            .map(|&c| c >> 1)
            .skip_while(|&c| c == b' ')
            .collect::<Vec<u8>>();
        dest_utf8.reverse();
        Ok(Self(format!(
            "{}-{}",
            String::from_utf8(dest_utf8)?,
            (bytes[6] >> 1) & 0x0f
        )))
    }

    pub fn to_field(&self) -> [u8; 10] {
        let mut field: [u8; 10] = Default::default();
        let bytes = self.0.as_bytes();
        let end = std::cmp::min(10, bytes.len());
        field[0..end].copy_from_slice(&bytes[0..end]);
        field
    }

    pub fn none_if_empty(&self) -> Option<&Callsign> {
        if self.0.is_empty() {
            None
        } else {
            Some(self)
        }
    }

    pub fn as_str(&self) -> &str {
        self.0.as_str()
    }

    pub fn with_ssid(&self) -> String {
        if self.as_str().contains('-') {
            self.as_str().to_owned()
        } else {
            self.as_str().to_owned() + "-0"
        }
    }
}

impl From<Address> for Callsign {
    fn from(value: Address) -> Self {
        Self(value.to_string())
    }
}

impl From<String> for Callsign {
    fn from(value: String) -> Self {
        Self(value)
    }
}

impl From<&str> for Callsign {
    fn from(value: &str) -> Self {
        Self(value.to_owned())
    }
}

impl std::fmt::Debug for Callsign {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_tuple("Callsign").field(&self.0).finish()
    }
}

impl std::fmt::Display for Callsign {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(&self.0)
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub struct Pair(Callsign, Callsign);

impl Pair {
    pub fn new(a: impl Into<Callsign>, b: impl Into<Callsign>) -> Self {
        Self(a.into(), b.into())
    }

    pub fn split(self) -> (Callsign, Callsign) {
        (self.0, self.1)
    }
}
