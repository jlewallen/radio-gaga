pub use super::*;

#[derive(Default, SpecCase)]
struct Case86_4 {}

impl SpecCase for Case86_4 {
    fn before(&self) -> State {
        State::AwaitingConnection(Inner::default().queued(test_info(8)).into())
    }

    fn signal(&self) -> Received {
        Received::Primitive(In::ConnectRequest)
    }

    fn after(&self, before: Inner) -> State {
        State::AwaitingConnection(before.discard_frame_queue().layer_3_initiated(true).into())
    }
}

#[derive(Default, SpecCase)]
struct Case86_6 {}

impl SpecCase for Case86_6 {
    fn before(&self) -> State {
        State::AwaitingConnection(Default::default())
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::SetAsynchronousBalancedMode(
            SetAsynchronousBalancedMode { p_or_f: true },
        ))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Frame(Frame::UnnumberedAcknowledge(
            UnnumberedAcknowledge { is_final: true },
        ))]
    }

    fn after(&self, _before: Inner) -> State {
        State::AwaitingConnection(AwaitingConnection::default())
    }
}

#[derive(Default, SpecCase)]
struct Case86_6Version22Downgrade {}

impl SpecCase for Case86_6Version22Downgrade {
    fn before(&self) -> State {
        State::AwaitingConnection(Inner::default().version(Version::Ax22).into())
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::SetAsynchronousBalancedMode(
            SetAsynchronousBalancedMode { p_or_f: true },
        ))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Frame(Frame::UnnumberedAcknowledge(
            UnnumberedAcknowledge { is_final: true },
        ))]
    }

    fn after(&self, _before: Inner) -> State {
        State::AwaitingConnection(AwaitingConnection::default())
    }
}

#[derive(Default, SpecCase)]
struct Case86_7 {}

impl SpecCase for Case86_7 {
    fn before(&self) -> State {
        State::AwaitingConnection(Default::default())
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::Disconnect(Disconnect { poll: true }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Frame(Frame::DisconnectedMode(
            DisconnectedMode { is_final: true },
        ))]
    }

    fn after(&self, _before: Inner) -> State {
        State::AwaitingConnection(AwaitingConnection::default())
    }
}
