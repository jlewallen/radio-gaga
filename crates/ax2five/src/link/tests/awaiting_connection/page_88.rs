pub use super::*;

#[derive(Default, SpecCase)]
struct Case88_1N {}

impl SpecCase for Case88_1N {
    fn before(&self) -> State {
        State::AwaitingConnection(Default::default())
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::DisconnectedMode(DisconnectedMode {
            is_final: false,
        }))
    }

    fn after(&self, _before: Inner) -> State {
        State::AwaitingConnection(Inner::default().into())
    }
}

#[derive(Default, SpecCase)]
struct Case88_1Y {}

impl SpecCase for Case88_1Y {
    fn before(&self) -> State {
        State::AwaitingConnection(
            Inner {
                queue: vec![test_info(8)].into(),
                timers: Timers {
                    timer_1: Timer::Started(500.into()),
                    ..Default::default()
                },
                ..Default::default()
            }
            .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::DisconnectedMode(DisconnectedMode { is_final: true }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Primitive(Out::DisconnectIndication)]
    }

    fn after(&self, _before: Inner) -> State {
        State::Disconnected(Inner::default().into())
    }
}

#[derive(Default, SpecCase)]
struct Case88_2N {}

impl SpecCase for Case88_2N {
    fn before(&self) -> State {
        State::AwaitingConnection(Default::default())
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::UnnumberedAcknowledge(UnnumberedAcknowledge {
            is_final: false,
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Primitive(Out::ErrorIndication(ErrorCode::D))]
    }

    fn after(&self, _before: Inner) -> State {
        State::AwaitingConnection(Inner::default().into())
    }
}

#[derive(Default, SpecCase)]
struct Case88_2YNY {}

impl SpecCase for Case88_2YNY {
    fn before(&self) -> State {
        State::AwaitingConnection(
            Inner {
                flags: Flags {
                    layer_3_initiated: false, // N
                    ..Default::default()
                },
                sequences: Sequences {
                    vs: Seq::new_modulo_8(2), // Y
                    va: Seq::new_modulo_8(2),
                    ..Default::default()
                },
                ..Default::default()
            }
            .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::UnnumberedAcknowledge(UnnumberedAcknowledge {
            is_final: true, // Y
        }))
    }

    fn after(&self, _before: Inner) -> State {
        State::Connected(
            Inner {
                flags: Flags {
                    ..Default::default()
                },
                ..Default::default()
            }
            .into(),
        )
    }
}

#[derive(Default, SpecCase)]
struct Case88_2YY {}

impl SpecCase for Case88_2YY {
    fn before(&self) -> State {
        State::AwaitingConnection(
            Inner {
                flags: Flags {
                    layer_3_initiated: true,
                    ..Default::default()
                },
                ..Default::default()
            }
            .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::UnnumberedAcknowledge(UnnumberedAcknowledge {
            is_final: true,
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Primitive(Out::ConnectConfirm)]
    }

    fn after(&self, _before: Inner) -> State {
        State::Connected(
            Inner {
                flags: Flags {
                    layer_3_initiated: true,
                    ..Default::default()
                },
                ..Default::default()
            }
            .into(),
        )
    }
}

#[derive(Default, SpecCase)]
struct Case88_2YNN {}

impl SpecCase for Case88_2YNN {
    fn before(&self) -> State {
        State::AwaitingConnection(
            Inner {
                sequences: Sequences {
                    vs: Seq::new_modulo_8(2), // N
                    va: Seq::new_modulo_8(3),
                    ..Default::default()
                },
                queue: std::collections::VecDeque::from([test_info(8)]),
                ..Default::default()
            }
            .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(UnnumberedAcknowledge { is_final: true }.into())
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Primitive(Out::ConnectIndication)]
    }

    fn after(&self, _before: Inner) -> State {
        State::Connected(
            Inner {
                flags: Flags {
                    ..Default::default()
                },
                ..Default::default()
            }
            .into(),
        )
    }
}

#[derive(Default, SpecCase)]
struct Case88_3NVersion22 {}

impl SpecCase for Case88_3NVersion22 {
    fn before(&self) -> State {
        State::AwaitingConnection(
            Inner::default()
                .version(Version::Ax22)
                .select_timer_1_value()
                .retry_count_of(1, 2)
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Primitive(In::Timer(StandardTimer::T1))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Frame(
            SetAsynchronousBalancedModeExtended { p_or_f: true }.into(),
        )]
    }

    fn after(&self, before: Inner) -> State {
        State::AwaitingConnection(before.retry_count(2).start_timer_1().into())
    }
}

#[derive(Default, SpecCase)]
struct Case88_3N {}

impl SpecCase for Case88_3N {
    fn before(&self) -> State {
        State::AwaitingConnection(
            Inner::default()
                .select_timer_1_value()
                .retry_count_of(1, 2)
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Primitive(In::Timer(StandardTimer::T1))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Frame(Frame::SetAsynchronousBalancedMode(
            SetAsynchronousBalancedMode { p_or_f: true },
        ))]
    }

    fn after(&self, before: Inner) -> State {
        State::AwaitingConnection(before.retry_count(2).start_timer_1().into())
    }
}

#[derive(Default, SpecCase)]
struct Case88_3Y {}

impl SpecCase for Case88_3Y {
    fn before(&self) -> State {
        State::AwaitingConnection(
            Inner::default()
                .queued(test_info(8))
                .retry_count_of(2, 2)
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Primitive(In::Timer(StandardTimer::T1))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Primitive(Out::ErrorIndication(ErrorCode::G)),
            Generated::Primitive(Out::DisconnectIndication),
        ]
    }

    fn after(&self, before: Inner) -> State {
        State::Disconnected(before.discard_frame_queue().retry_count(0).into())
    }
}

#[derive(Default, SpecCase)]
struct Case88_4 {}

impl SpecCase for Case88_4 {
    fn before(&self) -> State {
        State::AwaitingConnection(
            Inner {
                ..Default::default()
            }
            .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(SetAsynchronousBalancedModeExtended { p_or_f: true }.into())
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Frame(DisconnectedMode { is_final: true }.into())]
    }

    fn after(&self, _before: Inner) -> State {
        State::AwaitingConnection(
            Inner {
                flags: Flags {
                    version: Version::Ax22,
                    ..Default::default()
                },
                ..Default::default()
            }
            .into(),
        )
    }
}

#[derive(Default, SpecCase)]
struct Case88_0Version22Sabme {}

impl SpecCase for Case88_0Version22Sabme {
    fn before(&self) -> State {
        State::AwaitingConnection(
            Inner {
                flags: Flags {
                    version: Version::Ax22,
                    ..Default::default()
                },
                ..Default::default()
            }
            .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(SetAsynchronousBalancedModeExtended { p_or_f: true }.into())
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Frame(
            UnnumberedAcknowledge { is_final: true }.into(),
        )]
    }

    fn after(&self, before: Inner) -> State {
        State::AwaitingConnection(before.into())
    }
}

#[derive(Default, SpecCase)]
struct Case88_0Version22FrameReject {}

impl SpecCase for Case88_0Version22FrameReject {
    fn before(&self) -> State {
        State::AwaitingConnection(
            Inner {
                flags: Flags {
                    version: Version::Ax22,
                    ..Default::default()
                },
                ..Default::default()
            }
            .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(FrameReject { is_final: true }.into())
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Frame(
            SetAsynchronousBalancedMode { p_or_f: true }.into(),
        )]
    }

    fn after(&self, before: Inner) -> State {
        State::AwaitingConnection(
            before
                .version(Version::Ax20)
                .select_timer_1_value()
                .layer_3_initiated(true)
                .start_timer_1()
                .into(),
        )
    }
}
