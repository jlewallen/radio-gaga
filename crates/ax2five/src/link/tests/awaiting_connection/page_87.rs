pub use super::*;

#[derive(Default, SpecCase)]
struct Case87_1 {}

impl SpecCase for Case87_1 {
    fn before(&self) -> State {
        State::AwaitingConnection(Default::default())
    }

    fn signal(&self) -> Received {
        Received::Primitive(In::DataRequest(test_info(8)))
    }

    fn after(&self, before: Inner) -> State {
        State::AwaitingConnection(before.queued(test_info(8)).into())
    }
}

#[derive(Default, SpecCase)]
struct Case87_2 {}

impl SpecCase for Case87_2 {
    fn before(&self) -> State {
        State::AwaitingConnection(Default::default())
    }

    fn signal(&self) -> Received {
        Received::Primitive(In::Flush)
    }

    fn after(&self, before: Inner) -> State {
        State::AwaitingConnection(before.into())
    }
}

#[derive(Default, SpecCase)]
struct Case87_4 {}

impl SpecCase for Case87_4 {
    fn before(&self) -> State {
        State::AwaitingConnection(Default::default())
    }

    fn signal(&self) -> Received {
        Received::Primitive(In::UnitDataRequest(test_info(8)))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Frame(Frame::UnnumberedInformation(
            UnnumberedInformation {
                c_or_r: Some(CommandResponse::Command),
                p_or_f: true,
                info: test_info(8),
            },
        ))]
    }

    fn after(&self, _before: Inner) -> State {
        State::AwaitingConnection(AwaitingConnection::default())
    }
}

#[derive(Default, SpecCase)]
struct Case87_3N {}

impl SpecCase for Case87_3N {
    fn before(&self) -> State {
        State::AwaitingConnection(Inner::default().into())
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::UnnumberedInformation(UnnumberedInformation {
            c_or_r: Some(CommandResponse::Command),
            p_or_f: false,
            info: test_info(8),
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Primitive(Out::UnitDataIndication(test_info(8)))]
    }

    fn after(&self, before: Inner) -> State {
        State::AwaitingConnection(before.into())
    }
}

#[derive(Default, SpecCase)]
struct Case87_3Y {}

impl SpecCase for Case87_3Y {
    fn before(&self) -> State {
        State::AwaitingConnection(Inner::default().into())
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::UnnumberedInformation(UnnumberedInformation {
            c_or_r: Some(CommandResponse::Command),
            p_or_f: true,
            info: test_info(8),
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Primitive(Out::UnitDataIndication(test_info(8))),
            Frame::DisconnectedMode(DisconnectedMode { is_final: true }).into(),
        ]
    }

    fn after(&self, before: Inner) -> State {
        State::AwaitingConnection(before.into())
    }
}
