use super::*;

// RR
#[derive(Default, SpecCase)]
struct Case95_1N {}

impl SpecCase for Case95_1N {
    fn before(&self) -> State {
        State::Connected(
            Inner::default()
                .select_timer_1_value()
                .unacknowledged_data()
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::ReceiveReady(ReceiveReady {
            c_or_r: Some(CommandResponse::Command),
            p_or_f: true,
            receive_sequence: 5,
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Frame(Frame::ReceiveReady(ReceiveReady {
                c_or_r: Some(CommandResponse::Response),
                p_or_f: true,
                receive_sequence: 0,
            })),
            Generated::Primitive(Out::ErrorIndication(ErrorCode::J)),
            Generated::Frame(Frame::SetAsynchronousBalancedMode(
                SetAsynchronousBalancedMode { p_or_f: true },
            )),
        ]
    }

    fn after(&self, before: Inner) -> State {
        State::AwaitingConnection(before.start_timer_1().into())
    }
}

#[derive(Default, SpecCase)]
struct Case95_1Y {}

impl SpecCase for Case95_1Y {
    fn before(&self) -> State {
        State::Connected(Inner::default().unacknowledged_data().into())
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::ReceiveReady(ReceiveReady {
            c_or_r: Some(CommandResponse::Command),
            p_or_f: true,
            receive_sequence: 1,
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Frame(Frame::ReceiveReady(ReceiveReady {
            c_or_r: Some(CommandResponse::Response),
            p_or_f: true,
            receive_sequence: 0,
        }))]
    }

    fn after(&self, before: Inner) -> State {
        State::Connected(before.into())
    }
}

// RnR
#[derive(Default, SpecCase)]
struct Case95_2N {}

impl SpecCase for Case95_2N {
    fn before(&self) -> State {
        State::Connected(
            Inner::default()
                .select_timer_1_value()
                .unacknowledged_data()
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::ReceiveNotReady(ReceiveNotReady {
            c_or_r: Some(CommandResponse::Command),
            p_or_f: true,
            receive_sequence: 5,
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Frame(Frame::ReceiveReady(ReceiveReady {
                c_or_r: Some(CommandResponse::Response),
                p_or_f: true,
                receive_sequence: 0,
            })),
            Generated::Primitive(Out::ErrorIndication(ErrorCode::J)),
            Generated::Frame(Frame::SetAsynchronousBalancedMode(
                SetAsynchronousBalancedMode { p_or_f: true },
            )),
        ]
    }

    fn after(&self, before: Inner) -> State {
        State::AwaitingConnection(before.start_timer_1().into())
    }
}

#[derive(Default, SpecCase)]
struct Case95_2Y {}

impl SpecCase for Case95_2Y {
    fn before(&self) -> State {
        State::Connected(
            Inner::default()
                .select_timer_1_value()
                .unacknowledged_data()
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::ReceiveNotReady(ReceiveNotReady {
            c_or_r: Some(CommandResponse::Command),
            p_or_f: true,
            receive_sequence: 1,
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Frame(Frame::ReceiveReady(ReceiveReady {
            c_or_r: Some(CommandResponse::Response),
            p_or_f: true,
            receive_sequence: 0,
        }))]
    }

    fn after(&self, before: Inner) -> State {
        State::Connected(
            before
                .set_peer_busy()
                .start_timer_1()
                .start_timer_3()
                .into(),
        )
    }
}

// SREJ
#[derive(Default, SpecCase)]
struct Case95_4N {}

impl SpecCase for Case95_4N {
    fn before(&self) -> State {
        State::Connected(Inner::default().select_timer_1_value().into())
    }

    fn signal(&self) -> Received {
        Received::Frame(
            SelectiveReject {
                p_or_f: true,
                receive_sequence: 1,
            }
            .into(),
        )
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Primitive(Out::ErrorIndication(ErrorCode::J)),
            Generated::Frame(Frame::SetAsynchronousBalancedMode(
                SetAsynchronousBalancedMode { p_or_f: true },
            )),
        ]
    }

    fn after(&self, before: Inner) -> State {
        State::AwaitingConnection(before.start_timer_1().into())
    }
}

#[derive(Default, SpecCase)]
struct Case95_4YY {}

impl SpecCase for Case95_4YY {
    fn before(&self) -> State {
        State::Connected(
            Inner::default()
                .save_pending_ack(1.into(), test_info(8))
                .increase_vs()
                .increase_vs()
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(
            SelectiveReject {
                p_or_f: true,
                receive_sequence: 1,
            }
            .into(),
        )
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Frame(
            ReceiveReady {
                c_or_r: Some(CommandResponse::Response),
                p_or_f: true,
                receive_sequence: 0,
            }
            .into(),
        )]
    }

    fn after(&self, before: Inner) -> State {
        State::Connected(
            before
                .set_va(1.into())
                .stop_timer_1()
                .start_timer_3()
                .select_timer_1_value()
                .queued(test_info(8))
                .into(),
        )
    }
}

#[derive(Default, SpecCase)]
struct Case95_4YN {}

impl SpecCase for Case95_4YN {
    fn before(&self) -> State {
        State::Connected(
            Inner::default()
                .save_pending_ack(1.into(), test_info(8))
                .increase_vs()
                .increase_vs()
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(
            SelectiveReject {
                p_or_f: false,
                receive_sequence: 1,
            }
            .into(),
        )
    }

    fn after(&self, before: Inner) -> State {
        State::Connected(
            before
                .stop_timer_1()
                .start_timer_3()
                .select_timer_1_value()
                .queued(test_info(8))
                .into(),
        )
    }
}
