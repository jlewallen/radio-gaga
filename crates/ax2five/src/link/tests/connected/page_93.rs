use super::*;

/// Timer 1 expiry
#[derive(Default, SpecCase)]
struct Case93_1 {}

impl SpecCase for Case93_1 {
    fn before(&self) -> State {
        State::Connected(Inner::default().select_timer_1_value().into())
    }

    fn signal(&self) -> Received {
        Received::Primitive(In::Timer(StandardTimer::T1))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Frame(Frame::ReceiveReady(ReceiveReady {
            c_or_r: Some(CommandResponse::Command),
            p_or_f: true,
            receive_sequence: 0,
        }))]
    }

    fn after(&self, before: Inner) -> State {
        State::TimerRecovery(before.start_timer_1().retry_count(1).into())
    }
}

/// Timer 1 expiry
#[derive(Default, SpecCase)]
struct Case93_1OwnReceiverBusy {}

impl SpecCase for Case93_1OwnReceiverBusy {
    fn before(&self) -> State {
        State::Connected(
            Inner::default()
                .select_timer_1_value()
                .set_own_busy()
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Primitive(In::Timer(StandardTimer::T1))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Frame(Frame::ReceiveNotReady(ReceiveNotReady {
            c_or_r: Some(CommandResponse::Command),
            p_or_f: true,
            receive_sequence: 0,
        }))]
    }

    fn after(&self, before: Inner) -> State {
        State::TimerRecovery(before.start_timer_1().retry_count(1).into())
    }
}

/// Timer 3 expiry
#[derive(Default, SpecCase)]
struct Case93_2 {}

impl SpecCase for Case93_2 {
    fn before(&self) -> State {
        State::Connected(Inner::default().select_timer_1_value().into())
    }

    fn signal(&self) -> Received {
        Received::Primitive(In::Timer(StandardTimer::T3))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Frame(Frame::ReceiveReady(ReceiveReady {
            c_or_r: Some(CommandResponse::Command),
            p_or_f: true,
            receive_sequence: 0,
        }))]
    }

    fn after(&self, before: Inner) -> State {
        State::TimerRecovery(before.start_timer_1().into())
    }
}

/// Timer 1 expiry
#[derive(Default, SpecCase)]
struct Case93_2OwnReceiverBusy {}

impl SpecCase for Case93_2OwnReceiverBusy {
    fn before(&self) -> State {
        State::Connected(
            Inner::default()
                .select_timer_1_value()
                .set_own_busy()
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Primitive(In::Timer(StandardTimer::T3))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Frame(Frame::ReceiveNotReady(ReceiveNotReady {
            c_or_r: Some(CommandResponse::Command),
            p_or_f: true,
            receive_sequence: 0,
        }))]
    }

    fn after(&self, before: Inner) -> State {
        State::TimerRecovery(before.start_timer_1().into())
    }
}

#[derive(Default, SpecCase)]
struct Case93_5 {}

impl SpecCase for Case93_5 {
    fn before(&self) -> State {
        State::Connected(
            Inner::default()
                .timer_1_value(500)
                .queued(test_info(8))
                .start_timer_3()
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::Disconnect(Disconnect { poll: true }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Frame(Frame::UnnumberedAcknowledge(UnnumberedAcknowledge {
                is_final: true,
            })),
            Generated::Primitive(Out::DisconnectIndication),
        ]
    }

    fn after(&self, before: Inner) -> State {
        State::Disconnected(
            before
                .discard_frame_queue()
                .stop_timer_3()
                .stop_timer_1()
                .into(),
        )
    }
}

#[derive(Default, SpecCase)]
struct Case93_6 {}

impl SpecCase for Case93_6 {
    fn before(&self) -> State {
        State::Connected(
            Inner::default()
                .timer_1_value(500)
                .layer_3_initiated(true)
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::UnnumberedAcknowledge(UnnumberedAcknowledge {
            is_final: true,
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Primitive(Out::ErrorIndication(ErrorCode::C)),
            Generated::Frame(Frame::SetAsynchronousBalancedMode(
                SetAsynchronousBalancedMode { p_or_f: true },
            )),
        ]
    }

    fn after(&self, before: Inner) -> State {
        State::AwaitingConnection(before.layer_3_initiated(false).start_timer_1().into())
    }
}

#[derive(Default, SpecCase)]
struct Case93_7 {}

impl SpecCase for Case93_7 {
    fn before(&self) -> State {
        State::Connected(
            Inner::default()
                .timer_1_value(500)
                .layer_3_initiated(true)
                .queued(test_info(8))
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::DisconnectedMode(DisconnectedMode { is_final: true }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Primitive(Out::ErrorIndication(ErrorCode::E)),
            Generated::Primitive(Out::DisconnectIndication),
        ]
    }

    fn after(&self, before: Inner) -> State {
        State::Disconnected(
            before
                .discard_frame_queue()
                .stop_timer_1()
                .stop_timer_3()
                .into(),
        )
    }
}
