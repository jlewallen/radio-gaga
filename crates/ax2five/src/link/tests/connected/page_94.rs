use super::*;

#[derive(Default, SpecCase)]
struct Case94_4 {}

impl SpecCase for Case94_4 {
    fn before(&self) -> State {
        State::Connected(Inner::default().into())
    }

    fn signal(&self) -> Received {
        Received::Primitive(In::UnitDataRequest(test_info(8)))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Frame(Frame::UnnumberedInformation(
            UnnumberedInformation {
                c_or_r: Some(CommandResponse::Command),
                p_or_f: true,
                info: test_info(8),
            },
        ))]
    }

    fn after(&self, before: Inner) -> State {
        State::Connected(before.into())
    }
}

#[derive(Default, SpecCase)]
struct Case94_5N {}

impl SpecCase for Case94_5N {
    fn before(&self) -> State {
        State::Connected(Inner::default().into())
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::UnnumberedInformation(UnnumberedInformation {
            c_or_r: Some(CommandResponse::Command),
            p_or_f: false,
            info: test_info(8),
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Primitive(Out::UnitDataIndication(test_info(8)))]
    }

    fn after(&self, before: Inner) -> State {
        State::Connected(before.into())
    }
}

#[derive(Default, SpecCase)]
struct Case94_5Y {}

impl SpecCase for Case94_5Y {
    fn before(&self) -> State {
        State::Connected(Inner::default().into())
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::UnnumberedInformation(UnnumberedInformation {
            c_or_r: Some(CommandResponse::Command),
            p_or_f: true,
            info: test_info(8),
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Primitive(Out::UnitDataIndication(test_info(8))),
            Frame::ReceiveReady(ReceiveReady {
                c_or_r: Some(CommandResponse::Response),
                p_or_f: true,
                receive_sequence: 0,
            })
            .into(),
        ]
    }

    fn after(&self, before: Inner) -> State {
        State::Connected(before.into())
    }
}
