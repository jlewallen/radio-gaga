use super::*;

#[derive(Default, SpecCase)]
struct Case96_1Y {}

impl SpecCase for Case96_1Y {
    fn before(&self) -> State {
        State::Connected(
            Inner::default()
                .increase_vs()
                .save_pending_ack(Seq::new_modulo_8(0), test_info(8))
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::Reject(Reject {
            c_or_r: Some(CommandResponse::Command),
            p_or_f: true,
            receive_sequence: 0,
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Frame(Frame::ReceiveReady(ReceiveReady {
            c_or_r: Some(CommandResponse::Response),
            p_or_f: true,
            receive_sequence: 0,
        }))]
    }

    fn after(&self, before: Inner) -> State {
        State::Connected(
            before
                .remove_pending_ack(Seq::new_modulo_8(0))
                .0
                .queued(test_info(8))
                .select_timer_1_value()
                .into(),
        )
    }
}

#[derive(Default, SpecCase)]
struct Case96_1N {}

impl SpecCase for Case96_1N {
    fn before(&self) -> State {
        State::Connected(Inner::default().select_timer_1_value().into())
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::Reject(Reject {
            c_or_r: Some(CommandResponse::Command),
            p_or_f: true,
            receive_sequence: 1,
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Frame(Frame::ReceiveReady(ReceiveReady {
                c_or_r: Some(CommandResponse::Response),
                p_or_f: true,
                receive_sequence: 0,
            })),
            Generated::Primitive(Out::ErrorIndication(ErrorCode::J)),
            Generated::Frame(Frame::SetAsynchronousBalancedMode(
                SetAsynchronousBalancedMode { p_or_f: true },
            )),
        ]
    }

    fn after(&self, before: Inner) -> State {
        State::AwaitingConnection(before.start_timer_1().into())
    }
}

// Information Frame Information Frame

#[derive(Default, SpecCase)]
struct Case96_2N {}

impl SpecCase for Case96_2N {
    fn before(&self) -> State {
        State::Connected(Inner::default().into())
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::Information(Information {
            c_or_r: Some(CommandResponse::Response),
            poll: false,
            info: test_info(8),
            send_sequence: 0,
            receive_sequence: 0,
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Primitive(Out::ErrorIndication(ErrorCode::S))]
    }

    fn after(&self, before: Inner) -> State {
        State::Connected(before.into())
    }
}

// Is Case92_2YN necessary?

#[derive(Default, SpecCase)]
struct Case96_2YYN {}

impl SpecCase for Case96_2YYN {
    fn before(&self) -> State {
        State::Connected(Inner::default().select_timer_1_value().into())
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::Information(Information {
            c_or_r: Some(CommandResponse::Command),
            poll: false,
            info: test_info(8),
            send_sequence: 0,
            receive_sequence: 2,
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Primitive(Out::ErrorIndication(ErrorCode::J)),
            Generated::Frame(Frame::SetAsynchronousBalancedMode(
                SetAsynchronousBalancedMode { p_or_f: true },
            )),
        ]
    }

    fn after(&self, before: Inner) -> State {
        State::AwaitingConnection(before.start_timer_1().into())
    }
}

#[derive(Default, SpecCase)]
struct Case96_2YYYYNotP {}

impl SpecCase for Case96_2YYYYNotP {
    fn before(&self) -> State {
        State::Connected(
            Inner::default()
                .start_timer_3()
                .select_timer_1_value()
                .set_own_busy()
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::Information(Information {
            c_or_r: Some(CommandResponse::Command),
            poll: false,
            info: test_info(8),
            send_sequence: 0,
            receive_sequence: 0,
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![]
    }

    fn after(&self, before: Inner) -> State {
        State::Connected(before.into())
    }
}

#[derive(Default, SpecCase)]
struct Case96_2YYYY {}

impl SpecCase for Case96_2YYYY {
    fn before(&self) -> State {
        State::Connected(
            Inner::default()
                .start_timer_3()
                .select_timer_1_value()
                .set_own_busy()
                .set_acknowledge_pending()
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::Information(Information {
            c_or_r: Some(CommandResponse::Command),
            poll: true,
            info: test_info(8),
            send_sequence: 0,
            receive_sequence: 0,
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Primitive(Out::AcknowledgePending(false)),
            Generated::Frame(Frame::ReceiveNotReady(ReceiveNotReady {
                c_or_r: Some(CommandResponse::Response),
                p_or_f: true,
                receive_sequence: 0,
            })),
        ]
    }

    fn after(&self, before: Inner) -> State {
        State::Connected(before.clear_acknowledge_pending().into())
    }
}

#[derive(Default, SpecCase)]
struct Case96_2YYYNYNonZero {}

impl SpecCase for Case96_2YYYNYNonZero {
    fn before(&self) -> State {
        State::Connected(
            Inner::default()
                .increase_vr()
                .start_timer_3()
                .select_timer_1_value()
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::Information(Information {
            c_or_r: Some(CommandResponse::Command),
            poll: true,
            info: test_info(8),
            send_sequence: 1,
            receive_sequence: 0,
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Primitive(Out::DataIndication(test_info(8))),
            Generated::Frame(Frame::ReceiveReady(ReceiveReady {
                c_or_r: Some(CommandResponse::Response),
                p_or_f: true,
                receive_sequence: 2,
            })),
        ]
    }

    fn after(&self, before: Inner) -> State {
        State::Connected(before.increase_vr().into())
    }
}

#[derive(Default, SpecCase)]
struct Case96_2YYYNNNN {}

impl SpecCase for Case96_2YYYNNNN {
    fn before(&self) -> State {
        State::Connected(
            Inner::default()
                .increase_vr()
                .start_timer_3()
                .select_timer_1_value()
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::Information(Information {
            c_or_r: Some(CommandResponse::Command),
            poll: true,
            info: test_info(8),
            send_sequence: 0,
            receive_sequence: 0,
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Frame(Frame::Reject(Reject {
            c_or_r: Some(CommandResponse::Command),
            p_or_f: true,
            receive_sequence: 1,
        }))]
    }

    fn after(&self, before: Inner) -> State {
        State::Connected(before.set_reject_exception().into())
    }
}

#[derive(Default, SpecCase)]
struct Case96_2YYYNY {}

impl SpecCase for Case96_2YYYNY {
    fn before(&self) -> State {
        State::Connected(
            Inner::default()
                .increase_vr()
                .start_timer_3()
                .select_timer_1_value()
                .set_reject_exception()
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::Information(Information {
            c_or_r: Some(CommandResponse::Command),
            poll: true,
            info: test_info(8),
            send_sequence: 0,
            receive_sequence: 0,
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Frame(Frame::ReceiveReady(ReceiveReady {
            c_or_r: Some(CommandResponse::Command),
            p_or_f: true,
            receive_sequence: 1,
        }))]
    }

    fn after(&self, before: Inner) -> State {
        State::Connected(before.into())
    }
}

#[derive(Default, SpecCase)]
struct Case96_2YYYYY {}

impl SpecCase for Case96_2YYYYY {
    fn before(&self) -> State {
        State::Connected(
            Inner::default()
                .start_timer_3()
                .select_timer_1_value()
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::Information(Information {
            c_or_r: Some(CommandResponse::Command),
            poll: true,
            info: test_info(8),
            send_sequence: 0,
            receive_sequence: 0,
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Primitive(Out::DataIndication(test_info(8))),
            Generated::Frame(Frame::ReceiveReady(ReceiveReady {
                c_or_r: Some(CommandResponse::Response),
                p_or_f: true,
                receive_sequence: 1,
            })),
        ]
    }

    fn after(&self, before: Inner) -> State {
        State::Connected(before.increase_vr().clear_reject_exception().into())
    }
}

#[derive(Default, SpecCase)]
struct Case96_2YYYYYNoImmediateAck {}

impl SpecCase for Case96_2YYYYYNoImmediateAck {
    fn before(&self) -> State {
        State::Connected(
            Inner::default()
                .start_timer_3()
                .select_timer_1_value()
                .set_immediate_ack_on_information_poll(false)
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::Information(Information {
            c_or_r: Some(CommandResponse::Command),
            poll: true,
            info: test_info(8),
            send_sequence: 0,
            receive_sequence: 0,
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Primitive(Out::DataIndication(test_info(8))),
            Generated::Primitive(Out::AcknowledgePending(true)),
        ]
    }

    fn after(&self, before: Inner) -> State {
        State::Connected(before.increase_vr().set_acknowledge_pending().into())
    }
}

#[derive(Default, SpecCase)]
struct Case96_2YYYYYNoPoll {}

impl SpecCase for Case96_2YYYYYNoPoll {
    fn before(&self) -> State {
        State::Connected(
            Inner::default()
                .start_timer_3()
                .select_timer_1_value()
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::Information(Information {
            c_or_r: Some(CommandResponse::Command),
            poll: false,
            info: test_info(8),
            send_sequence: 0,
            receive_sequence: 0,
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Primitive(Out::DataIndication(test_info(8))),
            Generated::Primitive(Out::AcknowledgePending(true)),
        ]
    }

    fn after(&self, before: Inner) -> State {
        State::Connected(before.increase_vr().set_acknowledge_pending().into())
    }
}

// After incrementing vr in receive, the queued frames are checked and then Indicated.
#[derive(Default, SpecCase)]
struct Case96_2YYYYYAfterSrej {}

impl SpecCase for Case96_2YYYYYAfterSrej {
    fn before(&self) -> State {
        State::Connected(
            Inner::default()
                .start_timer_3()
                .select_timer_1_value()
                .save_incoming(Seq::new_modulo_8(1), test_info(8))
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::Information(Information {
            c_or_r: Some(CommandResponse::Command),
            poll: true,
            info: test_info(8),
            send_sequence: 0,
            receive_sequence: 0,
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Primitive(Out::DataIndication(test_info(8))),
            Generated::Primitive(Out::DataIndication(test_info(8))),
            Generated::Frame(Frame::ReceiveReady(ReceiveReady {
                c_or_r: Some(CommandResponse::Response),
                p_or_f: true,
                receive_sequence: 2,
            })),
        ]
    }

    fn after(&self, before: Inner) -> State {
        State::Connected(
            before
                .remove_incoming(Seq::new_modulo_8(1))
                .0
                .increase_vr()
                .increase_vr()
                .clear_reject_exception()
                .into(),
        )
    }
}

#[derive(Default, SpecCase)]
struct Case96_2YYYYNotPAndAcksSent {}

impl SpecCase for Case96_2YYYYNotPAndAcksSent {
    fn before(&self) -> State {
        State::Connected(
            Inner::default()
                .increase_vs()
                .select_timer_1_value()
                .start_timer_1()
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::Information(Information {
            c_or_r: Some(CommandResponse::Command),
            poll: false,
            info: test_info(8),
            send_sequence: 0,
            receive_sequence: 1,
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Primitive(Out::DataIndication(test_info(8))),
            Generated::Primitive(Out::AcknowledgePending(true)),
        ]
    }

    fn after(&self, before: Inner) -> State {
        State::Connected(
            before
                .increase_va()
                .increase_vr()
                .stop_timer_1()
                .start_timer_3()
                .set_acknowledge_pending()
                .into(),
        )
    }
}

#[derive(Default, SpecCase)]
struct Case96_0Acknowledge {}

impl SpecCase for Case96_0Acknowledge {
    fn before(&self) -> State {
        State::Connected(
            Inner::default()
                .increase_vr()
                .select_timer_1_value()
                .start_timer_1()
                .set_acknowledge_pending()
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Primitive(In::Acknowledge)
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Primitive(Out::AcknowledgePending(false)),
            Generated::Frame(Frame::ReceiveReady(ReceiveReady {
                c_or_r: Some(CommandResponse::Response),
                p_or_f: false,
                receive_sequence: 1,
            })),
        ]
    }

    fn after(&self, before: Inner) -> State {
        State::Connected(before.clear_acknowledge_pending().into())
    }
}

#[derive(Default, SpecCase)]
struct Case96_0AcknowledgeNonPending {}

impl SpecCase for Case96_0AcknowledgeNonPending {
    fn before(&self) -> State {
        State::Connected(
            Inner::default()
                .increase_vr()
                .select_timer_1_value()
                .start_timer_1()
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Primitive(In::Acknowledge)
    }

    fn generated(&self) -> Vec<Generated> {
        vec![]
    }

    fn after(&self, before: Inner) -> State {
        State::Connected(before.into())
    }
}

/// SREJ Enabled
#[derive(Default, SpecCase)]
struct Case96_2SrejRejectNoGaps {}

impl SpecCase for Case96_2SrejRejectNoGaps {
    fn before(&self) -> State {
        State::Connected(
            Inner::default()
                .set_selective_reject_enabled(true)
                .select_timer_1_value()
                .start_timer_3()
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::Information(Information {
            c_or_r: Some(CommandResponse::Command),
            poll: true,
            info: test_info(8),
            send_sequence: 2,
            receive_sequence: 0,
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Frame(
            Reject {
                c_or_r: Some(CommandResponse::Command),
                p_or_f: true,
                receive_sequence: 0,
            }
            .into(),
        )]
    }

    fn after(&self, before: Inner) -> State {
        State::Connected(
            before
                .set_reject_exception()
                .save_incoming(2.into(), test_info(8))
                .into(),
        )
    }
}

#[derive(Default, SpecCase)]
struct Case96_2SrejStarting {}

impl SpecCase for Case96_2SrejStarting {
    fn before(&self) -> State {
        State::Connected(
            Inner::default()
                .set_selective_reject_enabled(true)
                .select_timer_1_value()
                .start_timer_3()
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::Information(Information {
            c_or_r: Some(CommandResponse::Command),
            poll: true,
            info: test_info(8),
            send_sequence: 1,
            receive_sequence: 0,
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Frame(
            SelectiveReject {
                p_or_f: true,
                receive_sequence: 0,
            }
            .into(),
        )]
    }

    fn after(&self, before: Inner) -> State {
        State::Connected(
            before
                .set_selective_reject_exception(1)
                .save_incoming(1.into(), test_info(8))
                .into(),
        )
    }
}

#[derive(Default, SpecCase)]
struct Case96_2SrejStarted {}

impl SpecCase for Case96_2SrejStarted {
    fn before(&self) -> State {
        State::Connected(
            Inner::default()
                .set_selective_reject_enabled(true)
                .save_incoming(1.into(), test_info(8))
                .set_selective_reject_exception(1)
                .select_timer_1_value()
                .start_timer_3()
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::Information(Information {
            c_or_r: Some(CommandResponse::Command),
            poll: true,
            info: test_info(8),
            send_sequence: 1,
            receive_sequence: 0,
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Frame(
            SelectiveReject {
                p_or_f: false,
                receive_sequence: 1,
            }
            .into(),
        )]
    }

    fn after(&self, before: Inner) -> State {
        State::Connected(
            before
                .set_selective_reject_exception(2)
                .save_incoming(1.into(), test_info(8))
                .into(),
        )
    }
}
