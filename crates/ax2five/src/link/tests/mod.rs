use tracing::*;
use tracing_test::traced_test;

pub use super::frames::*;
pub use super::inner::*;
pub use super::sched::*;
pub use super::states::*;
pub use macros::SpecCase;
pub use pretty_assertions::assert_eq;

mod awaiting_connection;
mod awaiting_release;
mod connected;
mod disconnected;
mod timer_recovery;

use super::frames::Info;

fn new_info(b: u8, size: usize) -> Info {
    vec![b; size].into()
}

fn test_info(size: usize) -> Info {
    new_info(b'j', size)
}

trait SpecCase {
    fn before(&self) -> State;

    fn signal(&self) -> Received;

    fn generated(&self) -> Vec<Generated> {
        Vec::default()
    }

    fn after(&self, before: Inner) -> State;
}

#[test]
#[traced_test]
pub fn test_scheduler() -> anyhow::Result<()> {
    let mut scheduler = Scheduler::default();
    let a = scheduler.add("HECTOR", "DESI");
    let _b = scheduler.connect("DESI", "HECTOR");

    scheduler.loopback(100.into())?;

    scheduler.loopback(100.into())?;

    scheduler.loopback(100.into())?;

    scheduler.write(a, vec![b'j'; 8])?;

    scheduler.loopback(100.into())?;

    scheduler.loopback(100.into())?;

    Ok(())
}
