use super::*;

/// Timer 1 expiry
#[derive(Default, SpecCase)]
struct Case99_1N {}

impl SpecCase for Case99_1N {
    fn before(&self) -> State {
        State::TimerRecovery(
            Inner::default()
                .timer_1_value(500)
                .retry_count_of(0, 2)
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Primitive(In::Timer(StandardTimer::T1))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Frame(Frame::ReceiveReady(ReceiveReady {
            c_or_r: Some(CommandResponse::Command),
            p_or_f: true,
            receive_sequence: 0,
        }))]
    }

    fn after(&self, before: Inner) -> State {
        State::TimerRecovery(before.retry_count_of(1, 2).start_timer_1().into())
    }
}

/// Timer 1 expiry
#[derive(Default, SpecCase)]
struct Case99_1YYY {}

impl SpecCase for Case99_1YYY {
    fn before(&self) -> State {
        State::TimerRecovery(Inner::default().retry_count_of(2, 2).set_peer_busy().into())
    }

    fn signal(&self) -> Received {
        Received::Primitive(In::Timer(StandardTimer::T1))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Primitive(Out::ErrorIndication(ErrorCode::U)),
            Generated::Primitive(Out::DisconnectIndication), // Spec has this has a REQUEST?
            Generated::Frame(Frame::DisconnectedMode(DisconnectedMode { is_final: true })),
        ]
    }

    fn after(&self, _before: Inner) -> State {
        State::Disconnected(
            Inner {
                flags: Flags {
                    parameters: Parameters {
                        retries_n2: 2,
                        ..Default::default()
                    },
                    peer_receiver_busy: true,
                    ..Default::default()
                },
                ..Inner::default()
            }
            .into(),
        )
    }
}

/// Timer 1 expiry
#[derive(Default, SpecCase)]
struct Case99_1YN {}

impl SpecCase for Case99_1YN {
    fn before(&self) -> State {
        State::TimerRecovery(
            Inner::default()
                .retry_count_of(2, 2)
                .unacknowledged_data()
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Primitive(In::Timer(StandardTimer::T1))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Primitive(Out::ErrorIndication(ErrorCode::I)),
            Generated::Primitive(Out::DisconnectIndication), // Spec has this has a REQUEST?
            Generated::Frame(Frame::DisconnectedMode(DisconnectedMode { is_final: true })),
        ]
    }

    fn after(&self, _before: Inner) -> State {
        State::Disconnected(
            Inner {
                flags: Flags {
                    parameters: Parameters {
                        retries_n2: 2,
                        ..Default::default()
                    },
                    ..Default::default()
                },
                ..Inner::default()
            }
            .into(),
        )
    }
}

/// Timer 1 expiry
#[derive(Default, SpecCase)]
struct Case99_1YYN {}

impl SpecCase for Case99_1YYN {
    fn before(&self) -> State {
        State::TimerRecovery(Inner::default().retry_count_of(2, 2).into())
    }

    fn signal(&self) -> Received {
        Received::Primitive(In::Timer(StandardTimer::T1))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Primitive(Out::ErrorIndication(ErrorCode::T)),
            Generated::Primitive(Out::DisconnectIndication), // Spec has this has a REQUEST?
            Generated::Frame(Frame::DisconnectedMode(DisconnectedMode { is_final: true })),
        ]
    }

    fn after(&self, _before: Inner) -> State {
        State::Disconnected(
            Inner {
                flags: Flags {
                    parameters: Parameters {
                        retries_n2: 2,
                        ..Default::default()
                    },
                    ..Default::default()
                },
                ..Inner::default()
            }
            .into(),
        )
    }
}

#[derive(Default, SpecCase)]
struct Case99_2Y {}

impl SpecCase for Case99_2Y {
    fn before(&self) -> State {
        State::TimerRecovery(Inner::default().increase_vs().increase_va().into())
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::SetAsynchronousBalancedMode(
            SetAsynchronousBalancedMode { p_or_f: true },
        ))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Frame(Frame::UnnumberedAcknowledge(UnnumberedAcknowledge {
                is_final: true,
            })),
            Generated::Primitive(Out::ErrorIndication(ErrorCode::F)),
        ]
    }

    fn after(&self, before: Inner) -> State {
        State::Connected(
            before
                .stop_timer_1()
                .start_timer_3()
                .clear_sequences()
                .into(),
        )
    }
}

#[derive(Default, SpecCase)]
struct Case99_2N {}

impl SpecCase for Case99_2N {
    fn before(&self) -> State {
        State::TimerRecovery(Inner::default().queued(test_info(8)).increase_vs().into())
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::SetAsynchronousBalancedMode(
            SetAsynchronousBalancedMode { p_or_f: true },
        ))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Frame(Frame::UnnumberedAcknowledge(UnnumberedAcknowledge {
                is_final: true,
            })),
            Generated::Primitive(Out::ErrorIndication(ErrorCode::F)),
            Generated::Primitive(Out::ConnectIndication),
        ]
    }

    fn after(&self, before: Inner) -> State {
        State::Connected(
            before
                .discard_frame_queue()
                .stop_timer_1()
                .start_timer_3()
                .clear_sequences()
                .into(),
        )
    }
}

#[derive(Default, SpecCase)]
struct Case99_4YYY {}

impl SpecCase for Case99_4YYY {
    fn before(&self) -> State {
        State::TimerRecovery(Inner::default().increase_vs().into())
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::ReceiveReady(ReceiveReady {
            c_or_r: Some(CommandResponse::Response),
            p_or_f: true,
            receive_sequence: 1,
        }))
    }

    fn after(&self, before: Inner) -> State {
        State::Connected(
            before
                .stop_timer_1()
                .select_timer_1_value()
                .start_timer_3()
                .increase_va()
                .into(),
        )
    }
}

#[derive(Default, SpecCase)]
struct Case99_4YYN {}

impl SpecCase for Case99_4YYN {
    fn before(&self) -> State {
        State::TimerRecovery(
            Inner::default()
                .save_pending_ack(0.into(), test_info(8))
                .increase_vs()
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::ReceiveReady(ReceiveReady {
            c_or_r: Some(CommandResponse::Response),
            p_or_f: true,
            receive_sequence: 0,
        }))
    }

    fn after(&self, before: Inner) -> State {
        State::TimerRecovery(
            before
                .stop_timer_1()
                .select_timer_1_value()
                .remove_pending_ack(0.into())
                .0
                .queued(test_info(8))
                .into(),
        )
    }
}

#[derive(Default, SpecCase)]
struct Case99_4YN {}

impl SpecCase for Case99_4YN {
    fn before(&self) -> State {
        State::TimerRecovery(Inner::default().into())
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::ReceiveReady(ReceiveReady {
            c_or_r: Some(CommandResponse::Response),
            p_or_f: true,
            receive_sequence: 1,
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Primitive(Out::ErrorIndication(ErrorCode::J)),
            Generated::Frame(Frame::SetAsynchronousBalancedMode(
                SetAsynchronousBalancedMode { p_or_f: true },
            )),
        ]
    }

    fn after(&self, before: Inner) -> State {
        State::TimerRecovery(
            before
                .stop_timer_1()
                .select_timer_1_value()
                .start_timer_1()
                .into(),
        )
    }
}

#[derive(Default, SpecCase)]
struct Case99_4NNN {}

impl SpecCase for Case99_4NNN {
    fn before(&self) -> State {
        State::TimerRecovery(Inner::default().into())
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::ReceiveReady(ReceiveReady {
            c_or_r: Some(CommandResponse::Command),
            p_or_f: false,
            receive_sequence: 0,
        }))
    }

    fn after(&self, before: Inner) -> State {
        State::TimerRecovery(before.into())
    }
}

#[derive(Default, SpecCase)]
struct Case99_4NYN {}

impl SpecCase for Case99_4NYN {
    fn before(&self) -> State {
        State::TimerRecovery(Inner::default().select_timer_1_value().into())
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::ReceiveReady(ReceiveReady {
            c_or_r: Some(CommandResponse::Command),
            p_or_f: true,
            receive_sequence: 1,
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Frame(Frame::ReceiveReady(ReceiveReady {
                c_or_r: Some(CommandResponse::Response),
                p_or_f: true,
                receive_sequence: 0,
            })),
            Generated::Primitive(Out::ErrorIndication(ErrorCode::J)),
            Generated::Frame(Frame::SetAsynchronousBalancedMode(
                SetAsynchronousBalancedMode { p_or_f: true },
            )),
        ]
    }

    fn after(&self, before: Inner) -> State {
        State::AwaitingConnection(before.start_timer_1().into())
    }
}

#[derive(Default, SpecCase)]
struct Case99_4NYY {}

impl SpecCase for Case99_4NYY {
    fn before(&self) -> State {
        State::TimerRecovery(Inner::default().increase_vs().into())
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::ReceiveReady(ReceiveReady {
            c_or_r: Some(CommandResponse::Command),
            p_or_f: true,
            receive_sequence: 1,
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Frame(Frame::ReceiveReady(ReceiveReady {
            c_or_r: Some(CommandResponse::Response),
            p_or_f: true,
            receive_sequence: 0,
        }))]
    }

    fn after(&self, before: Inner) -> State {
        State::TimerRecovery(before.increase_va().into())
    }
}

// RnR

#[derive(Default, SpecCase)]
struct Case99_5YYY {}

impl SpecCase for Case99_5YYY {
    fn before(&self) -> State {
        State::TimerRecovery(Inner::default().increase_vs().into())
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::ReceiveNotReady(ReceiveNotReady {
            c_or_r: Some(CommandResponse::Response),
            p_or_f: true,
            receive_sequence: 1,
        }))
    }

    fn after(&self, before: Inner) -> State {
        State::Connected(
            before
                .set_peer_busy()
                .stop_timer_1()
                .select_timer_1_value()
                .start_timer_3()
                .increase_va()
                .into(),
        )
    }
}

#[derive(Default, SpecCase)]
struct Case99_5YYN {}

impl SpecCase for Case99_5YYN {
    fn before(&self) -> State {
        State::TimerRecovery(
            Inner::default()
                .save_pending_ack(0.into(), test_info(8))
                .increase_vs()
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::ReceiveNotReady(ReceiveNotReady {
            c_or_r: Some(CommandResponse::Response),
            p_or_f: true,
            receive_sequence: 0,
        }))
    }

    fn after(&self, before: Inner) -> State {
        State::TimerRecovery(
            before
                .set_peer_busy()
                .stop_timer_1()
                .select_timer_1_value()
                .remove_pending_ack(0.into())
                .0
                .queued(test_info(8))
                .into(),
        )
    }
}

#[derive(Default, SpecCase)]
struct Case99_5YN {}

impl SpecCase for Case99_5YN {
    fn before(&self) -> State {
        State::TimerRecovery(Inner::default().into())
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::ReceiveNotReady(ReceiveNotReady {
            c_or_r: Some(CommandResponse::Response),
            p_or_f: true,
            receive_sequence: 1,
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Primitive(Out::ErrorIndication(ErrorCode::J)),
            Generated::Frame(Frame::SetAsynchronousBalancedMode(
                SetAsynchronousBalancedMode { p_or_f: true },
            )),
        ]
    }

    fn after(&self, before: Inner) -> State {
        State::TimerRecovery(
            before
                // .set_peer_busy() cleared during establish data link
                .stop_timer_1()
                .select_timer_1_value()
                .start_timer_1()
                .into(),
        )
    }
}

#[derive(Default, SpecCase)]
struct Case99_5NNN {}

impl SpecCase for Case99_5NNN {
    fn before(&self) -> State {
        State::TimerRecovery(Inner::default().into())
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::ReceiveNotReady(ReceiveNotReady {
            c_or_r: Some(CommandResponse::Command),
            p_or_f: false,
            receive_sequence: 0,
        }))
    }

    fn after(&self, before: Inner) -> State {
        State::TimerRecovery(before.set_peer_busy().into())
    }
}

#[derive(Default, SpecCase)]
struct Case99_5NYN {}

impl SpecCase for Case99_5NYN {
    fn before(&self) -> State {
        State::TimerRecovery(Inner::default().select_timer_1_value().into())
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::ReceiveNotReady(ReceiveNotReady {
            c_or_r: Some(CommandResponse::Command),
            p_or_f: true,
            receive_sequence: 1,
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Frame(Frame::ReceiveReady(ReceiveReady {
                c_or_r: Some(CommandResponse::Response),
                p_or_f: true,
                receive_sequence: 0,
            })),
            Generated::Primitive(Out::ErrorIndication(ErrorCode::J)),
            Generated::Frame(Frame::SetAsynchronousBalancedMode(
                SetAsynchronousBalancedMode { p_or_f: true },
            )),
        ]
    }

    fn after(&self, before: Inner) -> State {
        State::AwaitingConnection(
            before
                // .set_peer_busy() cleared during establish data link
                .start_timer_1()
                .into(),
        )
    }
}

#[derive(Default, SpecCase)]
struct Case99_5NYY {}

impl SpecCase for Case99_5NYY {
    fn before(&self) -> State {
        State::TimerRecovery(Inner::default().increase_vs().into())
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::ReceiveNotReady(ReceiveNotReady {
            c_or_r: Some(CommandResponse::Command),
            p_or_f: true,
            receive_sequence: 1,
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Frame(Frame::ReceiveReady(ReceiveReady {
            c_or_r: Some(CommandResponse::Response),
            p_or_f: true,
            receive_sequence: 0,
        }))]
    }

    fn after(&self, before: Inner) -> State {
        State::TimerRecovery(before.set_peer_busy().increase_va().into())
    }
}
