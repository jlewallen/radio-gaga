use super::*;

#[derive(Default, SpecCase)]
struct Case101_1 {}

impl SpecCase for Case101_1 {
    fn before(&self) -> State {
        State::TimerRecovery(Inner::default().queued(test_info(8)).into())
    }

    fn signal(&self) -> Received {
        Received::Frame(DisconnectedMode { is_final: true }.into())
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Primitive(Out::ErrorIndication(ErrorCode::E)),
            Generated::Primitive(Out::DisconnectIndication),
        ]
    }

    fn after(&self, before: Inner) -> State {
        State::Disconnected(before.discard_frame_queue().into())
    }
}
