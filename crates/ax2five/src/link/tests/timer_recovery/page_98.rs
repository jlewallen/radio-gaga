use super::*;

// CONNECT-REQUEST
#[derive(Default, SpecCase)]
struct Case98_4 {}

impl SpecCase for Case98_4 {
    fn before(&self) -> State {
        State::TimerRecovery(
            Inner::default()
                .select_timer_1_value()
                .queued(test_info(8))
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Primitive(In::ConnectRequest)
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Frame(
            SetAsynchronousBalancedMode { p_or_f: true }.into(),
        )]
    }

    fn after(&self, before: Inner) -> State {
        State::AwaitingConnection(
            before
                .discard_frame_queue()
                .start_timer_1()
                .layer_3_initiated(true)
                .into(),
        )
    }
}

#[derive(Default, SpecCase)]
struct Case98_5 {}

impl SpecCase for Case98_5 {
    fn before(&self) -> State {
        State::TimerRecovery(
            Inner::default()
                .queued(test_info(8))
                .select_timer_1_value()
                .start_timer_3()
                .retry_count(1)
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Primitive(In::DisconnectRequest)
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Frame(Disconnect { poll: true }.into())]
    }

    fn after(&self, before: Inner) -> State {
        State::AwaitingRelease(
            before
                .discard_frame_queue()
                .retry_count(0)
                .start_timer_1()
                .stop_timer_3()
                .into(),
        )
    }
}

#[derive(Default, SpecCase)]
struct Case98_6 {}

impl SpecCase for Case98_6 {
    fn before(&self) -> State {
        State::TimerRecovery(Inner::default().into())
    }

    fn signal(&self) -> Received {
        Received::Primitive(In::DataRequest(test_info(8)))
    }

    fn after(&self, before: Inner) -> State {
        State::TimerRecovery(before.queued(test_info(8)).into())
    }
}

// Send outgoing frames, peer busy.
#[derive(Default, SpecCase)]
struct Case98_7Y {}

impl SpecCase for Case98_7Y {
    fn before(&self) -> State {
        State::TimerRecovery(Inner::default().queued(test_info(8)).set_peer_busy().into())
    }

    fn signal(&self) -> Received {
        Received::Primitive(In::Flush)
    }

    fn generated(&self) -> Vec<Generated> {
        vec![]
    }

    fn after(&self, before: Inner) -> State {
        State::TimerRecovery(before.into())
    }
}

// Send outgoing frames, reached maximum frames (k)
#[derive(Default, SpecCase)]
struct Case98_7NY {}

impl SpecCase for Case98_7NY {
    fn before(&self) -> State {
        State::TimerRecovery(
            Inner::default()
                .queued(test_info(8))
                .reached_max_frames()
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Primitive(In::Flush)
    }

    fn after(&self, before: Inner) -> State {
        State::TimerRecovery(before.into())
    }
}

// Send outgoing frames, sending, T1 running
#[derive(Default, SpecCase)]
struct Case98_7NNY {}

impl SpecCase for Case98_7NNY {
    fn before(&self) -> State {
        State::TimerRecovery(
            Inner::default()
                .queued(test_info(8))
                .timer_1_value(500)
                .start_timer_1()
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Primitive(In::Flush)
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Frame(Frame::Information(Information {
            c_or_r: Some(CommandResponse::Command),
            poll: true,
            info: test_info(8),
            send_sequence: 0,
            receive_sequence: 0,
        }))]
    }

    fn after(&self, before: Inner) -> State {
        State::TimerRecovery(
            before
                .discard_frame_queue()
                .increase_vs()
                .save_pending_ack(0.into(), test_info(8))
                .into(),
        )
    }
}

// Send outgoing frames, sending
#[derive(Default, SpecCase)]
struct Case98_7NNN {}

impl SpecCase for Case98_7NNN {
    fn before(&self) -> State {
        State::TimerRecovery(
            Inner::default()
                .timer_1_value(500)
                .queued(test_info(8))
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Primitive(In::Flush)
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Frame(Frame::Information(Information {
            c_or_r: Some(CommandResponse::Command),
            poll: true,
            info: test_info(8),
            send_sequence: 0,
            receive_sequence: 0,
        }))]
    }

    fn after(&self, before: Inner) -> State {
        State::TimerRecovery(
            before
                .discard_frame_queue()
                .increase_vs()
                .save_pending_ack(0.into(), test_info(8))
                .stop_timer_3()
                .start_timer_1()
                .into(),
        )
    }
}

// Send multiple outgoing frames queues up to maximum frames (k)
#[derive(Default, SpecCase)]
struct Case98_7NYUpToMax {}

impl SpecCase for Case98_7NYUpToMax {
    fn before(&self) -> State {
        State::TimerRecovery(
            Inner::default()
                .select_timer_1_value()
                .queued(test_info(8))
                .queued(test_info(8))
                .queued(test_info(8))
                .queued(test_info(8))
                .queued(test_info(8))
                .queued(test_info(8))
                .queued(test_info(8))
                .queued(test_info(8))
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Primitive(In::Flush)
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Frame(Frame::Information(Information {
                c_or_r: Some(CommandResponse::Command),
                poll: false,
                info: test_info(8),
                send_sequence: 0,
                receive_sequence: 0,
            })),
            Generated::Frame(Frame::Information(Information {
                c_or_r: Some(CommandResponse::Command),
                poll: false,
                info: test_info(8),
                send_sequence: 1,
                receive_sequence: 0,
            })),
            Generated::Frame(Frame::Information(Information {
                c_or_r: Some(CommandResponse::Command),
                poll: false,
                info: test_info(8),
                send_sequence: 2,
                receive_sequence: 0,
            })),
            Generated::Frame(Frame::Information(Information {
                c_or_r: Some(CommandResponse::Command),
                poll: false,
                info: test_info(8),
                send_sequence: 3,
                receive_sequence: 0,
            })),
            Generated::Frame(Frame::Information(Information {
                c_or_r: Some(CommandResponse::Command),
                poll: false,
                info: test_info(8),
                send_sequence: 4,
                receive_sequence: 0,
            })),
            Generated::Frame(Frame::Information(Information {
                c_or_r: Some(CommandResponse::Command),
                poll: false,
                info: test_info(8),
                send_sequence: 5,
                receive_sequence: 0,
            })),
            Generated::Frame(Frame::Information(Information {
                c_or_r: Some(CommandResponse::Command),
                poll: true,
                info: test_info(8),
                send_sequence: 6,
                receive_sequence: 0,
            })),
        ]
    }

    fn after(&self, before: Inner) -> State {
        State::TimerRecovery(
            before
                .start_timer_1()
                .increase_vs()
                .increase_vs()
                .increase_vs()
                .increase_vs()
                .increase_vs()
                .increase_vs()
                .increase_vs()
                .discard_frame_queue()
                .queued(test_info(8))
                .save_pending_ack(0.into(), test_info(8))
                .save_pending_ack(1.into(), test_info(8))
                .save_pending_ack(2.into(), test_info(8))
                .save_pending_ack(3.into(), test_info(8))
                .save_pending_ack(4.into(), test_info(8))
                .save_pending_ack(5.into(), test_info(8))
                .save_pending_ack(6.into(), test_info(8))
                .into(),
        )
    }
}

// DATA-REQUEST larger than N1
#[derive(Default, SpecCase)]
struct Case98_6LargerThanN1 {}

impl SpecCase for Case98_6LargerThanN1 {
    fn before(&self) -> State {
        State::TimerRecovery(
            Inner::default()
                .set_packet_length_n1(172)
                .timer_1_value(500)
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Primitive(In::DataRequest(test_info(256)))
    }

    fn after(&self, before: Inner) -> State {
        State::TimerRecovery(
            before
                .queued(test_info(172))
                .queued(test_info(256 - 172))
                .into(),
        )
    }
}
