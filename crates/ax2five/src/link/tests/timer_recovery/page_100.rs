use super::*;

#[derive(Default, SpecCase)]
struct Case100_1 {}

impl SpecCase for Case100_1 {
    fn before(&self) -> State {
        State::TimerRecovery(
            Inner::default()
                .select_timer_1_value()
                .start_timer_1()
                .queued(test_info(8))
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(Disconnect { poll: true }.into())
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Frame(UnnumberedAcknowledge { is_final: true }.into()),
            Generated::Primitive(Out::DisconnectIndication),
        ]
    }

    fn after(&self, before: Inner) -> State {
        State::Disconnected(
            before
                .discard_frame_queue()
                .stop_timer_1()
                .stop_timer_3()
                .into(),
        )
    }
}

#[derive(Default, SpecCase)]
struct Case100_2 {}

impl SpecCase for Case100_2 {
    fn before(&self) -> State {
        State::TimerRecovery(
            Inner::default()
                .layer_3_initiated(true)
                .select_timer_1_value()
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(UnnumberedAcknowledge { is_final: true }.into())
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Primitive(Out::ErrorIndication(ErrorCode::C)),
            Generated::Frame(SetAsynchronousBalancedMode { p_or_f: true }.into()),
        ]
    }

    fn after(&self, before: Inner) -> State {
        State::AwaitingConnection(before.layer_3_initiated(false).start_timer_1().into())
    }
}

#[derive(Default, SpecCase)]
struct Case100_4 {}

impl SpecCase for Case100_4 {
    fn before(&self) -> State {
        State::TimerRecovery(Inner::default().layer_3_initiated(true).into())
    }

    fn signal(&self) -> Received {
        Received::Frame(
            UnnumberedInformation {
                c_or_r: Some(CommandResponse::Command),
                p_or_f: true,
                info: test_info(8),
            }
            .into(),
        )
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Primitive(Out::UnitDataIndication(test_info(8))),
            Generated::Frame(Frame::ReceiveReady(ReceiveReady {
                c_or_r: Some(CommandResponse::Response),
                p_or_f: true,
                receive_sequence: 0,
            })),
        ]
    }

    fn after(&self, before: Inner) -> State {
        State::TimerRecovery(before.into())
    }
}

#[derive(Default, SpecCase)]
struct Case100_5 {}

impl SpecCase for Case100_5 {
    fn before(&self) -> State {
        State::TimerRecovery(Inner::default().into())
    }

    fn signal(&self) -> Received {
        Received::Primitive(In::UnitDataRequest(test_info(8)))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Frame(
            UnnumberedInformation {
                c_or_r: Some(CommandResponse::Command),
                p_or_f: true,
                info: test_info(8),
            }
            .into(),
        )]
    }

    fn after(&self, before: Inner) -> State {
        State::TimerRecovery(before.into())
    }
}

#[derive(Default, SpecCase)]
struct Case100_6YYY {}

impl SpecCase for Case100_6YYY {
    fn before(&self) -> State {
        State::TimerRecovery(Inner::default().increase_vs().increase_va().into())
    }

    fn signal(&self) -> Received {
        Received::Frame(
            Reject {
                c_or_r: Some(CommandResponse::Response),
                p_or_f: true,
                receive_sequence: 1,
            }
            .into(),
        )
    }

    fn after(&self, before: Inner) -> State {
        State::Connected(
            before
                .stop_timer_1()
                .select_timer_1_value()
                .start_timer_3()
                .into(),
        )
    }
}

#[derive(Default, SpecCase)]
struct Case100_6YYN {}

impl SpecCase for Case100_6YYN {
    fn before(&self) -> State {
        State::TimerRecovery(
            Inner::default()
                .save_pending_ack(0.into(), test_info(8))
                .increase_vs()
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(
            Reject {
                c_or_r: Some(CommandResponse::Response),
                p_or_f: true,
                receive_sequence: 0,
            }
            .into(),
        )
    }

    fn after(&self, before: Inner) -> State {
        State::TimerRecovery(
            before
                .select_timer_1_value()
                .remove_pending_ack(0.into())
                .0
                .queued(test_info(8))
                .into(),
        )
    }
}

#[derive(Default, SpecCase)]
struct Case100_6YN {}

impl SpecCase for Case100_6YN {
    fn before(&self) -> State {
        State::TimerRecovery(Inner::default().into())
    }

    fn signal(&self) -> Received {
        Received::Frame(
            Reject {
                c_or_r: Some(CommandResponse::Response),
                p_or_f: true,
                receive_sequence: 1,
            }
            .into(),
        )
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Primitive(Out::ErrorIndication(ErrorCode::J)),
            Generated::Frame(SetAsynchronousBalancedMode { p_or_f: true }.into()),
        ]
    }

    fn after(&self, before: Inner) -> State {
        State::AwaitingConnection(
            before
                .stop_timer_1()
                .select_timer_1_value()
                .start_timer_1()
                .into(),
        )
    }
}

#[derive(Default, SpecCase)]
struct Case100_6NNYY {}

impl SpecCase for Case100_6NNYY {
    fn before(&self) -> State {
        State::TimerRecovery(Inner::default().increase_vs().into())
    }

    fn signal(&self) -> Received {
        Received::Frame(
            Reject {
                c_or_r: Some(CommandResponse::Command),
                p_or_f: false,
                receive_sequence: 1,
            }
            .into(),
        )
    }

    fn after(&self, before: Inner) -> State {
        State::TimerRecovery(before.increase_va().into())
    }
}

#[derive(Default, SpecCase)]
struct Case100_6NYYY {}

impl SpecCase for Case100_6NYYY {
    fn before(&self) -> State {
        State::TimerRecovery(Inner::default().increase_vs().into())
    }

    fn signal(&self) -> Received {
        Received::Frame(
            Reject {
                c_or_r: Some(CommandResponse::Command),
                p_or_f: false,
                receive_sequence: 1,
            }
            .into(),
        )
    }

    fn after(&self, before: Inner) -> State {
        State::TimerRecovery(before.increase_va().into())
    }
}

#[derive(Default, SpecCase)]
struct Case100_6NNN {}

impl SpecCase for Case100_6NNN {
    fn before(&self) -> State {
        State::TimerRecovery(Inner::default().select_timer_1_value().into())
    }

    fn signal(&self) -> Received {
        Received::Frame(
            Reject {
                c_or_r: Some(CommandResponse::Command),
                p_or_f: false,
                receive_sequence: 1,
            }
            .into(),
        )
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Primitive(Out::ErrorIndication(ErrorCode::J)),
            Generated::Frame(SetAsynchronousBalancedMode { p_or_f: true }.into()),
        ]
    }

    fn after(&self, before: Inner) -> State {
        State::AwaitingConnection(before.start_timer_1().into())
    }
}

#[derive(Default, SpecCase)]
struct Case100_6NNYN {}

impl SpecCase for Case100_6NNYN {
    fn before(&self) -> State {
        State::TimerRecovery(
            Inner::default()
                .save_pending_ack(0.into(), new_info(b'a', 8))
                .save_pending_ack(1.into(), new_info(b'b', 8))
                .increase_vs()
                .increase_vs()
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(
            Reject {
                c_or_r: Some(CommandResponse::Command),
                p_or_f: false,
                receive_sequence: 1,
            }
            .into(),
        )
    }

    fn after(&self, before: Inner) -> State {
        State::TimerRecovery(
            before
                .remove_pending_ack(0.into())
                .0
                .remove_pending_ack(1.into())
                .0
                .increase_va()
                .queued(new_info(b'b', 8))
                .into(),
        )
    }
}
