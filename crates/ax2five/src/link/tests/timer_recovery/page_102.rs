use super::*;

#[derive(Default, SpecCase)]
struct Case102_1N {}

impl SpecCase for Case102_1N {
    fn before(&self) -> State {
        State::TimerRecovery(Inner::default().into())
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::Information(Information {
            c_or_r: Some(CommandResponse::Response),
            poll: false,
            info: test_info(8),
            send_sequence: 0,
            receive_sequence: 0,
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Primitive(Out::ErrorIndication(ErrorCode::S))]
    }

    fn after(&self, before: Inner) -> State {
        State::TimerRecovery(before.into())
    }
}

// Is Case92_2YN necessary?

#[derive(Default, SpecCase)]
struct Case102_1YYN {}

impl SpecCase for Case102_1YYN {
    fn before(&self) -> State {
        State::TimerRecovery(Inner::default().select_timer_1_value().into())
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::Information(Information {
            c_or_r: Some(CommandResponse::Command),
            poll: false,
            info: test_info(8),
            send_sequence: 0,
            receive_sequence: 2,
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Primitive(Out::ErrorIndication(ErrorCode::J)),
            Generated::Frame(Frame::SetAsynchronousBalancedMode(
                SetAsynchronousBalancedMode { p_or_f: true },
            )),
        ]
    }

    fn after(&self, before: Inner) -> State {
        State::AwaitingConnection(before.start_timer_1().into())
    }
}

#[derive(Default, SpecCase)]
struct Case102_1YYYYNotP {}

impl SpecCase for Case102_1YYYYNotP {
    fn before(&self) -> State {
        State::TimerRecovery(
            Inner::default()
                .start_timer_3()
                .select_timer_1_value()
                .set_own_busy()
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::Information(Information {
            c_or_r: Some(CommandResponse::Command),
            poll: false,
            info: test_info(8),
            send_sequence: 0,
            receive_sequence: 0,
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![]
    }

    fn after(&self, before: Inner) -> State {
        State::TimerRecovery(before.into())
    }
}

#[derive(Default, SpecCase)]
struct Case102_1YYYY {}

impl SpecCase for Case102_1YYYY {
    fn before(&self) -> State {
        State::TimerRecovery(
            Inner::default()
                .start_timer_3()
                .select_timer_1_value()
                .set_own_busy()
                .set_acknowledge_pending()
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::Information(Information {
            c_or_r: Some(CommandResponse::Command),
            poll: true,
            info: test_info(8),
            send_sequence: 0,
            receive_sequence: 0,
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Primitive(Out::AcknowledgePending(false)),
            Generated::Frame(Frame::ReceiveNotReady(ReceiveNotReady {
                c_or_r: Some(CommandResponse::Response),
                p_or_f: true,
                receive_sequence: 0,
            })),
        ]
    }

    fn after(&self, before: Inner) -> State {
        State::TimerRecovery(before.clear_acknowledge_pending().into())
    }
}

#[derive(Default, SpecCase)]
struct Case102_1YYYNYNonZero {}

impl SpecCase for Case102_1YYYNYNonZero {
    fn before(&self) -> State {
        State::TimerRecovery(
            Inner::default()
                .increase_vr()
                .start_timer_3()
                .select_timer_1_value()
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::Information(Information {
            c_or_r: Some(CommandResponse::Command),
            poll: true,
            info: test_info(8),
            send_sequence: 1,
            receive_sequence: 0,
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Primitive(Out::DataIndication(test_info(8))),
            Generated::Frame(Frame::ReceiveReady(ReceiveReady {
                c_or_r: Some(CommandResponse::Response),
                p_or_f: true,
                receive_sequence: 2,
            })),
        ]
    }

    fn after(&self, before: Inner) -> State {
        State::TimerRecovery(before.increase_vr().into())
    }
}

#[derive(Default, SpecCase)]
struct Case102_1YYYNN {}

impl SpecCase for Case102_1YYYNN {
    fn before(&self) -> State {
        State::TimerRecovery(
            Inner::default()
                .increase_vr()
                .start_timer_3()
                .select_timer_1_value()
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::Information(Information {
            c_or_r: Some(CommandResponse::Command),
            poll: true,
            info: test_info(8),
            send_sequence: 0,
            receive_sequence: 0,
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Frame(Frame::Reject(Reject {
            c_or_r: Some(CommandResponse::Command),
            p_or_f: true,
            receive_sequence: 1,
        }))]
    }

    fn after(&self, before: Inner) -> State {
        State::TimerRecovery(before.set_reject_exception().into())
    }
}

#[derive(Default, SpecCase)]
struct Case102_1YYYNY {}

impl SpecCase for Case102_1YYYNY {
    fn before(&self) -> State {
        State::TimerRecovery(
            Inner::default()
                .increase_vr()
                .start_timer_3()
                .select_timer_1_value()
                .set_reject_exception()
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::Information(Information {
            c_or_r: Some(CommandResponse::Command),
            poll: true,
            info: test_info(8),
            send_sequence: 0,
            receive_sequence: 0,
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Frame(Frame::ReceiveReady(ReceiveReady {
            c_or_r: Some(CommandResponse::Command),
            p_or_f: true,
            receive_sequence: 1,
        }))]
    }

    fn after(&self, before: Inner) -> State {
        State::TimerRecovery(before.into())
    }
}

#[derive(Default, SpecCase)]
struct Case102_1YYYYY {}

impl SpecCase for Case102_1YYYYY {
    fn before(&self) -> State {
        State::TimerRecovery(
            Inner::default()
                .start_timer_3()
                .select_timer_1_value()
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::Information(Information {
            c_or_r: Some(CommandResponse::Command),
            poll: true,
            info: test_info(8),
            send_sequence: 0,
            receive_sequence: 0,
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Primitive(Out::DataIndication(test_info(8))),
            Generated::Frame(Frame::ReceiveReady(ReceiveReady {
                c_or_r: Some(CommandResponse::Response),
                p_or_f: true,
                receive_sequence: 1,
            })),
        ]
    }

    fn after(&self, before: Inner) -> State {
        State::TimerRecovery(before.increase_vr().clear_reject_exception().into())
    }
}

#[derive(Default, SpecCase)]
struct Case102_1YYYYYNoPoll {}

impl SpecCase for Case102_1YYYYYNoPoll {
    fn before(&self) -> State {
        State::TimerRecovery(
            Inner::default()
                .start_timer_3()
                .select_timer_1_value()
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::Information(Information {
            c_or_r: Some(CommandResponse::Command),
            poll: false,
            info: test_info(8),
            send_sequence: 0,
            receive_sequence: 0,
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Primitive(Out::DataIndication(test_info(8))),
            Generated::Primitive(Out::AcknowledgePending(true)),
        ]
    }

    fn after(&self, before: Inner) -> State {
        State::TimerRecovery(before.increase_vr().set_acknowledge_pending().into())
    }
}

// After incrementing vr in receive, the queued frames are checked and then Indicated.
#[derive(Default, SpecCase)]
struct Case102_1YYYYYAfterSrej {}

impl SpecCase for Case102_1YYYYYAfterSrej {
    fn before(&self) -> State {
        State::TimerRecovery(
            Inner::default()
                .start_timer_3()
                .select_timer_1_value()
                .save_incoming(Seq::new_modulo_8(1), test_info(8))
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::Information(Information {
            c_or_r: Some(CommandResponse::Command),
            poll: true,
            info: test_info(8),
            send_sequence: 0,
            receive_sequence: 0,
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Primitive(Out::DataIndication(test_info(8))),
            Generated::Primitive(Out::DataIndication(test_info(8))),
            Generated::Frame(Frame::ReceiveReady(ReceiveReady {
                c_or_r: Some(CommandResponse::Response),
                p_or_f: true,
                receive_sequence: 2,
            })),
        ]
    }

    fn after(&self, before: Inner) -> State {
        State::TimerRecovery(
            before
                .remove_incoming(Seq::new_modulo_8(1))
                .0
                .increase_vr()
                .increase_vr()
                .clear_reject_exception()
                .into(),
        )
    }
}

#[derive(Default, SpecCase)]
struct Case102_1YYYYNotPAndAcksSent {}

impl SpecCase for Case102_1YYYYNotPAndAcksSent {
    fn before(&self) -> State {
        State::Connected(
            Inner::default()
                .increase_vs()
                .select_timer_1_value()
                .start_timer_1()
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::Information(Information {
            c_or_r: Some(CommandResponse::Command),
            poll: false,
            info: test_info(8),
            send_sequence: 0,
            receive_sequence: 1,
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Primitive(Out::DataIndication(test_info(8))),
            Generated::Primitive(Out::AcknowledgePending(true)),
        ]
    }

    fn after(&self, before: Inner) -> State {
        State::Connected(
            before
                .increase_va()
                .increase_vr()
                .stop_timer_1()
                .start_timer_3()
                .set_acknowledge_pending()
                .into(),
        )
    }
}

#[derive(Default, SpecCase)]
struct Case102_0Acknowledge {}

impl SpecCase for Case102_0Acknowledge {
    fn before(&self) -> State {
        State::TimerRecovery(
            Inner::default()
                .increase_vr()
                .select_timer_1_value()
                .start_timer_1()
                .set_acknowledge_pending()
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Primitive(In::Acknowledge)
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Primitive(Out::AcknowledgePending(false)),
            Generated::Frame(Frame::ReceiveReady(ReceiveReady {
                c_or_r: Some(CommandResponse::Response),
                p_or_f: false,
                receive_sequence: 1,
            })),
        ]
    }

    fn after(&self, before: Inner) -> State {
        State::TimerRecovery(before.clear_acknowledge_pending().into())
    }
}

#[derive(Default, SpecCase)]
struct Case102_0AcknowledgeNonPending {}

impl SpecCase for Case102_0AcknowledgeNonPending {
    fn before(&self) -> State {
        State::TimerRecovery(
            Inner::default()
                .increase_vr()
                .select_timer_1_value()
                .start_timer_1()
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Primitive(In::Acknowledge)
    }

    fn generated(&self) -> Vec<Generated> {
        vec![]
    }

    fn after(&self, before: Inner) -> State {
        State::TimerRecovery(before.into())
    }
}
