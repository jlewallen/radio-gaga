use super::*;

#[derive(Default, SpecCase)]
struct Case91_1N {}

impl SpecCase for Case91_1N {
    fn before(&self) -> State {
        State::AwaitingRelease(Inner::default().into())
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::DisconnectedMode(DisconnectedMode {
            is_final: false,
        }))
    }

    fn after(&self, before: Inner) -> State {
        State::AwaitingRelease(before.into())
    }
}

#[derive(Default, SpecCase)]
struct Case91_1Y {}

impl SpecCase for Case91_1Y {
    fn before(&self) -> State {
        State::AwaitingRelease(
            Inner::default()
                .select_timer_1_value()
                .start_timer_1()
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::DisconnectedMode(DisconnectedMode { is_final: true }))
    }

    fn generated(&self) -> Vec<Generated> {
        // Spec has this has ConnectConfirm, that seems wrong. #badspec
        vec![Generated::Primitive(Out::DisconnectConfirm)]
    }

    fn after(&self, before: Inner) -> State {
        State::Disconnected(before.stop_timer_1().into())
    }
}

#[derive(Default, SpecCase)]
struct Case91_2Y {}

impl SpecCase for Case91_2Y {
    fn before(&self) -> State {
        State::AwaitingRelease(Inner::default().retry_count_of(2, 2).into())
    }

    fn signal(&self) -> Received {
        Received::Primitive(In::Timer(StandardTimer::T1))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Primitive(Out::ErrorIndication(ErrorCode::H)),
            Generated::Primitive(Out::DisconnectConfirm),
        ]
    }

    fn after(&self, before: Inner) -> State {
        State::Disconnected(before.into())
    }
}

#[derive(Default, SpecCase)]
struct Case91_2N {}

impl SpecCase for Case91_2N {
    fn before(&self) -> State {
        State::AwaitingRelease(Inner::default().retry_count_of(1, 2).into())
    }

    fn signal(&self) -> Received {
        Received::Primitive(In::Timer(StandardTimer::T1))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Frame(Frame::Disconnect(Disconnect {
            poll: true,
        }))]
    }

    fn after(&self, before: Inner) -> State {
        State::AwaitingRelease(
            before
                .retry_count(2)
                .select_timer_1_value()
                .start_timer_1()
                .into(),
        )
    }
}
