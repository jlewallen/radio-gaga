use super::*;

#[derive(Default, SpecCase)]
struct Case89_4 {}

impl SpecCase for Case89_4 {
    fn before(&self) -> State {
        State::AwaitingRelease(
            Inner::default()
                .select_timer_1_value()
                .start_timer_1()
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Primitive(In::DisconnectRequest)
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Frame(Frame::DisconnectedMode(
            DisconnectedMode { is_final: true },
        ))]
    }

    fn after(&self, before: Inner) -> State {
        State::Disconnected(before.stop_timer_1().into())
    }
}

#[derive(Default, SpecCase)]
struct Case89_5 {}

impl SpecCase for Case89_5 {
    fn before(&self) -> State {
        State::AwaitingRelease(Inner::default().into())
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::SetAsynchronousBalancedMode(
            SetAsynchronousBalancedMode { p_or_f: true },
        ))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Frame(Frame::DisconnectedMode(
            DisconnectedMode { is_final: true },
        ))]
    }

    fn after(&self, before: Inner) -> State {
        State::AwaitingRelease(before.into())
    }
}

#[derive(Default, SpecCase)]
struct Case89_6 {}

impl SpecCase for Case89_6 {
    fn before(&self) -> State {
        State::AwaitingRelease(Inner::default().into())
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::Disconnect(Disconnect { poll: true }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Frame(Frame::UnnumberedAcknowledge(
            UnnumberedAcknowledge { is_final: true },
        ))]
    }

    fn after(&self, before: Inner) -> State {
        State::AwaitingRelease(before.into())
    }
}

#[derive(Default, SpecCase)]
struct Case89_7 {}

impl SpecCase for Case89_7 {
    fn before(&self) -> State {
        State::AwaitingRelease(Inner::default().into())
    }

    fn signal(&self) -> Received {
        Received::Primitive(In::UnitDataRequest(test_info(8)))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Frame(Frame::UnnumberedInformation(
            UnnumberedInformation {
                c_or_r: Some(CommandResponse::Command),
                p_or_f: true,
                info: test_info(8),
            },
        ))]
    }

    fn after(&self, before: Inner) -> State {
        State::AwaitingRelease(before.into())
    }
}
