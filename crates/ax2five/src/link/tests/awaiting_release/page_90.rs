use super::*;

#[derive(Default, SpecCase)]
struct Case90_2N {}

impl SpecCase for Case90_2N {
    fn before(&self) -> State {
        State::AwaitingRelease(Inner::default().into())
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::UnnumberedInformation(UnnumberedInformation {
            c_or_r: Some(CommandResponse::Command),
            p_or_f: false,
            info: test_info(8),
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Primitive(Out::UnitDataIndication(test_info(8)))]
    }

    fn after(&self, before: Inner) -> State {
        State::AwaitingRelease(before.into())
    }
}

#[derive(Default, SpecCase)]
struct Case90_2Y {}

impl SpecCase for Case90_2Y {
    fn before(&self) -> State {
        State::AwaitingRelease(Inner::default().into())
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::UnnumberedInformation(UnnumberedInformation {
            c_or_r: Some(CommandResponse::Command),
            p_or_f: true,
            info: test_info(8),
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Primitive(Out::UnitDataIndication(test_info(8))),
            Frame::DisconnectedMode(DisconnectedMode { is_final: true }).into(),
        ]
    }

    fn after(&self, before: Inner) -> State {
        State::AwaitingRelease(before.into())
    }
}

#[derive(Default, SpecCase)]
struct Case90_5N {}

impl SpecCase for Case90_5N {
    fn before(&self) -> State {
        State::AwaitingRelease(Inner::default().into())
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::UnnumberedAcknowledge(UnnumberedAcknowledge {
            is_final: false,
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Primitive(Out::ErrorIndication(ErrorCode::D))]
    }

    fn after(&self, before: Inner) -> State {
        State::AwaitingRelease(before.into())
    }
}

#[derive(Default, SpecCase)]
struct Case90_5Y {}

impl SpecCase for Case90_5Y {
    fn before(&self) -> State {
        State::AwaitingRelease(
            Inner::default()
                .select_timer_1_value()
                .start_timer_1()
                .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::UnnumberedAcknowledge(UnnumberedAcknowledge {
            is_final: true,
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Primitive(Out::DisconnectConfirm)]
    }

    fn after(&self, before: Inner) -> State {
        State::Disconnected(before.stop_timer_1().into())
    }
}
