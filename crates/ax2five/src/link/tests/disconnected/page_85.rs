use super::*;

#[derive(Default, SpecCase)]
struct Case85_4 {}

impl SpecCase for Case85_4 {
    fn before(&self) -> State {
        State::Disconnected(Default::default())
    }

    fn signal(&self) -> Received {
        Received::Primitive(In::ConnectRequest)
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Frame(Frame::SetAsynchronousBalancedMode(
            SetAsynchronousBalancedMode { p_or_f: true },
        ))]
    }

    fn after(&self, _before: Inner) -> State {
        State::AwaitingConnection(
            Inner::default()
                .layer_3_initiated(true)
                .set_srt_and_timer_1_value()
                .start_timer_1()
                .into(),
        )
    }
}

#[derive(Default, SpecCase)]
struct Case85_5Y {}

impl SpecCase for Case85_5Y {
    fn before(&self) -> State {
        State::Disconnected(Default::default())
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::SetAsynchronousBalancedMode(
            SetAsynchronousBalancedMode { p_or_f: true },
        ))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Frame(Frame::UnnumberedAcknowledge(UnnumberedAcknowledge {
                is_final: true,
            })),
            Generated::Primitive(Out::ConnectIndication),
        ]
    }

    fn after(&self, _before: Inner) -> State {
        State::Connected(
            Inner::default()
                .set_srt_and_timer_1_value()
                .start_timer_3()
                .into(),
        )
    }
}

#[derive(Default, SpecCase)]
struct Case85_5N {}

impl SpecCase for Case85_5N {
    fn before(&self) -> State {
        State::Disconnected(
            Inner {
                flags: Flags {
                    unable_to_establish: true,
                    ..Default::default()
                },
                ..Default::default()
            }
            .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::SetAsynchronousBalancedMode(
            SetAsynchronousBalancedMode { p_or_f: true },
        ))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Frame(Frame::DisconnectedMode(
            DisconnectedMode { is_final: true },
        ))]
    }

    fn after(&self, before: Inner) -> State {
        State::Disconnected(before.into())
    }
}

#[derive(Default, SpecCase)]
struct Case85_6Y {}

impl SpecCase for Case85_6Y {
    fn before(&self) -> State {
        State::Disconnected(Disconnected::default())
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::SetAsynchronousBalancedModeExtended(
            SetAsynchronousBalancedModeExtended { p_or_f: true },
        ))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Frame(Frame::UnnumberedAcknowledge(UnnumberedAcknowledge {
                is_final: true,
            })),
            Generated::Primitive(Out::ConnectIndication),
        ]
    }

    fn after(&self, _before: Inner) -> State {
        State::Connected(
            Inner::default()
                .version(Version::Ax22)
                .set_srt_and_timer_1_value()
                .start_timer_3()
                .into(),
        )
    }
}

#[derive(Default, SpecCase)]
struct Case85_6N {}

impl SpecCase for Case85_6N {
    fn before(&self) -> State {
        State::Disconnected(
            Inner {
                flags: Flags {
                    unable_to_establish: true,
                    ..Default::default()
                },
                ..Default::default()
            }
            .into(),
        )
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::SetAsynchronousBalancedModeExtended(
            SetAsynchronousBalancedModeExtended { p_or_f: true },
        ))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Frame(Frame::DisconnectedMode(
            DisconnectedMode { is_final: true },
        ))]
    }

    fn after(&self, before: Inner) -> State {
        State::Disconnected(before.into())
    }
}
