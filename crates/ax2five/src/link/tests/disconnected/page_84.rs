use super::*;

#[derive(Default, SpecCase)]
struct Case84_4 {}

impl SpecCase for Case84_4 {
    fn before(&self) -> State {
        State::Disconnected(Default::default())
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::UnnumberedAcknowledge(UnnumberedAcknowledge {
            is_final: true,
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Primitive(Out::ErrorIndication(ErrorCode::C)),
            Generated::Primitive(Out::ErrorIndication(ErrorCode::D)),
        ]
    }

    fn after(&self, _before: Inner) -> State {
        State::Disconnected(Disconnected::default())
    }
}

#[derive(Default, SpecCase)]
struct Case84_5 {}

impl SpecCase for Case84_5 {
    fn before(&self) -> State {
        State::Disconnected(Default::default())
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::DisconnectedMode(DisconnectedMode { is_final: true }))
    }

    fn after(&self, _before: Inner) -> State {
        State::Disconnected(Disconnected::default())
    }
}

#[derive(Default, SpecCase)]
struct Case84_6N {}

impl SpecCase for Case84_6N {
    fn before(&self) -> State {
        State::Disconnected(Inner::default().into())
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::UnnumberedInformation(UnnumberedInformation {
            c_or_r: Some(CommandResponse::Command),
            p_or_f: false,
            info: test_info(8),
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Primitive(Out::UnitDataIndication(test_info(8)))]
    }

    fn after(&self, before: Inner) -> State {
        State::Disconnected(before.into())
    }
}

#[derive(Default, SpecCase)]
struct Case84_6Y {}

impl SpecCase for Case84_6Y {
    fn before(&self) -> State {
        State::Disconnected(Inner::default().into())
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::UnnumberedInformation(UnnumberedInformation {
            c_or_r: Some(CommandResponse::Command),
            p_or_f: true,
            info: test_info(8),
        }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![
            Generated::Primitive(Out::UnitDataIndication(test_info(8))),
            Frame::DisconnectedMode(DisconnectedMode { is_final: true }).into(),
        ]
    }

    fn after(&self, before: Inner) -> State {
        State::Disconnected(before.into())
    }
}

#[derive(Default, SpecCase)]
struct Case84_7 {}

impl SpecCase for Case84_7 {
    fn before(&self) -> State {
        State::Disconnected(Inner::default().into())
    }

    fn signal(&self) -> Received {
        Received::Primitive(In::DisconnectRequest)
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Primitive(Out::DisconnectConfirm)]
    }

    fn after(&self, before: Inner) -> State {
        State::Disconnected(before.into())
    }
}

#[derive(Default, SpecCase)]
struct Case84_8 {}

impl SpecCase for Case84_8 {
    fn before(&self) -> State {
        State::Disconnected(Inner::default().into())
    }

    fn signal(&self) -> Received {
        Received::Frame(Frame::Disconnect(Disconnect { poll: true }))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Frame(Frame::DisconnectedMode(
            DisconnectedMode { is_final: true },
        ))]
    }

    fn after(&self, before: Inner) -> State {
        State::Disconnected(before.into())
    }
}

#[derive(Default, SpecCase)]
struct Case84_9 {}

impl SpecCase for Case84_9 {
    fn before(&self) -> State {
        State::Disconnected(Inner::default().into())
    }

    fn signal(&self) -> Received {
        Received::Primitive(In::UnitDataRequest(test_info(8)))
    }

    fn generated(&self) -> Vec<Generated> {
        vec![Generated::Frame(Frame::UnnumberedInformation(
            UnnumberedInformation {
                c_or_r: Some(CommandResponse::Command),
                p_or_f: true,
                info: test_info(8),
            },
        ))]
    }

    fn after(&self, before: Inner) -> State {
        State::Disconnected(before.into())
    }
}
