pub use super::*;

mod page_86;
mod page_87;
mod page_88;

#[derive(Default, SpecCase)]
struct Case0_0AcknowledgeNonPending {}

impl SpecCase for Case0_0AcknowledgeNonPending {
    fn before(&self) -> State {
        State::AwaitingConnection(Inner::default().into())
    }

    fn signal(&self) -> Received {
        Received::Primitive(In::Acknowledge)
    }

    fn after(&self, before: Inner) -> State {
        State::AwaitingConnection(before.into())
    }
}

/// Should rarely happen, and the spec appears a little ambiguous on this. I
/// think it would be better if kept the packet. Chances are we could easily recover.
/// I've only seen this once in testing, so far, though.
#[derive(Default, SpecCase)]
struct Case0_0InformationIsIgnored {}

impl SpecCase for Case0_0InformationIsIgnored {
    fn before(&self) -> State {
        State::AwaitingConnection(Inner::default().into())
    }

    fn signal(&self) -> Received {
        Received::Frame(
            Information {
                c_or_r: Some(CommandResponse::Command),
                poll: true,
                info: test_info(8),
                send_sequence: 0,
                receive_sequence: 0,
            }
            .into(),
        )
    }

    fn after(&self, before: Inner) -> State {
        State::AwaitingConnection(before.into())
    }
}
