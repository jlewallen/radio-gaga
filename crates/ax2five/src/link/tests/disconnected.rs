pub use super::*;

mod page_84;
mod page_85;

#[derive(Default, SpecCase)]
struct Case0_0AcknowledgeNonPending {}

impl SpecCase for Case0_0AcknowledgeNonPending {
    fn before(&self) -> State {
        State::Disconnected(Inner::default().into())
    }

    fn signal(&self) -> Received {
        Received::Primitive(In::Acknowledge)
    }

    fn after(&self, before: Inner) -> State {
        State::Disconnected(before.into())
    }
}
