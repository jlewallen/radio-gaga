pub use super::*;

mod page_89;
mod page_90;
mod page_91;

#[derive(Default, SpecCase)]
struct Case0_0AcknowledgeNonPending {}

impl SpecCase for Case0_0AcknowledgeNonPending {
    fn before(&self) -> State {
        State::AwaitingRelease(Inner::default().into())
    }

    fn signal(&self) -> Received {
        Received::Primitive(In::Acknowledge)
    }

    fn after(&self, before: Inner) -> State {
        State::AwaitingRelease(before.into())
    }
}
