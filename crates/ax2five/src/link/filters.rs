use ax25::frame::{Address, AddressParseError, Ax25Frame};

pub trait FrameFilter {
    fn handle(&self, frame: Ax25Frame) -> Option<Ax25Frame>;
}

#[derive(Default)]
pub struct AllowAll {}

impl FrameFilter for AllowAll {
    fn handle(&self, frame: Ax25Frame) -> Option<Ax25Frame> {
        Some(frame)
    }
}

pub struct SingleDestination {
    address: Address,
}

impl SingleDestination {
    pub fn new(callsign: &str) -> Result<Self, AddressParseError> {
        Ok(Self {
            address: callsign.parse()?,
        })
    }
}

impl FrameFilter for SingleDestination {
    fn handle(&self, frame: Ax25Frame) -> Option<Ax25Frame> {
        if same_addresses(&frame.destination, &self.address) {
            Some(frame)
        } else {
            None
        }
    }
}

pub(crate) fn same_addresses(a: &Address, b: &Address) -> bool {
    // On transmit, c_bit is set which causes this to fail
    // depending... on that. TODO
    a.callsign == b.callsign && a.ssid == b.ssid
}
