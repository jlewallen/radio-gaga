use tracing::*;

use super::{inner::TimerValue, sched::SchedulerIn};

pub trait AcknowledgePending: Default {
    fn queue(&mut self);
    fn clear(&mut self);
    fn elapsed(&mut self, elapsed: std::time::Duration) -> Vec<SchedulerIn>;
}

#[derive(Default)]
pub struct AcknowledgeOnlyOnPoll {}

impl AcknowledgePending for AcknowledgeOnlyOnPoll {
    fn queue(&mut self) {}

    fn clear(&mut self) {}

    #[allow(unused_variables)]
    fn elapsed(&mut self, elapsed: std::time::Duration) -> Vec<SchedulerIn> {
        vec![]
    }
}

pub const DEFAULT_T2: i64 = 3000;

#[derive(Default)]
pub struct AcknowledgeAfterT2 {
    /// T2, the Response Delay Timer, may optionally be implemented by the TNC
    /// to specify a maximum amount of delay to be introduced between the time
    /// an I frame is received and the time the resulting response frame is
    /// sent.  This delay is introduced to allow a receiving TNC to wait a short
    /// period of time to determine if more than one frame is being sent to it.
    /// If more frames are received, the TNC can acknowledge them at once (up to
    /// seven), rather than acknowledging each individual frame. The use of
    /// timer T2 is not required; it is simply recommended to improve channel
    /// efficiency. Note that to achieve maximum throughput on full-duplex
    /// channels, acknowledgments should not be delayed beyond k/2 frames. The k
    /// parameter is defined in Section 6.8.2.3.
    timer_2: Option<TimerValue>,
}

impl AcknowledgeAfterT2 {
    pub fn started() -> Self {
        debug!("t2:started");
        Self {
            timer_2: Some(DEFAULT_T2.into()),
        }
    }
}

impl AcknowledgePending for AcknowledgeAfterT2 {
    fn queue(&mut self) {
        debug!("t2:started");
        self.timer_2 = Some(DEFAULT_T2.into());
    }

    fn clear(&mut self) {
        if self.timer_2.is_some() {
            debug!("t2:stopped");
            self.timer_2 = None;
        }
    }

    fn elapsed(&mut self, elapsed: std::time::Duration) -> Vec<SchedulerIn> {
        if let Some(timer) = self.timer_2 {
            match timer.elapsed(elapsed) {
                Some(timer) => {
                    self.timer_2 = Some(timer);
                    vec![]
                }
                None => {
                    self.timer_2 = None;
                    vec![SchedulerIn::Acknowledge]
                }
            }
        } else {
            vec![]
        }
    }
}
