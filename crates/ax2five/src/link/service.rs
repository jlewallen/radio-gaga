use itertools::Itertools;
use std::time::Duration;
use thiserror::Error;
use tokio::time::timeout;
use tracing::*;

use crate::{
    AcknowledgeAfterT2, AcknowledgePending, FrameFilter, Scheduler, SchedulerIn, TickOut, Transport,
};
use ax25::frame::Ax25Frame;

use super::{SocketEvent, TimerExpiration};

#[allow(dead_code)]
trait Middleware<Value> {
    type Out;
    type Error;

    fn middleware(&mut self, value: Value) -> Result<Self::Out, Self::Error>;
}

pub trait Adapter<TV>: TimerExpiration {
    #[allow(unused_variables)]
    fn elapsed(&mut self, elapsed: std::time::Duration) -> Vec<SchedulerIn> {
        vec![]
    }

    fn send(&mut self, value: SocketEvent) -> Option<TV>;
    fn receive(&mut self, value: TV) -> Option<SchedulerIn>;
}

pub struct FramesOnlyAdapter<F> {
    filter: F,
    t2_timer: AcknowledgeAfterT2,
}

impl<F: FrameFilter> FramesOnlyAdapter<F> {
    pub fn new(filter: F) -> Self {
        Self {
            filter,
            t2_timer: Default::default(),
        }
    }
}

impl<F> TimerExpiration for FramesOnlyAdapter<F> {}

impl<F: FrameFilter> Adapter<Ax25Frame> for FramesOnlyAdapter<F> {
    fn elapsed(&mut self, elapsed: std::time::Duration) -> Vec<SchedulerIn> {
        self.t2_timer.elapsed(elapsed)
    }

    fn send(&mut self, value: SocketEvent) -> Option<Ax25Frame> {
        match value.event() {
            TickOut::Send(frame) => Some(frame),
            TickOut::AwaitingAck(_) => {
                self.t2_timer.queue();
                None
            }
            _ => None,
        }
    }

    fn receive(&mut self, value: Ax25Frame) -> Option<SchedulerIn> {
        self.filter
            .handle(value)
            .map(|frame| SchedulerIn::Received(frame.into()))
    }
}

#[allow(async_fn_in_trait)]
pub trait Service<TValue, TPort, TAdapt>
where
    TPort: Transport<TValue>,
{
    type Error;

    async fn service(
        &mut self,
        transport: &mut TPort,
        adapter: &mut TAdapt,
    ) -> Result<Vec<SocketEvent>, Self::Error>;
}

impl<TValue, TPort, TAdapt> Service<TValue, TPort, TAdapt> for Scheduler
where
    TPort: Transport<TValue>,
    TAdapt: Adapter<TValue>,
    TPort::Error: std::error::Error + Send + Sync + 'static,
{
    type Error = TickError<TPort::Error>;

    async fn service(
        &mut self,
        transport: &mut TPort,
        adapter: &mut TAdapt,
    ) -> Result<Vec<SocketEvent>, Self::Error> {
        loop {
            let elapsed = self.update_clock();

            let _span = span!(Level::INFO, "service", up = ?self.uptime()).entered();

            for signal in adapter.elapsed(elapsed).into_iter() {
                self.received(signal)?;
            }

            let events = self.tick(elapsed.into())?;
            for event in events.iter() {
                if let Some(frame) = adapter.send(event.clone()) {
                    transport.send(frame).await.map_err(TickError::Transport)?;
                }
            }

            // If we have any events the caller should be aware of, send them back.
            if !events.is_empty() {
                return Ok(events);
            }

            // Await a packet/frame but don't wait so long that we'll miss the
            // next timer expiration.
            let delay = [self.timer_expiration(), adapter.timer_expiration()]
                .into_iter()
                .flatten()
                .sorted()
                .next()
                .unwrap_or(Duration::from_millis(100));
            trace!("{:?}", self.timer_expiration());
            let received = timeout(delay, transport.receive()).await;
            if let Ok(received) = received {
                if let Some(frame) = received.map_err(TickError::Transport)? {
                    if let Some(incoming) = adapter.receive(frame) {
                        self.received(incoming)?;
                    }
                }
            } else {
                break;
            }
        }

        Ok(vec![])
    }
}

#[derive(Error, Debug)]
pub enum TickError<T: std::error::Error + Sync + Send + 'static> {
    #[error("transport error")]
    Transport(T),
    #[error("data link error")]
    Scheduler(#[from] crate::link::Error),
}
