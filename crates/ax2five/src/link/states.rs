use thiserror::Error;
use tracing::*;

use super::{frames::*, inner::*};

#[derive(Debug, PartialEq, Default)]
pub(super) struct AwaitingConnection {
    inner: Inner,
}

impl AwaitingConnection {
    pub fn handle(self, signal: Received) -> Transition<State> {
        match signal {
            Received::Frame(frame) => match frame {
                Frame::Disconnect(_) => self
                    .transition()
                    .send(DisconnectedMode { is_final: true })
                    .into_state(State::AwaitingConnection),
                Frame::DisconnectedMode(frame) => {
                    if frame.is_final {
                        self.inner
                            .discard_frame_queue()
                            .stop_timer_1()
                            .transition()
                            .primitive(Out::DisconnectIndication)
                            .into_state(State::Disconnected)
                    } else {
                        Transition::new(State::AwaitingConnection(AwaitingConnection {
                            inner: self.inner,
                        }))
                    }
                }
                Frame::UnnumberedAcknowledge(frame) => {
                    if frame.is_final {
                        if self.inner.flags.layer_3_initiated {
                            self.inner
                                .stop_timer_1()
                                .stop_timer_3()
                                .clear_sequences()
                                .select_timer_1_value()
                                .transition()
                                .primitive(Out::ConnectConfirm)
                                .into_state(State::Connected)
                        } else if self.inner.sequences.vs == self.inner.sequences.va {
                            self.inner
                                .stop_timer_1()
                                .stop_timer_3()
                                .clear_sequences()
                                .select_timer_1_value()
                                .transition()
                                .into_state(State::Connected)
                        } else {
                            self.inner
                                .discard_frame_queue()
                                .stop_timer_1()
                                .stop_timer_3()
                                .clear_sequences()
                                .select_timer_1_value()
                                .transition()
                                .primitive(Out::ConnectIndication)
                                .into_state(State::Connected)
                        }
                    } else {
                        Transition::new(self.inner)
                            .error(ErrorCode::D)
                            .into_state(State::AwaitingConnection)
                    }
                }
                Frame::UnnumberedInformation(frame) => {
                    if frame.p_or_f {
                        self.transition()
                            .ui_check(frame)
                            .send(DisconnectedMode { is_final: true })
                            .into_state(State::AwaitingConnection)
                    } else {
                        self.transition()
                            .ui_check(frame)
                            .into_state(State::AwaitingConnection)
                    }
                }
                Frame::SetAsynchronousBalancedMode(_) => self
                    .inner
                    .version(Version::Ax20)
                    .transition()
                    .send(UnnumberedAcknowledge { is_final: true })
                    .into_state(State::AwaitingConnection),
                Frame::SetAsynchronousBalancedModeExtended(frame) => {
                    if self.inner.flags.version == Version::Ax20 {
                        self.inner
                            .version(Version::Ax22)
                            .transition()
                            .send(DisconnectedMode {
                                is_final: frame.p_or_f,
                            })
                            .into_state(State::AwaitingConnection)
                    } else {
                        self.inner
                            .transition()
                            .send(UnnumberedAcknowledge {
                                is_final: frame.p_or_f,
                            })
                            .into_state(State::AwaitingConnection)
                    }
                }
                Frame::FrameReject(_) => self
                    .inner
                    .version(Version::Ax20)
                    .select_timer_1_value()
                    .layer_3_initiated(true)
                    .transition()
                    .establish_data_link()
                    .into_state(State::AwaitingConnection),
                _ => self.ignored(&frame),
            },
            Received::Primitive(primitive) => match primitive {
                In::UnitDataRequest(info) => self
                    .transition()
                    .send(UnnumberedInformation {
                        c_or_r: Some(CommandResponse::Command),
                        p_or_f: true,
                        info,
                    })
                    .into_state(State::AwaitingConnection),
                In::Timer(timer) => match timer {
                    StandardTimer::T1 => {
                        if self.inner.at_retry_limit() {
                            self.inner
                                .retry_count(0)
                                .discard_frame_queue()
                                .transition()
                                .error(ErrorCode::G)
                                .primitive(Out::DisconnectIndication)
                                .into_state(State::Disconnected)
                        } else {
                            let sabm_frame = self.inner.flags.version.sabm_frame();
                            self.inner
                                .retrying()
                                .select_timer_1_value()
                                .start_timer_1()
                                .transition()
                                .send(sabm_frame)
                                .into_state(State::AwaitingConnection)
                        }
                    }
                    _ => self.ignored(&timer),
                },
                In::DataRequest(info) => self
                    .inner
                    .queue_multiple_if_necessary(info)
                    .transition()
                    .into_state(State::AwaitingConnection),
                In::ConnectRequest => self
                    .inner
                    .discard_frame_queue()
                    .layer_3_initiated(true)
                    .transition()
                    .into_state(State::AwaitingConnection),
                In::Flush => self.transition().into_state(State::AwaitingConnection),
                In::Acknowledge => Acknowledge::new(self.inner)
                    .ignored()
                    .into_state(State::AwaitingConnection),
                In::DisconnectRequest => {
                    self.ignored(&"(Page 86) DISCONNECT-REQUEST Requeue?".to_owned())
                }
                _ => self.ignored(&primitive),
            },
        }
    }
}

impl From<Inner> for AwaitingConnection {
    fn from(value: Inner) -> Self {
        Self { inner: value }
    }
}

#[derive(Debug, PartialEq, Default)]
pub(super) struct AwaitingRelease {
    pub inner: Inner,
}

impl AwaitingRelease {
    pub fn handle(self, signal: Received) -> Transition<State> {
        match signal {
            Received::Frame(frame) => match frame {
                Frame::Disconnect(frame) => self
                    .transition()
                    .send(UnnumberedAcknowledge {
                        is_final: frame.poll,
                    })
                    .into_state(State::AwaitingRelease),
                Frame::DisconnectedMode(frame) => {
                    if frame.is_final {
                        self.transition()
                            .primitive(Out::DisconnectConfirm) // Spec has this as ConnectConfirm #badspec
                            .stop_timer_1()
                            .into_state(State::Disconnected)
                    } else {
                        self.transition().into_state(State::AwaitingRelease)
                    }
                }
                Frame::UnnumberedAcknowledge(frame) => {
                    if frame.is_final {
                        self.transition()
                            .primitive(Out::DisconnectConfirm)
                            .stop_timer_1()
                            .into_state(State::Disconnected)
                    } else {
                        self.transition()
                            .error(ErrorCode::D)
                            .into_state(State::AwaitingRelease)
                    }
                }
                Frame::UnnumberedInformation(frame) => {
                    if frame.p_or_f {
                        self.transition()
                            .ui_check(frame)
                            .send(DisconnectedMode { is_final: true })
                            .into_state(State::AwaitingRelease)
                    } else {
                        self.transition()
                            .ui_check(frame)
                            .into_state(State::AwaitingRelease)
                    }
                }
                Frame::SetAsynchronousBalancedMode(frame) => self
                    .transition()
                    .send(DisconnectedMode {
                        is_final: frame.p_or_f,
                    })
                    .into_state(State::AwaitingRelease),
                _ => self.ignored(&frame),
            },
            Received::Primitive(primitive) => match primitive {
                In::DisconnectRequest => self
                    .transition()
                    .stop_timer_1()
                    .send(DisconnectedMode { is_final: true })
                    .into_state(State::Disconnected),
                In::UnitDataRequest(info) => self
                    .transition()
                    .send(UnnumberedInformation {
                        c_or_r: Some(CommandResponse::Command),
                        p_or_f: true,
                        info,
                    })
                    .into_state(State::AwaitingRelease),
                In::Timer(timer) => match timer {
                    StandardTimer::T1 => {
                        if self.inner.at_retry_limit() {
                            self.transition()
                                .primitive(Out::ErrorIndication(ErrorCode::H))
                                .primitive(Out::DisconnectConfirm)
                                .into_state(State::Disconnected)
                        } else {
                            self.transition()
                                .inner(|inner| {
                                    inner.retrying().select_timer_1_value().start_timer_1()
                                })
                                .send(Disconnect { poll: true })
                                .into_state(State::AwaitingRelease)
                        }
                    }
                    _ => self.ignored(&timer),
                },
                In::Acknowledge => Acknowledge::new(self.inner)
                    .ignored()
                    .into_state(State::AwaitingRelease),
                _ => self.ignored(&primitive),
            },
        }
    }
}

impl From<Inner> for AwaitingRelease {
    fn from(value: Inner) -> Self {
        Self { inner: value }
    }
}

#[derive(Debug, PartialEq, Default)]
pub(super) struct Connected {
    inner: Inner,
}

impl Connected {
    pub fn handle(self, signal: Received) -> Transition<State> {
        match signal {
            Received::Frame(frame) => match frame {
                Frame::Disconnect(frame) => self
                    .inner
                    .discard_frame_queue()
                    .stop_timer_1()
                    .stop_timer_3()
                    .transition()
                    .send(UnnumberedAcknowledge {
                        is_final: frame.poll,
                    })
                    .primitive(Out::DisconnectIndication)
                    .into_state(State::Disconnected),
                Frame::DisconnectedMode(_) => self
                    .inner
                    .discard_frame_queue()
                    .stop_timer_1()
                    .stop_timer_3()
                    .transition()
                    .error(ErrorCode::E)
                    .primitive(Out::DisconnectIndication)
                    .into_state(State::Disconnected),
                Frame::UnnumberedAcknowledge(_) => self
                    .inner
                    .layer_3_initiated(false)
                    .transition()
                    .error(ErrorCode::C)
                    .establish_data_link()
                    .into_state(State::AwaitingConnection),
                Frame::UnnumberedInformation(frame) => {
                    if frame.p_or_f {
                        self.transition()
                            .ui_check(frame)
                            .enquiry_response(true)
                            .into_state(State::Connected)
                    } else {
                        self.transition()
                            .ui_check(frame)
                            .into_state(State::Connected)
                    }
                }
                Frame::ReceiveNotReady(frame) => {
                    Connected::handle_readiness(self.inner.set_peer_busy(), frame.into())
                }
                Frame::ReceiveReady(frame) => {
                    Connected::handle_readiness(self.inner.clear_peer_busy(), frame.into())
                }
                Frame::Reject(frame) => {
                    let sequence_ok = self.inner.receive_sequence_ok(frame.receive_sequence);
                    let transition = self
                        .inner
                        .clear_peer_busy()
                        .transition()
                        .check_need_for_response(frame.c_or_r, frame.p_or_f);

                    if sequence_ok {
                        transition
                            .inner(|i| {
                                i.set_va(frame.receive_sequence.into())
                                    .stop_timer_1()
                                    .stop_timer_3()
                                    .select_timer_1_value()
                            })
                            .invoke_retransmission(frame.receive_sequence.into())
                            .into_state(State::Connected)
                    } else {
                        transition
                            .nr_error_recovery()
                            .into_state(State::AwaitingConnection)
                    }
                }
                Frame::SelectiveReject(frame) => {
                    if self.inner.receive_sequence_ok(frame.receive_sequence) {
                        let inner = if frame.p_or_f {
                            self.inner.set_va(frame.receive_sequence.into())
                        } else {
                            self.inner
                        };

                        inner
                            .clear_peer_busy()
                            .stop_timer_1()
                            .start_timer_3()
                            .select_timer_1_value()
                            .requeue_selected_from_pending_ack(frame.receive_sequence.into())
                            .transition()
                            // This seems kind of silly to me, as we'll probably end up sending an I for the rejected frame, why not just skip this?
                            .check_need_for_response(Some(CommandResponse::Command), frame.p_or_f)
                            .into_state(State::Connected)
                    } else {
                        self.transition()
                            .nr_error_recovery()
                            .into_state(State::AwaitingConnection)
                    }
                }
                Frame::SetAsynchronousBalancedModeExtended(frame) => {
                    FrameHandler::new(frame.into(), self.inner)
                        .handle_reset()
                        .into_state(State::Connected)
                }
                Frame::SetAsynchronousBalancedMode(frame) => {
                    FrameHandler::new(frame.into(), self.inner)
                        .handle_reset()
                        .into_state(State::Connected)
                }
                Frame::Information(frame) => {
                    FrameHandler::new(frame, self.inner).handle(State::Connected)
                }
                _ => self.ignored(&frame),
            },
            Received::Primitive(primitive) => match primitive {
                In::ConnectRequest => self
                    .inner
                    .discard_frame_queue()
                    .layer_3_initiated(true)
                    .transition()
                    .establish_data_link()
                    .into_state(State::AwaitingConnection),
                In::DisconnectRequest => self
                    .inner
                    .discard_frame_queue()
                    .retry_count(0)
                    .stop_timer_3()
                    .start_timer_1()
                    .transition()
                    .send(Disconnect { poll: true })
                    .into_state(State::AwaitingRelease),
                In::DataRequest(info) => self
                    .inner
                    .queue_multiple_if_necessary(info)
                    .transition()
                    .into_state(State::Connected),
                In::UnitDataRequest(info) => self
                    .transition()
                    .send(UnnumberedInformation {
                        c_or_r: Some(CommandResponse::Command),
                        p_or_f: true,
                        info,
                    })
                    .into_state(State::Connected),
                In::Timer(standard) => match standard {
                    StandardTimer::T1 => self
                        .inner
                        .retry_count(1)
                        .transition()
                        .transmit_enquiry()
                        .into_state(State::TimerRecovery),
                    StandardTimer::T3 => self
                        .inner
                        .retry_count(0)
                        .transition()
                        .transmit_enquiry()
                        .into_state(State::TimerRecovery),
                },
                In::Flush => FlushQueue::new(self.inner)
                    .flush()
                    .into_state(State::Connected),
                In::Acknowledge => Acknowledge::new(self.inner)
                    .acknowledge()
                    .into_state(State::Connected),
                _ => self.ignored(&primitive),
            },
        }
    }

    fn handle_readiness(inner: Inner, readiness: Readiness) -> Transition<State> {
        let ok = inner.receive_sequence_ok(readiness.receive_sequence);
        let transition = inner
            .transition()
            .check_need_for_response(readiness.c_or_r, readiness.p_or_f);

        if ok {
            transition
                .inner(|i| i.check_i_frame_ackd(readiness.receive_sequence))
                .into_state(State::Connected)
        } else {
            transition
                .nr_error_recovery()
                .into_state(State::AwaitingConnection)
        }
    }
}

impl From<Inner> for Connected {
    fn from(value: Inner) -> Self {
        Self { inner: value }
    }
}

#[derive(Debug, PartialEq, Default)]
pub(super) struct TimerRecovery {
    inner: Inner,
}

impl TimerRecovery {
    pub fn handle(self, signal: Received) -> Transition<State> {
        match signal {
            Received::Frame(frame) => match frame {
                Frame::SetAsynchronousBalancedModeExtended(frame) => {
                    FrameHandler::new(frame.into(), self.inner)
                        .handle_reset()
                        .into_state(State::Connected)
                }
                Frame::SetAsynchronousBalancedMode(frame) => {
                    FrameHandler::new(frame.into(), self.inner)
                        .handle_reset()
                        .into_state(State::Connected)
                }
                Frame::Disconnect(_) => self
                    .inner
                    .discard_frame_queue()
                    .stop_timer_1()
                    .stop_timer_3()
                    .transition()
                    .send(UnnumberedAcknowledge { is_final: true })
                    .primitive(Out::DisconnectIndication)
                    .into_state(State::Disconnected),
                Frame::DisconnectedMode(_) => self
                    .inner
                    .discard_frame_queue()
                    .transition()
                    .stop_timer_1()
                    .stop_timer_3()
                    .error(ErrorCode::E)
                    .primitive(Out::DisconnectIndication)
                    .into_state(State::Disconnected),
                Frame::UnnumberedAcknowledge(_) => self
                    .inner
                    .layer_3_initiated(false)
                    .transition()
                    .error(ErrorCode::C)
                    .establish_data_link()
                    .into_state(State::AwaitingConnection),
                Frame::UnnumberedInformation(frame) => {
                    if frame.p_or_f {
                        self.transition()
                            .ui_check(frame)
                            .enquiry_response(true)
                            .into_state(State::TimerRecovery)
                    } else {
                        self.transition()
                            .ui_check(frame)
                            .into_state(State::TimerRecovery)
                    }
                }
                Frame::ReceiveNotReady(frame) => {
                    TimerRecovery::handle_readiness(self.inner.set_peer_busy(), frame.into())
                }
                Frame::ReceiveReady(frame) => {
                    TimerRecovery::handle_readiness(self.inner.clear_peer_busy(), frame.into())
                }
                Frame::Information(frame) => {
                    FrameHandler::new(frame, self.inner).handle(State::TimerRecovery)
                }
                Frame::Reject(frame) => {
                    FrameHandler::new(frame, self.inner).handle(State::TimerRecovery)
                }
                _ => self.ignored(&frame),
            },
            Received::Primitive(primitive) => match primitive {
                In::DataRequest(info) => self
                    .inner
                    .queue_multiple_if_necessary(info)
                    .transition()
                    .into_state(State::TimerRecovery),
                In::UnitDataRequest(info) => self
                    .transition()
                    .send(UnnumberedInformation {
                        c_or_r: Some(CommandResponse::Command),
                        p_or_f: true,
                        info,
                    })
                    .into_state(State::TimerRecovery),
                In::Timer(timer) => match timer {
                    StandardTimer::T1 => {
                        if self.inner.flags.retry_count == self.inner.flags.parameters.retries_n2 {
                            let error_code = if self.inner.sequences.has_unacknowledged_data() {
                                ErrorCode::I
                            } else if self.inner.flags.peer_receiver_busy {
                                ErrorCode::U
                            } else {
                                ErrorCode::T
                            };

                            self.inner
                                .retry_count(0)
                                .clear_sequences()
                                .transition()
                                .error(error_code)
                                .primitive(Out::DisconnectIndication)
                                .send(DisconnectedMode { is_final: true })
                                .into_state(State::Disconnected)
                        } else {
                            self.inner
                                .retrying()
                                .transition()
                                .transmit_enquiry()
                                .into_state(State::TimerRecovery)
                        }
                    }
                    _ => self.ignored(&timer),
                },
                In::Flush => FlushQueue::new(self.inner)
                    .flush()
                    .into_state(State::TimerRecovery),
                In::DisconnectRequest => self
                    .inner
                    .discard_frame_queue()
                    .retry_count(0)
                    .stop_timer_3()
                    .start_timer_1()
                    .transition()
                    .send(Disconnect { poll: true })
                    .into_state(State::AwaitingRelease),
                In::ConnectRequest => self
                    .inner
                    .discard_frame_queue()
                    .layer_3_initiated(true)
                    .transition()
                    .establish_data_link()
                    .into_state(State::AwaitingConnection),
                In::Acknowledge => Acknowledge::new(self.inner)
                    .acknowledge()
                    .into_state(State::TimerRecovery),
                _ => self.ignored(&primitive),
            },
        }
    }

    fn handle_readiness(inner: Inner, readiness: Readiness) -> Transition<State> {
        let sequence_ok = inner.receive_sequence_ok(readiness.receive_sequence);
        if readiness.c_or_r == Some(CommandResponse::Response) {
            let inner = inner.stop_timer_1().select_timer_1_value();
            if sequence_ok {
                let inner = inner.set_va(readiness.receive_sequence);
                if inner.sequences.all_data_acknowledged() {
                    inner
                        .start_timer_3()
                        .transition()
                        .into_state(State::Connected)
                } else {
                    inner
                        .transition()
                        .invoke_retransmission(readiness.receive_sequence)
                        .into_state(State::TimerRecovery)
                }
            } else {
                inner
                    .transition()
                    .nr_error_recovery()
                    .into_state(State::TimerRecovery)
            }
        } else {
            let transition = if readiness.p_or_f {
                inner.transition().enquiry_response(true)
            } else {
                inner.transition()
            };
            if sequence_ok {
                transition
                    .inner(|i| i.set_va(readiness.receive_sequence))
                    .into_state(State::TimerRecovery)
            } else {
                transition
                    .nr_error_recovery()
                    .into_state(State::AwaitingConnection)
            }
        }
    }
}

impl From<Inner> for TimerRecovery {
    fn from(value: Inner) -> Self {
        Self { inner: value }
    }
}

#[derive(Debug, Default, PartialEq)]
pub(super) struct Disconnected {
    inner: Inner,
}

impl Disconnected {
    pub fn handle(self, signal: Received) -> Transition<State> {
        match signal {
            Received::Frame(frame) => match frame {
                Frame::Disconnect(_) => self
                    .inner
                    .transition()
                    .send(DisconnectedMode { is_final: true })
                    .into_state(State::Disconnected),
                Frame::DisconnectedMode(_) => {
                    self.inner.transition().into_state(State::Disconnected)
                }
                Frame::UnnumberedAcknowledge(_) => self
                    .inner
                    .transition()
                    .error(ErrorCode::C)
                    .error(ErrorCode::D)
                    .into_state(State::Disconnected),
                Frame::UnnumberedInformation(frame) => {
                    if frame.p_or_f {
                        self.inner
                            .transition()
                            .ui_check(frame)
                            .send(DisconnectedMode { is_final: true })
                            .into_state(State::Disconnected)
                    } else {
                        self.inner
                            .transition()
                            .ui_check(frame)
                            .into_state(State::Disconnected)
                    }
                }
                Frame::SetAsynchronousBalancedMode(frame) => {
                    self.establish(frame.p_or_f, Version::Ax20)
                }
                Frame::SetAsynchronousBalancedModeExtended(frame) => {
                    self.establish(frame.p_or_f, Version::Ax22)
                }
                _ => self.ignored(&frame),
            },
            Received::Primitive(primitive) => match primitive {
                In::ConnectRequest => self
                    .inner
                    .set_srt_and_timer_1_value()
                    .layer_3_initiated(true)
                    .transition()
                    .establish_data_link()
                    .into_state(State::AwaitingConnection),
                In::DisconnectRequest => self
                    .inner
                    .transition()
                    .primitive(Out::DisconnectConfirm)
                    .into_state(State::Disconnected),
                In::UnitDataRequest(info) => self
                    .inner
                    .transition()
                    .send(UnnumberedInformation {
                        c_or_r: Some(CommandResponse::Command),
                        p_or_f: true,
                        info,
                    })
                    .into_state(State::Disconnected),
                In::Acknowledge => Acknowledge::new(self.inner)
                    .ignored()
                    .into_state(State::Disconnected),
                _ => self.ignored(&primitive),
            },
        }
    }

    fn establish(self, poll: bool, version: Version) -> Transition<State> {
        if self.inner.flags.unable_to_establish {
            self.inner
                .transition()
                .send(DisconnectedMode { is_final: poll })
                .into_state(State::Disconnected)
        } else {
            self.inner
                .version(version)
                .clear_exception_conditions()
                .clear_sequences()
                .set_srt_and_timer_1_value()
                .start_timer_3()
                .transition()
                .send(UnnumberedAcknowledge { is_final: poll })
                .primitive(Out::ConnectIndication)
                .into_state(|inner| State::Connected(Connected { inner }))
        }
    }
}

impl From<Inner> for Disconnected {
    fn from(value: Inner) -> Self {
        Self { inner: value }
    }
}

#[derive(Debug, PartialEq)]
pub(super) enum State {
    Disconnected(Disconnected),
    AwaitingConnection(AwaitingConnection),
    AwaitingRelease(AwaitingRelease),
    Connected(Connected),
    TimerRecovery(TimerRecovery),
}

impl State {
    pub fn handle(self, received: Received) -> Transition<State> {
        match self {
            State::Disconnected(state) => state.handle(received),
            State::AwaitingConnection(state) => state.handle(received),
            State::AwaitingRelease(state) => state.handle(received),
            State::Connected(state) => state.handle(received),
            State::TimerRecovery(state) => state.handle(received),
        }
    }

    pub fn tick(self, elapsed: TimerValue) -> (Self, Vec<Received>) {
        let inner = self.inner();
        match inner.timers.elapsed(elapsed) {
            Some((timers, generated)) => (
                match self {
                    State::AwaitingConnection(state) => {
                        State::AwaitingConnection(state.inner.with_timers(timers).into())
                    }
                    State::Connected(state) => {
                        State::Connected(state.inner.with_timers(timers).into())
                    }
                    State::TimerRecovery(state) => {
                        State::TimerRecovery(state.inner.with_timers(timers).into())
                    }
                    State::AwaitingRelease(state) => {
                        State::AwaitingRelease(state.inner.with_timers(timers).into())
                    }
                    State::Disconnected(_) => panic!("timer expiration in disconnected state"),
                },
                generated,
            ),
            None => (self, Vec::default()),
        }
    }

    pub fn inner(&self) -> &Inner {
        match self {
            State::Disconnected(state) => &state.inner,
            State::AwaitingConnection(state) => &state.inner,
            State::AwaitingRelease(state) => &state.inner,
            State::Connected(state) => &state.inner,
            State::TimerRecovery(state) => &state.inner,
        }
    }

    pub fn is_connected(&self) -> bool {
        match self {
            State::Disconnected(_) => false,
            State::AwaitingConnection(_) => false,
            State::AwaitingRelease(_) => false,
            State::Connected(_) => true,
            State::TimerRecovery(_) => true,
        }
    }

    pub fn is_busy(&self) -> bool {
        !matches!(self, State::Disconnected(_))
    }

    pub fn timer_expiration(&self) -> Option<TimerValue> {
        self.inner().timers.remaining()
    }
}

impl From<Disconnected> for State {
    fn from(value: Disconnected) -> Self {
        Self::Disconnected(value)
    }
}

impl From<AwaitingConnection> for State {
    fn from(value: AwaitingConnection) -> Self {
        Self::AwaitingConnection(value)
    }
}

impl From<AwaitingRelease> for State {
    fn from(value: AwaitingRelease) -> Self {
        Self::AwaitingRelease(value)
    }
}

impl From<Connected> for State {
    fn from(value: Connected) -> Self {
        Self::Connected(value)
    }
}

impl From<TimerRecovery> for State {
    fn from(value: TimerRecovery) -> Self {
        Self::TimerRecovery(value)
    }
}

#[derive(Debug, Error, PartialEq)]
pub enum Error {}

#[derive(Debug, PartialEq)]
pub enum Generated {
    Primitive(Out),
    Frame(Frame),
}

impl From<Out> for Generated {
    fn from(val: Out) -> Self {
        Generated::Primitive(val)
    }
}

impl From<Frame> for Generated {
    fn from(val: Frame) -> Self {
        Generated::Frame(val)
    }
}

#[derive(Debug, PartialEq)]
pub enum Received {
    Frame(Frame),
    Primitive(In),
}

impl From<In> for Received {
    fn from(val: In) -> Self {
        Received::Primitive(val)
    }
}

impl From<Frame> for Received {
    fn from(val: Frame) -> Self {
        Received::Frame(val)
    }
}

#[derive(Debug, PartialEq)]
#[allow(dead_code)]
pub enum In {
    /// This primitive is used by the Layer 3 entity to request the
    /// establishment of a AX.25 connection.
    ConnectRequest,
    /// This primitive is used by the Layer 3 entity to request the release of a
    /// AX.25 connection.
    DisconnectRequest,
    /// This primitive is used by the Layer 3 entity to request the transmission
    /// of data using connection oriented protocol. This frame is examined and
    /// acted upon by the segmenter, if necessary.
    DataRequest(Info),
    /// This primitive is used by the Layer 3 entity to request the transmission
    /// of data using connectionless protocol. This frame is examined and acted
    /// upon by the segmenter, if necessary.
    UnitDataRequest(Info),
    /// This primitive is used by the Layer 3 entity to temporarily suspend the
    /// flow of incoming information
    FlowOffRequest,
    /// This primitive is used by the Layer 3 entity to resume the flow of
    /// incoming information.
    FlowOnRequest,
    /// Timer expirey. This is not represented as a primitive in the spec, and
    /// instead takes on the same symbol as an incoming frame. I've decided I
    /// like this more.
    Timer(StandardTimer),
    /// Attempt to flush outgoing queue.
    Flush,
    /// Send pending acknowledgement.
    Acknowledge,
}

#[derive(Debug, PartialEq)]
pub enum Out {
    /// This primitive is used by the Data-link State Machine to indicate an
    /// AX.25 connection has been requested.
    ConnectIndication,
    /// This primitive is used by the Data-link State Machine to indicate a
    /// AX.25 connection has been made
    ConnectConfirm,
    /// This primitive is used by the Data-link State Machine to indicate an
    /// AX.25 connection has been released.
    DisconnectIndication,
    /// This primitive is used by the Data-link State Machine to indicate an
    /// AX.25 connection has been released and confirmed.
    DisconnectConfirm,
    /// This primitive is used by the reassembler to indicate reception of Layer
    /// 3 data using connection oriented protocol.
    DataIndication(Info),
    /// This primitive is used by the reassembler to indicate reception of Layer
    /// 3 data using connectionless protocol.
    UnitDataIndication(Info),
    /// This primitive is used by the Data-link State Machine to indicate when
    /// frames have been received that are inconsistent with this protocol
    /// definition. This includes short frames, frames with inconsistent
    /// parameter values, etc.
    ErrorIndication(ErrorCode),
    /// Correlates with the LM-SIEZE request to send a pending acknowledgement
    /// at the soonest possible convienence. This is provided as a mechanism to
    /// customize this process.
    AcknowledgePending(bool),
}

#[derive(Debug, PartialEq)]
#[allow(dead_code)]
pub enum ErrorCode {
    /// A — F=1 received but P=1 not outstanding.
    A,
    /// B — Unexpected DM with F=1 in states 3, 4 or 5.
    B,
    /// C — Unexpected UA in states 3, 4 or 5.
    C,
    /// D — UA received without F=1 when SABM or DISC was sent P=1.
    D,
    /// E — DM received in states 3, 4 or 5.
    E,
    /// F — Data link reset; i.e., SABM received in state 3, 4 or 5.
    F,
    /// Page 88
    G,
    /// ?
    H,
    /// I — N2 timeouts: unacknowledged data.
    I,
    /// J — N(r) sequence error.
    J,
    /// L — Control field invalid or not implemented.
    L,
    /// M — Information field was received in a U- or S-type frame.
    M,
    /// N — Length of frame incorrect for frame type.
    N,
    /// O — I frame exceeded maximum allowed length.
    O,
    /// P — N(s) out of the window.
    P,
    /// Q — UI response received, or UI command with P=1 received.
    Q,
    /// R — UI frame exceeded maximum allowed length.
    R,
    /// S — I response received.
    S,
    /// T — N2 timeouts: no response to enquiry.
    T,
    /// U — N2 timeouts: extended peer busy condition.
    U,
    /// V — No DL machines available to establish connection.
    V,
}

struct FrameHandler<T> {
    frame: T,
    inner: Inner,
}

impl<T> FrameHandler<T> {
    pub fn new(frame: T, inner: impl Into<Inner>) -> Self {
        Self {
            frame,
            inner: inner.into(),
        }
    }
}

impl FrameHandler<SetAsynchronous> {
    fn handle_reset(self) -> Transition<Inner> {
        let transition = self
            .inner
            .transition()
            .send(UnnumberedAcknowledge { is_final: true })
            .clear_exception_conditions()
            .error(ErrorCode::F);
        let transition = if transition.inner_ref().all_data_acknowledged() {
            transition
        } else {
            transition
                .inner(|i| i.discard_frame_queue())
                .primitive(Out::ConnectIndication)
        };
        transition
            .stop_timer_1()
            .start_timer_3()
            .inner(|i| i.clear_sequences())
    }
}

impl FrameHandler<Information> {
    fn handle<S: From<Inner>, T: FnOnce(S) -> State>(self, stay: T) -> Transition<State> {
        if self.frame.c_or_r == Some(CommandResponse::Response) {
            self.inner.transition().error(ErrorCode::S).into_state(stay)
        } else if !self.inner.receive_sequence_ok(self.frame.receive_sequence) {
            self.inner
                .transition()
                .nr_error_recovery()
                .into_state(State::AwaitingConnection)
        } else {
            let inner = self
                .inner
                .check_i_frame_ackd(self.frame.receive_sequence.into());

            if inner.flags.own_receiver_busy {
                if self.frame.poll {
                    let receive_sequence = inner.sequences.vr.into();
                    inner
                        .transition()
                        .clear_acknowledge_pending()
                        .send(ReceiveNotReady {
                            c_or_r: Some(CommandResponse::Response),
                            p_or_f: true,
                            receive_sequence,
                        })
                        .into_state(stay)
                } else {
                    inner.transition().into_state(stay)
                }
            } else if self.frame.send_sequence != inner.sequences.vr.into() {
                let send_sequence: Seq = self.frame.send_sequence.into();
                let srej_enabled = inner.flags.selective_reject_enabled;
                let srej_started = inner.flags.selective_reject_exception > 0;
                // N(s) > V(r) + 1 -->  V(r) + 1 <= N(s)
                let rej_fallback = srej_enabled
                    && !srej_started
                    && send_sequence.greater_than(inner.sequences.vr.add(1));

                info!(
                    "N(s) != v(r) ({:?} != {:?}) (srej-en = {:?}) (rej-fb = {:?})",
                    send_sequence, inner.sequences.vr, srej_enabled, rej_fallback
                );

                let inner = if srej_enabled {
                    inner.save_incoming(self.frame.send_sequence.into(), self.frame.info)
                } else {
                    inner
                };

                if (!srej_enabled && !inner.flags.reject_exception) || rej_fallback {
                    let receive_sequence = inner.sequences.vr.into();
                    inner
                        .set_reject_exception()
                        .transition()
                        .clear_acknowledge_pending()
                        .send(Reject {
                            c_or_r: Some(CommandResponse::Command),
                            p_or_f: self.frame.poll,
                            receive_sequence,
                        })
                        .into_state(stay)
                } else if srej_enabled {
                    let inner = inner.increase_selective_reject_exception();
                    let receive_sequence = if srej_started {
                        self.frame.send_sequence
                    } else {
                        inner.sequences.vr.into()
                    };

                    inner
                        .transition()
                        .clear_acknowledge_pending()
                        .send(SelectiveReject {
                            p_or_f: !srej_started,
                            receive_sequence,
                        })
                        .into_state(stay)
                } else if !self.frame.poll {
                    inner.transition().into_state(stay)
                } else {
                    inner
                        .transition()
                        .send_receive_ready_command(self.frame.poll)
                        .into_state(stay)
                }
            } else {
                let immediate_ack_on_information_poll =
                    inner.flags.parameters.immediate_ack_on_information_poll;

                let transition = inner
                    .increase_vr()
                    .clear_reject_exception()
                    .transition()
                    .primitive(Out::DataIndication(self.frame.info))
                    .drain_incoming(); // After above indication, maintain order.

                let transition = if self.frame.poll && immediate_ack_on_information_poll {
                    // 6.2 The next response frame returned to an I frame with the P bit
                    // set to “1”, received during the information transfer state,
                    // is an RR, RNR or REJ response with the F bit set to “1”.
                    transition.send_receive_ready_response()
                } else {
                    transition.set_acknowledge_pending_if_not()
                };

                transition.into_state(stay)
            }
        }
    }
}

struct Acknowledge {
    inner: Inner,
}

impl Acknowledge {
    pub fn new(inner: impl Into<Inner>) -> Self {
        Self {
            inner: inner.into(),
        }
    }

    fn acknowledge(self) -> Transition<Inner> {
        if self.inner.flags.acknowledge_pending {
            debug!("ack-pending: sending");
            self.inner
                .transition()
                .clear_acknowledge_pending()
                .enquiry_response(false)
        } else {
            trace!("ack-pending: ignored");
            self.inner.transition()
        }
    }

    fn ignored(self) -> Transition<Inner> {
        if self.inner.flags.acknowledge_pending {
            warn!("ack-pending: suppressed");
            self.inner.transition()
        } else {
            self.inner.transition()
        }
    }
}

struct FlushQueue {
    inner: Inner,
}

impl FlushQueue {
    pub fn new(inner: impl Into<Inner>) -> Self {
        Self {
            inner: inner.into(),
        }
    }

    fn flush(self) -> Transition<Inner> {
        if self.inner.flags.peer_receiver_busy {
            self.inner.transition()
        } else if self.inner.can_send_frames() {
            let mut inner = self.inner.transition();

            while inner.inner_ref().can_send_frames() && !inner.inner_ref().frame_queue_empty() {
                let send_sequence = inner.inner_ref().sequences.vs;
                let receive_sequence = inner.inner_ref().sequences.vr;

                let (new_inner, info) = inner.pop_from_queue();

                inner = new_inner;

                if let Some(info) = info {
                    inner = inner.inner(|i| i.increase_vs());

                    let last_frame = !inner.inner_ref().can_send_frames()
                        || inner.inner_ref().frame_queue_empty();

                    inner = inner
                        .inner(|i| i.save_pending_ack(send_sequence, info.clone()))
                        .send(Information {
                            c_or_r: Some(CommandResponse::Command),
                            poll: last_frame,
                            info,
                            send_sequence: send_sequence.into(),
                            receive_sequence: receive_sequence.into(),
                        });
                } else {
                    break;
                }
            }

            if inner.inner_ref().timers.timer_1.is_started() {
                inner
            } else {
                inner.stop_timer_3().start_timer_1()
            }
        } else {
            self.inner.transition()
        }
    }
}

impl FrameHandler<Reject> {
    fn handle<S: From<Inner>, T: FnOnce(S) -> State>(self, stay: T) -> Transition<State> {
        let inner = self.inner.clear_peer_busy();
        let receive_sequence_ok = inner.receive_sequence_ok(self.frame.receive_sequence);

        if self.frame.c_or_r == Some(CommandResponse::Response) && self.frame.p_or_f {
            let inner = inner.stop_timer_1().select_timer_1_value();

            if receive_sequence_ok {
                let inner = inner.set_va(self.frame.receive_sequence.into());

                if inner.all_data_acknowledged() {
                    inner
                        .start_timer_3()
                        .transition()
                        .into_state(State::Connected)
                } else {
                    inner
                        .transition()
                        .invoke_retransmission(self.frame.receive_sequence.into())
                        .into_state(stay)
                }
            } else {
                inner
                    .transition()
                    .nr_error_recovery()
                    .into_state(State::AwaitingConnection)
            }
        } else {
            let inner = if self.frame.p_or_f {
                inner.transition().enquiry_response(true)
            } else {
                inner.transition()
            };

            if receive_sequence_ok {
                let inner = inner.inner(|i| i.set_va(self.frame.receive_sequence.into()));

                if inner.inner_ref().all_data_acknowledged() {
                    inner.into_state(stay)
                } else {
                    inner
                        .invoke_retransmission(self.frame.receive_sequence.into())
                        .into_state(stay)
                }
            } else {
                inner
                    .nr_error_recovery()
                    .into_state(State::AwaitingConnection)
            }
        }
    }
}

trait OwnsInner<T>: Into<State>
where
    Self: Sized,
{
    fn ignored<I: std::fmt::Debug>(self, ignoring: &I) -> Transition<State> {
        warn!("ignoring {:?}", ignoring);
        Transition::new(self.into())
    }

    fn transition(self) -> Transition<Inner>;
}

impl OwnsInner<AwaitingConnection> for AwaitingConnection {
    fn transition(self) -> Transition<Inner> {
        Transition::new(self.inner)
    }
}

impl OwnsInner<AwaitingRelease> for AwaitingRelease {
    fn transition(self) -> Transition<Inner> {
        Transition::new(self.inner)
    }
}

impl OwnsInner<Disconnected> for Disconnected {
    fn transition(self) -> Transition<Inner> {
        Transition::new(self.inner)
    }
}

impl OwnsInner<Connected> for Connected {
    fn transition(self) -> Transition<Inner> {
        Transition::new(self.inner)
    }
}

impl OwnsInner<TimerRecovery> for TimerRecovery {
    fn transition(self) -> Transition<Inner> {
        Transition::new(self.inner)
    }
}
