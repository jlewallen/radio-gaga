use crate::callsign::{Callsign, Pair};

use super::inner::{Seq, Version};

#[derive(PartialEq, Clone)]
pub struct Info(Vec<u8>);

impl Info {
    pub fn into_inner(self) -> Vec<u8> {
        self.0
    }

    pub fn chunks(self, size: usize) -> Vec<Info> {
        self.0
            .chunks(size)
            .map(|chunk| Info(chunk.into()))
            .collect()
    }
}

impl std::fmt::Debug for Info {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("Info({} bytes)", self.0.len()))
    }
}

impl From<Info> for Vec<u8> {
    fn from(val: Info) -> Self {
        val.0
    }
}

impl From<Vec<u8>> for Info {
    fn from(value: Vec<u8>) -> Self {
        Self(value)
    }
}

#[derive(PartialEq, Clone, Debug)]
pub enum CommandResponse {
    Command,
    Response,
}

impl From<CommandResponse> for ax25::frame::CommandResponse {
    fn from(value: CommandResponse) -> Self {
        match value {
            CommandResponse::Command => ax25::frame::CommandResponse::Command,
            CommandResponse::Response => ax25::frame::CommandResponse::Response,
        }
    }
}

impl From<ax25::frame::CommandResponse> for CommandResponse {
    fn from(value: ax25::frame::CommandResponse) -> Self {
        match value {
            ax25::frame::CommandResponse::Command => CommandResponse::Command,
            ax25::frame::CommandResponse::Response => CommandResponse::Response,
        }
    }
}

#[derive(PartialEq, Clone)]
pub struct Disconnect {
    pub poll: bool,
}

impl std::fmt::Debug for Disconnect {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_tuple("Disc").field(&self.poll).finish()
    }
}

impl From<Disconnect> for Frame {
    fn from(val: Disconnect) -> Self {
        Frame::Disconnect(val)
    }
}

#[derive(PartialEq, Clone)]
pub struct DisconnectedMode {
    pub is_final: bool,
}

impl std::fmt::Debug for DisconnectedMode {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_tuple("DiscMode").field(&self.is_final).finish()
    }
}

impl From<DisconnectedMode> for Frame {
    fn from(val: DisconnectedMode) -> Self {
        Frame::DisconnectedMode(val)
    }
}

#[derive(PartialEq, Clone)]
pub struct UnnumberedAcknowledge {
    pub is_final: bool,
}

impl std::fmt::Debug for UnnumberedAcknowledge {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_tuple("UA").field(&self.is_final).finish()
    }
}

impl From<UnnumberedAcknowledge> for Frame {
    fn from(val: UnnumberedAcknowledge) -> Self {
        Frame::UnnumberedAcknowledge(val)
    }
}

#[derive(PartialEq, Clone)]
pub struct UnnumberedInformation {
    pub c_or_r: Option<CommandResponse>,
    pub p_or_f: bool,
    pub info: Info,
}

impl std::fmt::Debug for UnnumberedInformation {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_tuple("UI")
            .field(&self.c_or_r)
            .field(&self.p_or_f)
            .field(&self.info)
            .finish()
    }
}

impl From<UnnumberedInformation> for Frame {
    fn from(val: UnnumberedInformation) -> Self {
        Frame::UnnumberedInformation(val)
    }
}

#[derive(PartialEq, Clone)]
pub struct ReceiveReady {
    pub c_or_r: Option<CommandResponse>,
    pub p_or_f: bool,
    pub receive_sequence: u8,
}

impl std::fmt::Debug for ReceiveReady {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!(
            "RR({:?}, PF={:?}, N(r)={})",
            &self.c_or_r, &self.p_or_f, &self.receive_sequence
        ))
    }
}

impl From<ReceiveReady> for Frame {
    fn from(val: ReceiveReady) -> Self {
        Frame::ReceiveReady(val)
    }
}

#[derive(PartialEq, Clone)]
pub struct ReceiveNotReady {
    pub c_or_r: Option<CommandResponse>,
    pub p_or_f: bool,
    pub receive_sequence: u8,
}

impl std::fmt::Debug for ReceiveNotReady {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!(
            "RNR({:?}, PF={:?}, N(r)={})",
            &self.c_or_r, &self.p_or_f, &self.receive_sequence
        ))
    }
}

impl From<ReceiveNotReady> for Frame {
    fn from(val: ReceiveNotReady) -> Self {
        Frame::ReceiveNotReady(val)
    }
}

#[derive(PartialEq, Clone)]
pub struct SetAsynchronousBalancedMode {
    pub p_or_f: bool,
}

impl std::fmt::Debug for SetAsynchronousBalancedMode {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_tuple("Sabm").field(&self.p_or_f).finish()
    }
}

impl From<SetAsynchronousBalancedMode> for Frame {
    fn from(val: SetAsynchronousBalancedMode) -> Self {
        Frame::SetAsynchronousBalancedMode(val)
    }
}

#[derive(PartialEq, Clone)]
pub struct SetAsynchronousBalancedModeExtended {
    pub p_or_f: bool,
}

impl std::fmt::Debug for SetAsynchronousBalancedModeExtended {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_tuple("SabmE").field(&self.p_or_f).finish()
    }
}

impl From<SetAsynchronousBalancedModeExtended> for Frame {
    fn from(val: SetAsynchronousBalancedModeExtended) -> Self {
        Frame::SetAsynchronousBalancedModeExtended(val)
    }
}

#[derive(PartialEq, Clone)]
pub struct Information {
    pub c_or_r: Option<CommandResponse>,
    pub poll: bool,
    pub info: Info,
    pub send_sequence: u8,
    pub receive_sequence: u8,
}

impl std::fmt::Debug for Information {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!(
            "I({:?}, P={:?}, N(s)={}, N(r)={}, {:?})",
            &self.c_or_r, &self.poll, &self.send_sequence, &self.receive_sequence, &self.info
        ))
    }
}

impl From<Information> for Frame {
    fn from(val: Information) -> Self {
        Frame::Information(val)
    }
}

#[derive(PartialEq, Clone)]
pub struct Reject {
    pub c_or_r: Option<CommandResponse>,
    pub p_or_f: bool,
    pub receive_sequence: u8,
}

impl std::fmt::Debug for Reject {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!(
            "Rej({:?}, PF={:?}, N(r)={})",
            &self.c_or_r, &self.p_or_f, &self.receive_sequence
        ))
    }
}

impl From<Reject> for Frame {
    fn from(val: Reject) -> Self {
        Frame::Reject(val)
    }
}

#[derive(PartialEq, Clone)]
pub struct SelectiveReject {
    pub p_or_f: bool,
    pub receive_sequence: u8,
}

impl std::fmt::Debug for SelectiveReject {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!(
            "SRej(PF={:?}, N(r)={})",
            &self.p_or_f, &self.receive_sequence
        ))
    }
}

impl From<SelectiveReject> for Frame {
    fn from(val: SelectiveReject) -> Self {
        Frame::SelectiveReject(val)
    }
}

#[derive(PartialEq, Clone)]
pub struct FrameReject {
    pub is_final: bool,
}

impl std::fmt::Debug for FrameReject {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("FRMR(F={:?}))", &self.is_final))
    }
}

impl From<FrameReject> for Frame {
    fn from(val: FrameReject) -> Self {
        Frame::FrameReject(val)
    }
}

#[derive(PartialEq, Clone, Debug)]
pub enum Frame {
    Disconnect(Disconnect),
    DisconnectedMode(DisconnectedMode),
    UnnumberedAcknowledge(UnnumberedAcknowledge),
    UnnumberedInformation(UnnumberedInformation),
    ReceiveReady(ReceiveReady),
    ReceiveNotReady(ReceiveNotReady),
    Reject(Reject),
    SelectiveReject(SelectiveReject),
    SetAsynchronousBalancedMode(SetAsynchronousBalancedMode),
    SetAsynchronousBalancedModeExtended(SetAsynchronousBalancedModeExtended),
    Information(Information),
    #[allow(clippy::enum_variant_names)]
    FrameReject(FrameReject),
}

#[derive(Debug, PartialEq, Clone)]
pub struct AddressedFrame {
    source: Callsign,
    destination: Callsign,
    frame: Frame,
}

impl AddressedFrame {
    pub fn new(pair: Pair, frame: Frame) -> Self {
        let (source, destination) = pair.split();
        Self {
            source,
            destination,
            frame,
        }
    }

    pub fn source(&self) -> &Callsign {
        &self.source
    }

    pub fn destination(&self) -> &Callsign {
        &self.destination
    }
}

impl From<AddressedFrame> for Frame {
    fn from(value: AddressedFrame) -> Self {
        value.frame
    }
}

impl From<ax25::frame::Ax25Frame> for AddressedFrame {
    fn from(value: ax25::frame::Ax25Frame) -> Self {
        match value.content {
            ax25::frame::FrameContent::Information(frame) => AddressedFrame {
                source: value.source.into(),
                destination: value.destination.into(),
                frame: Frame::Information(Information {
                    c_or_r: value.command_or_response.map(|v| v.into()),
                    poll: frame.poll,
                    info: frame.info.into(),
                    send_sequence: frame.send_sequence,
                    receive_sequence: frame.receive_sequence,
                }),
            },
            ax25::frame::FrameContent::ReceiveReady(frame) => AddressedFrame {
                source: value.source.into(),
                destination: value.destination.into(),
                frame: Frame::ReceiveReady(ReceiveReady {
                    c_or_r: value.command_or_response.map(|v| v.into()),
                    p_or_f: frame.poll_or_final,
                    receive_sequence: frame.receive_sequence,
                }),
            },
            ax25::frame::FrameContent::ReceiveNotReady(frame) => AddressedFrame {
                source: value.source.into(),
                destination: value.destination.into(),
                frame: Frame::ReceiveNotReady(ReceiveNotReady {
                    c_or_r: value.command_or_response.map(|v| v.into()),
                    p_or_f: frame.poll_or_final,
                    receive_sequence: frame.receive_sequence,
                }),
            },
            ax25::frame::FrameContent::Reject(frame) => AddressedFrame {
                source: value.source.into(),
                destination: value.destination.into(),
                frame: Frame::Reject(Reject {
                    c_or_r: value.command_or_response.map(|v| v.into()),
                    p_or_f: frame.poll_or_final,
                    receive_sequence: frame.receive_sequence,
                }),
            },
            ax25::frame::FrameContent::SelectiveReject(frame) => AddressedFrame {
                source: value.source.into(),
                destination: value.destination.into(),
                frame: Frame::SelectiveReject(SelectiveReject {
                    p_or_f: frame.poll_or_final,
                    receive_sequence: frame.receive_sequence,
                }),
            },
            ax25::frame::FrameContent::SetAsynchronousBalancedMode(frame) => AddressedFrame {
                source: value.source.into(),
                destination: value.destination.into(),
                frame: Frame::SetAsynchronousBalancedMode(SetAsynchronousBalancedMode {
                    p_or_f: frame.poll,
                }),
            },
            ax25::frame::FrameContent::SetAsynchronousBalancedModeExtended(frame) => {
                AddressedFrame {
                    source: value.source.into(),
                    destination: value.destination.into(),
                    frame: Frame::SetAsynchronousBalancedModeExtended(
                        SetAsynchronousBalancedModeExtended { p_or_f: frame.poll },
                    ),
                }
            }
            ax25::frame::FrameContent::Disconnect(frame) => AddressedFrame {
                source: value.source.into(),
                destination: value.destination.into(),
                frame: Frame::Disconnect(Disconnect { poll: frame.poll }),
            },
            ax25::frame::FrameContent::DisconnectedMode(frame) => AddressedFrame {
                source: value.source.into(),
                destination: value.destination.into(),
                frame: Frame::DisconnectedMode(DisconnectedMode {
                    is_final: frame.final_bit,
                }),
            },
            ax25::frame::FrameContent::UnnumberedAcknowledge(frame) => AddressedFrame {
                source: value.source.into(),
                destination: value.destination.into(),
                frame: Frame::UnnumberedAcknowledge(UnnumberedAcknowledge {
                    is_final: frame.final_bit,
                }),
            },
            ax25::frame::FrameContent::UnnumberedInformation(frame) => AddressedFrame {
                source: value.source.into(),
                destination: value.destination.into(),
                frame: Frame::UnnumberedInformation(UnnumberedInformation {
                    c_or_r: value.command_or_response.map(|v| v.into()),
                    p_or_f: frame.poll_or_final,
                    info: frame.info.into(),
                }),
            },
            ax25::frame::FrameContent::FrameReject(_) => todo!(),
            ax25::frame::FrameContent::UnknownContent(_) => todo!(),
            ax25::frame::FrameContent::ExchangeIdentification(_) => todo!(),
            ax25::frame::FrameContent::Test(_) => todo!(),
        }
    }
}

impl From<AddressedFrame> for ax25::frame::Ax25Frame {
    fn from(value: AddressedFrame) -> Self {
        match value.frame {
            Frame::Disconnect(frame) => ax25::frame::Ax25Frame {
                source: value.source.with_ssid().parse().unwrap(),
                destination: value.destination.with_ssid().parse().unwrap(),
                route: vec![],
                command_or_response: Some(ax25::frame::CommandResponse::Command),
                content: ax25::frame::FrameContent::Disconnect(ax25::frame::Disconnect {
                    poll: frame.poll,
                }),
            },
            Frame::DisconnectedMode(frame) => ax25::frame::Ax25Frame {
                source: value.source.with_ssid().parse().unwrap(),
                destination: value.destination.with_ssid().parse().unwrap(),
                route: vec![],
                command_or_response: Some(ax25::frame::CommandResponse::Command),
                content: ax25::frame::FrameContent::DisconnectedMode(
                    ax25::frame::DisconnectedMode {
                        final_bit: frame.is_final,
                    },
                ),
            },
            Frame::UnnumberedAcknowledge(frame) => ax25::frame::Ax25Frame {
                source: value.source.with_ssid().parse().unwrap(),
                destination: value.destination.with_ssid().parse().unwrap(),
                route: vec![],
                command_or_response: Some(ax25::frame::CommandResponse::Command),
                content: ax25::frame::FrameContent::UnnumberedAcknowledge(
                    ax25::frame::UnnumberedAcknowledge {
                        final_bit: frame.is_final,
                    },
                ),
            },
            Frame::UnnumberedInformation(frame) => ax25::frame::Ax25Frame {
                source: value.source.with_ssid().parse().unwrap(),
                destination: value.destination.with_ssid().parse().unwrap(),
                route: vec![],
                command_or_response: frame.c_or_r.map(|v| v.into()),
                content: ax25::frame::FrameContent::UnnumberedInformation(
                    ax25::frame::UnnumberedInformation {
                        pid: ax25::frame::ProtocolIdentifier::None,
                        info: frame.info.into(),
                        poll_or_final: frame.p_or_f,
                    },
                ),
            },
            Frame::ReceiveReady(frame) => ax25::frame::Ax25Frame {
                source: value.source.with_ssid().parse().unwrap(),
                destination: value.destination.with_ssid().parse().unwrap(),
                route: vec![],
                command_or_response: frame.c_or_r.map(|v| v.into()),
                content: ax25::frame::FrameContent::ReceiveReady(ax25::frame::ReceiveReady {
                    receive_sequence: frame.receive_sequence,
                    poll_or_final: frame.p_or_f,
                }),
            },
            Frame::ReceiveNotReady(frame) => ax25::frame::Ax25Frame {
                source: value.source.with_ssid().parse().unwrap(),
                destination: value.destination.with_ssid().parse().unwrap(),
                route: vec![],
                command_or_response: frame.c_or_r.map(|v| v.into()),
                content: ax25::frame::FrameContent::ReceiveNotReady(ax25::frame::ReceiveNotReady {
                    receive_sequence: frame.receive_sequence,
                    poll_or_final: frame.p_or_f,
                }),
            },
            Frame::SetAsynchronousBalancedMode(frame) => ax25::frame::Ax25Frame {
                source: value.source.with_ssid().parse().unwrap(),
                destination: value.destination.with_ssid().parse().unwrap(),
                route: vec![],
                command_or_response: Some(ax25::frame::CommandResponse::Command),
                content: ax25::frame::FrameContent::SetAsynchronousBalancedMode(
                    ax25::frame::SetAsynchronousBalancedMode { poll: frame.p_or_f },
                ),
            },
            Frame::Information(frame) => ax25::frame::Ax25Frame {
                source: value.source.with_ssid().parse().unwrap(),
                destination: value.destination.with_ssid().parse().unwrap(),
                route: vec![],
                command_or_response: frame.c_or_r.map(|v| v.into()),
                content: ax25::frame::FrameContent::Information(ax25::frame::Information {
                    pid: ax25::frame::ProtocolIdentifier::None,
                    info: frame.info.into(),
                    receive_sequence: frame.receive_sequence,
                    send_sequence: frame.send_sequence,
                    poll: frame.poll,
                }),
            },
            Frame::Reject(frame) => ax25::frame::Ax25Frame {
                source: value.source.with_ssid().parse().unwrap(),
                destination: value.destination.with_ssid().parse().unwrap(),
                route: vec![],
                command_or_response: frame.c_or_r.map(|v| v.into()),
                content: ax25::frame::FrameContent::Reject(ax25::frame::Reject {
                    receive_sequence: frame.receive_sequence,
                    poll_or_final: frame.p_or_f,
                }),
            },
            Frame::SelectiveReject(frame) => ax25::frame::Ax25Frame {
                source: value.source.with_ssid().parse().unwrap(),
                destination: value.destination.with_ssid().parse().unwrap(),
                route: vec![],
                command_or_response: Some(ax25::frame::CommandResponse::Command),
                content: ax25::frame::FrameContent::SelectiveReject(ax25::frame::SelectiveReject {
                    receive_sequence: frame.receive_sequence,
                    poll_or_final: frame.p_or_f,
                }),
            },
            Frame::SetAsynchronousBalancedModeExtended(frame) => ax25::frame::Ax25Frame {
                source: value.source.with_ssid().parse().unwrap(),
                destination: value.destination.with_ssid().parse().unwrap(),
                route: vec![],
                command_or_response: Some(ax25::frame::CommandResponse::Command),
                content: ax25::frame::FrameContent::SetAsynchronousBalancedModeExtended(
                    ax25::frame::SetAsynchronousBalancedModeExtended { poll: frame.p_or_f },
                ),
            },
            Frame::FrameReject(frame) => ax25::frame::Ax25Frame {
                source: value.source.with_ssid().parse().unwrap(),
                destination: value.destination.with_ssid().parse().unwrap(),
                route: vec![],
                command_or_response: Some(ax25::frame::CommandResponse::Command),
                content: ax25::frame::FrameContent::FrameReject(ax25::frame::FrameReject {
                    final_bit: frame.is_final,
                    rejected_control_field_raw: 0,
                    z: false,
                    y: false,
                    x: false,
                    w: false,
                    receive_sequence: 0,
                    send_sequence: 0,
                    command_response: ax25::frame::CommandResponse::Command,
                }),
            },
        }
    }
}

pub(super) struct SetAsynchronous {
    #[allow(dead_code)]
    pub p_or_f: bool,
    #[allow(dead_code)]
    pub version: Version,
}

impl From<SetAsynchronousBalancedMode> for SetAsynchronous {
    fn from(value: SetAsynchronousBalancedMode) -> Self {
        Self {
            p_or_f: value.p_or_f,
            version: Version::Ax20,
        }
    }
}

impl From<SetAsynchronousBalancedModeExtended> for SetAsynchronous {
    fn from(value: SetAsynchronousBalancedModeExtended) -> Self {
        Self {
            p_or_f: value.p_or_f,
            version: Version::Ax22,
        }
    }
}

pub(super) struct Readiness {
    pub c_or_r: Option<CommandResponse>,
    pub p_or_f: bool,
    pub receive_sequence: Seq,
    #[allow(dead_code)]
    pub peer_busy: bool,
}

impl From<ReceiveReady> for Readiness {
    fn from(value: ReceiveReady) -> Self {
        Self {
            c_or_r: value.c_or_r,
            p_or_f: value.p_or_f,
            receive_sequence: Seq::new_modulo_8(value.receive_sequence),
            peer_busy: false,
        }
    }
}

impl From<ReceiveNotReady> for Readiness {
    fn from(value: ReceiveNotReady) -> Self {
        Self {
            c_or_r: value.c_or_r,
            p_or_f: value.p_or_f,
            receive_sequence: Seq::new_modulo_8(value.receive_sequence),
            peer_busy: true,
        }
    }
}
