use itertools::Itertools;
use std::{
    cmp::Ordering,
    collections::{BTreeMap, VecDeque},
    hash::Hash,
    ops::AddAssign,
};
use tracing::*;

use super::frames::*;
use super::states::{ErrorCode, Generated, In, Out, Received, State};

pub const DEFAULT_T1: i64 = 5000;
pub const DEFAULT_T3: i64 = 30_000;

#[derive(Debug, PartialEq, Clone)]
pub struct Parameters {
    /// The default maximum number of octets allowed in the I field is 256. This
    /// variable is negotiable between end stations. The I field is an integral
    /// number of octets.
    pub packet_length_n1: usize,
    /// Maximum number of retries permitted.
    pub retries_n2: usize,
    /// Next value for T1; default initial value is initial value of SRT.
    pub timer_1_value: TimerValue,
    /// T3, the Inactive Link Timer, maintains link integrity whenever T1 is not
    /// running. It is recommended that whenever there are no outstanding
    /// unacknowledged I frames or P-bit frames (during the information-transfer
    /// state), an RR or RNR frame with the P bit set to “1” be sent every T3
    /// time units to query the status of the other TNC. The period of T3 is
    /// locally defined, and depends greatly on Layer 1 operation. T3 should be
    /// greater than T1; it may be very large on channels of high integrity.
    pub timer_3_value: TimerValue,
    /// 6.7.2.3. Maximum Number of I Frames Outstanding (k) The maximum number
    /// of I frames outstanding at a time is seven (modulo 8) or 127 (modulo
    /// 128).
    pub max_frames_k: u8,
    pub immediate_ack_on_information_poll: bool,
}

impl Parameters {
    pub fn verify(&self) {
        assert!(self.max_frames_k <= 7);
    }
}

impl Default for Parameters {
    fn default() -> Self {
        Self {
            packet_length_n1: 225,
            retries_n2: 3,
            timer_1_value: DEFAULT_T1.into(),
            timer_3_value: DEFAULT_T3.into(),
            max_frames_k: 7,
            immediate_ack_on_information_poll: true,
        }
    }
}

impl From<Parameters> for Flags {
    fn from(value: Parameters) -> Self {
        value.verify();

        Self {
            parameters: value,
            ..Default::default()
        }
    }
}

#[derive(Debug, PartialEq, Clone, Default)]
pub(super) struct Flags {
    pub parameters: Parameters,
    /// Version of Ax25
    pub version: Version,
    /// Retry count.
    pub retry_count: usize,
    /// SABM was sent by request of Layer 3; i.e., DL-CONNECT Request primitive
    pub layer_3_initiated: bool,
    /// Remote station is busy and cannot receive I frames.
    pub peer_receiver_busy: bool,
    /// Layer 3 is busy and cannot receive I frames.
    pub own_receiver_busy: bool,
    /// A REJ frame has been sent to the remote station.
    pub reject_exception: bool,
    /// A SREJ frame has been sent to the remote station.
    pub selective_reject_exception: usize,
    /// SREJ is enabled. Page 97
    pub selective_reject_enabled: bool,
    /// I frames have been successfully received but not yet acknowledged to the
    /// remote station.
    pub acknowledge_pending: bool,
    /// Smoothed round trip time.
    pub smoothed_round_trip_time: usize,
    /// Used in Disconnected state to refuse incoming connections.
    pub unable_to_establish: bool,
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Copy, Clone, Default)]
pub struct TimerValue(i64);

impl From<i64> for TimerValue {
    fn from(value: i64) -> Self {
        TimerValue(value)
    }
}

impl AddAssign for TimerValue {
    fn add_assign(&mut self, rhs: Self) {
        self.0 += rhs.0;
    }
}

impl TimerValue {
    pub fn elapsed(self, elapsed: impl Into<TimerValue>) -> Option<Self> {
        let elapsed = elapsed.into();
        if self.0 >= elapsed.0 {
            Some(Self(self.0 - elapsed.0))
        } else {
            None
        }
    }
}

impl From<std::time::Duration> for TimerValue {
    fn from(value: std::time::Duration) -> Self {
        Self(value.as_millis() as i64)
    }
}

impl From<TimerValue> for std::time::Duration {
    fn from(value: TimerValue) -> Self {
        std::time::Duration::from_millis(value.0 as u64)
    }
}

#[derive(Debug, PartialEq, Clone)]
pub enum Timer {
    Stopped,
    Started(TimerValue),
}

impl Default for Timer {
    fn default() -> Self {
        Self::Stopped
    }
}

impl Timer {
    pub fn is_started(&self) -> bool {
        matches!(self, Timer::Started(_))
    }

    fn elapsed(&self, elapsed: TimerValue) -> (Self, bool) {
        match self {
            Timer::Stopped => (Timer::Stopped, false),
            Timer::Started(remaining) => match remaining.elapsed(elapsed) {
                Some(remaining) => (Timer::Started(remaining), false),
                None => (Timer::Stopped, true),
            },
        }
    }

    fn remaining(&self) -> Option<TimerValue> {
        match self {
            Timer::Started(remaining) => Some(*remaining),
            Timer::Stopped => None,
        }
    }
}

#[derive(Debug, PartialEq, Default, Clone)]
pub(super) struct Timers {
    /// Outstanding I frame or P-bit
    pub timer_1: Timer,
    /// Idle supervision (keep alive)
    pub timer_3: Timer,
}

impl Timers {
    pub fn elapsed(&self, elapsed: TimerValue) -> Option<(Timers, Vec<Received>)> {
        if self.timer_1 == Timer::Stopped && self.timer_3 == Timer::Stopped {
            None
        } else {
            let mut signals = Vec::new();
            let (timer_1, t1) = self.timer_1.elapsed(elapsed);
            let (timer_3, t3) = self.timer_3.elapsed(elapsed);
            if t1 {
                signals.push(Received::Primitive(In::Timer(StandardTimer::T1)));
            }
            if t3 {
                signals.push(Received::Primitive(In::Timer(StandardTimer::T3)));
            }
            Some((Timers { timer_1, timer_3 }, signals))
        }
    }

    pub fn remaining(&self) -> Option<TimerValue> {
        [self.timer_1.remaining(), self.timer_3.remaining()]
            .into_iter()
            .flatten()
            .sorted()
            .next()
    }
}

#[derive(Debug, PartialEq, Eq, Copy, Clone, Hash)]
pub struct Seq(i32, i32);

impl Default for Seq {
    fn default() -> Self {
        Self(Default::default(), 8)
    }
}

impl Seq {
    pub fn new_modulo_8(value: u8) -> Self {
        Self(value as i32, 8).add(0)
    }

    #[allow(dead_code)]
    pub fn new_modulo_128(value: u8) -> Self {
        Self(value as i32, 128).add(0)
    }

    pub fn add(self, value: i32) -> Self {
        Seq((self.0 + value) & (self.1 - 1), self.1)
    }

    pub fn sub(&self, value: Seq) -> i32 {
        self.add(-value.0).0
    }

    pub fn greater_than(&self, rhs: Seq) -> bool {
        self.0 > rhs.0
    }
}

macro_rules! uint_half {
    ($x:expr) => {
        1 << ($x - 1)
    };
}

#[allow(clippy::non_canonical_partial_ord_impl)]
impl PartialOrd for Seq {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        assert!(self.1 == 8);

        let selph = self.0 as u8;
        let other = other.0 as u8;
        if selph == other {
            Some(Ordering::Equal)
        } else if (selph < other && (other - selph) < uint_half!(3))
            || (selph > other && (selph - other) > uint_half!(3))
        {
            Some(Ordering::Less)
        } else if (selph < other && (other - selph) > uint_half!(3))
            || (selph > other && (selph - other) < uint_half!(3))
        {
            Some(Ordering::Greater)
        } else {
            None
        }
    }
}

impl Ord for Seq {
    fn cmp(&self, other: &Self) -> Ordering {
        self.partial_cmp(other).unwrap_or(Ordering::Equal)
    }
}

impl From<u8> for Seq {
    fn from(value: u8) -> Self {
        Self::new_modulo_8(value)
    }
}

impl From<Seq> for u8 {
    fn from(val: Seq) -> Self {
        val.0 as u8
    }
}

#[derive(Debug, PartialEq, Default, Clone)]
pub(super) struct Sequences {
    /// 4.2.4.1. Send State Variable V(S)
    /// The send state variable exists within the TNC and is never sent. It contains
    /// the next sequential number to be assigned to the next transmitted I frame.
    /// This variable is updated with the transmission of each I frame.
    pub vs: Seq,
    /// 4.2.4.3. Receive State Variable V(R)
    /// The receive state variable exists within the TNC. It contains the sequence
    /// number of the next expected received I frame. This variable is updated upon
    /// the reception of an error-free I frame whose send sequence number equals the
    /// present received state variable value.
    pub vr: Seq,
    /// 4.2.4.5. Acknowledge State Variable V(A)
    /// The acknowledge state variable exists within the TNC and is never sent. It
    /// contains the sequence number of the last frame acknowledged by its peer
    /// [V(A)-1 equals the N(S) of the last acknowledged I frame].
    pub va: Seq,
    // 4.2.4.4. Received Sequence Number N(R)
    // The received sequence number exists in both I and S frames. Prior to sending
    // an I or S frame, this variable is updated to equal that of the received state
    // variable, thus implicitly acknowledging the proper reception of all frames up
    // to and including N(R)-1.
    // Jacob: May be possible to remove this from the struct, and set directly
    // on outgoing frames?
    // pub nr: u8,
    // 4.2.4.2. Send Sequence Number N(S)
    // The send sequence number is found in the control field of all I frames. It
    // contains the sequence number of the I frame being sent. Just prior to the
    // transmission of the I frame, N(S) is updated to equal the send state
    // variable.
    // pub ns: u8,
}

impl Sequences {
    pub fn has_unacknowledged_data(&self) -> bool {
        !self.all_data_acknowledged()
    }

    pub fn all_data_acknowledged(&self) -> bool {
        self.va == self.vs
    }

    /// Page 95
    pub fn receive_sequence_ok(&self, receive_sequence: impl Into<Seq>) -> bool {
        let nr = receive_sequence.into();
        let relatie_va = self.va.sub(self.va);
        let relatie_nr = nr.sub(self.va);
        let relatie_vs = self.vs.sub(self.va);
        relatie_va <= relatie_nr && relatie_nr <= relatie_vs
    }
}

#[derive(Debug, PartialEq, Clone, Default)]
pub enum Version {
    #[default]
    Ax20,
    Ax22,
}

impl Version {
    pub(super) fn sabm_frame(&self) -> Frame {
        match self {
            Version::Ax20 => {
                Frame::SetAsynchronousBalancedMode(SetAsynchronousBalancedMode { p_or_f: true })
            }
            Version::Ax22 => {
                Frame::SetAsynchronousBalancedModeExtended(SetAsynchronousBalancedModeExtended {
                    p_or_f: true,
                })
            }
        }
    }
}

#[derive(Debug, PartialEq, Default)]
pub(super) struct Transition<T> {
    inner: T,
    generated: Vec<Generated>,
}

impl<T> Transition<T> {
    pub fn new(inner: T) -> Self {
        Self {
            inner,
            generated: Vec::default(),
        }
    }

    pub fn inner_ref(&self) -> &T {
        &self.inner
    }

    pub fn split(self) -> (T, Vec<Generated>) {
        (self.inner, self.generated)
    }
}

impl From<Inner> for Transition<Inner> {
    fn from(value: Inner) -> Self {
        Self {
            inner: value,
            generated: Vec::new(),
        }
    }
}

impl Transition<Inner> {
    pub fn with(self, generated: impl Into<Generated>) -> Self {
        Self {
            inner: self.inner,
            generated: self
                .generated
                .into_iter()
                .chain(std::iter::once(generated.into()))
                .collect_vec(),
        }
    }

    pub fn send(self, frame: impl Into<Frame>) -> Self {
        self.with(frame.into())
    }

    pub fn error(self, p: impl Into<ErrorCode>) -> Self {
        self.primitive(Out::ErrorIndication(p.into()))
    }

    pub fn primitive(self, p: impl Into<Out>) -> Self {
        self.with(Generated::Primitive(p.into()))
    }

    /// Page 108
    pub fn ui_check(self, frame: UnnumberedInformation) -> Self {
        if frame.c_or_r == Some(CommandResponse::Command) {
            // if !(info field length <= N1 and content is octet aligned) ErrorCode::K
            self.primitive(Out::UnitDataIndication(frame.info))
        } else {
            self.error(ErrorCode::Q)
        }
    }

    /// Page 108 Figure C4.7
    pub fn check_need_for_response(self, c_or_r: Option<CommandResponse>, p_or_f: bool) -> Self {
        let c_or_r = c_or_r.unwrap_or_else(|| {
            warn!("assuming command");
            CommandResponse::Command
        });

        match (c_or_r, p_or_f) {
            (CommandResponse::Command, true) => self.enquiry_response(true),
            (CommandResponse::Command, false) => self,
            (CommandResponse::Response, true) => self,
            // NOTE Spec uses `1 here.
            (CommandResponse::Response, false) => self.error(ErrorCode::A),
        }
    }

    /// Page 106 Figure C4.7
    pub fn nr_error_recovery(self) -> Self {
        self.error(ErrorCode::J)
            .establish_data_link()
            .inner(|i| i.layer_3_initiated(false))
    }

    /// Page 106 Figure C4.7
    pub fn establish_data_link(self) -> Self {
        self.clear_exception_conditions()
            .retry_count(0)
            .send(SetAsynchronousBalancedMode { p_or_f: true })
            .stop_timer_3()
            .start_timer_1()
    }

    /// Page 106 Figure C4.7
    pub fn transmit_enquiry(self) -> Self {
        let nr = self.inner.sequences.vr;
        if self.inner.flags.own_receiver_busy {
            self.send(ReceiveNotReady {
                c_or_r: Some(CommandResponse::Command),
                p_or_f: true,
                receive_sequence: nr.into(),
            })
            .clear_acknowledge_pending()
            .start_timer_1()
        } else {
            self.send(ReceiveReady {
                c_or_r: Some(CommandResponse::Command),
                p_or_f: true,
                receive_sequence: nr.into(),
            })
            .clear_acknowledge_pending()
            .start_timer_1()
        }
    }

    /// Page 106 Figure C4.7
    /// Note Very similar to transmit_enquiry, except for start_timer_1. One
    /// thing of note is that the flowchart says Command and nearly everybody
    /// else, myself included, thinks this should be a Response.
    pub fn enquiry_response(self, is_final: bool) -> Self {
        let nr = self.inner.sequences.vr;
        if self.inner.flags.own_receiver_busy {
            self.send(ReceiveNotReady {
                c_or_r: Some(CommandResponse::Response),
                p_or_f: is_final,
                receive_sequence: nr.into(),
            })
            .clear_acknowledge_pending()
        } else {
            self.send(ReceiveReady {
                c_or_r: Some(CommandResponse::Response),
                p_or_f: is_final,
                receive_sequence: nr.into(),
            })
            .clear_acknowledge_pending()
        }
    }

    pub fn drain_incoming(self) -> Transition<Inner> {
        let (inner, drained) = self.inner.drain_incoming();

        trace!("{}:{} drained {:?}", file!(), line!(), drained.len());

        Self {
            inner,
            generated: self
                .generated
                .into_iter()
                .chain(
                    drained
                        .into_iter()
                        .map(|info| Generated::Primitive(Out::DataIndication(info))),
                )
                .collect(),
        }
    }

    pub fn pop_from_queue(self) -> (Self, Option<Info>) {
        let (inner, info) = self.inner.pop_from_queue();
        (Self { inner, ..self }, info)
    }

    pub fn into_state<S: From<Inner>, T: FnOnce(S) -> State>(self, map: T) -> Transition<State> {
        Transition {
            inner: map(self.inner.into()),
            generated: self.generated,
        }
    }

    pub fn inner(self, map: impl FnOnce(Inner) -> Inner) -> Self {
        Self {
            inner: map(self.inner),
            generated: self.generated,
        }
    }

    pub fn clear_exception_conditions(self) -> Self {
        Self {
            inner: self.inner.clear_exception_conditions(),
            generated: self.generated,
        }
    }

    fn retry_count(self, value: usize) -> Self {
        Self {
            inner: self.inner.retry_count(value),
            generated: self.generated,
        }
    }

    pub fn start_timer_1(self) -> Self {
        Self {
            inner: self.inner.start_timer_1(),
            generated: self.generated,
        }
    }

    pub fn stop_timer_1(self) -> Self {
        Self {
            inner: self.inner.stop_timer_1(),
            generated: self.generated,
        }
    }

    pub fn start_timer_3(self) -> Self {
        Self {
            inner: self.inner.start_timer_3(),
            generated: self.generated,
        }
    }

    pub fn stop_timer_3(self) -> Self {
        Self {
            inner: self.inner.stop_timer_3(),
            generated: self.generated,
        }
    }

    pub fn send_receive_ready_response(self) -> Self {
        debug!("send receive-ready response");
        let receive_sequence = self.inner.sequences.vr.into();
        self.clear_acknowledge_pending().send(ReceiveReady {
            c_or_r: Some(CommandResponse::Response),
            p_or_f: true,
            receive_sequence,
        })
    }

    pub fn send_receive_ready_command(self, p_or_f: bool) -> Self {
        debug!("send receive-ready command");
        let receive_sequence = self.inner.sequences.vr.into();
        self.clear_acknowledge_pending().send(ReceiveReady {
            c_or_r: Some(CommandResponse::Command),
            p_or_f,
            receive_sequence,
        })
    }

    pub fn clear_acknowledge_pending(self) -> Self {
        if self.inner.flags.acknowledge_pending {
            self.inner(|inner| Inner {
                flags: Flags {
                    acknowledge_pending: false,
                    ..inner.flags
                },
                ..inner
            })
            .primitive(Out::AcknowledgePending(false))
        } else {
            self
        }
    }

    pub fn set_acknowledge_pending_if_not(self) -> Self {
        if !self.inner.flags.acknowledge_pending {
            self.inner(|i| i.set_acknowledge_pending())
                .primitive(Out::AcknowledgePending(true))
        } else {
            self
        }
    }

    pub fn invoke_retransmission(self, receive_sequence: Seq) -> Transition<Inner> {
        self.inner(|i| i.requeue_from_pending_ack(receive_sequence))
    }
}

#[derive(Debug, PartialEq, Default, Clone)]
pub(super) struct Inner {
    pub flags: Flags,
    pub timers: Timers,
    pub queue: VecDeque<Info>,
    pub pending_ack: BTreeMap<Seq, Info>,
    pub incoming: BTreeMap<Seq, Info>,
    pub sequences: Sequences,
}

impl Inner {
    pub fn can_send_frames(&self) -> bool {
        self.sequences.vs
            != self
                .sequences
                .va
                .add(self.flags.parameters.max_frames_k as i32)
    }

    pub fn frame_queue_empty(&self) -> bool {
        self.queue.is_empty()
    }

    pub fn at_retry_limit(&self) -> bool {
        self.flags.retry_count >= self.flags.parameters.retries_n2
    }

    pub fn layer_3_initiated(self, value: bool) -> Self {
        Self {
            flags: Flags {
                layer_3_initiated: value,
                ..self.flags
            },
            ..self
        }
    }

    pub fn version(self, version: Version) -> Self {
        Self {
            flags: Flags {
                version,
                ..self.flags
            },
            ..self
        }
    }

    pub fn with_timers(self, timers: Timers) -> Self {
        Self { timers, ..self }
    }

    pub fn with_flags(self, flags: Flags) -> Self {
        Self { flags, ..self }
    }

    fn with_sequences(self, sequences: Sequences) -> Self {
        Self { sequences, ..self }
    }

    pub fn retry_count(self, rc: usize) -> Self {
        Self {
            flags: Flags {
                retry_count: rc,
                ..self.flags
            },
            ..self
        }
    }

    pub fn retrying(self) -> Self {
        let value = self.flags.retry_count + 1;
        self.retry_count(value)
    }

    #[cfg(test)]
    pub fn retry_count_of(self, rc: usize, n2: usize) -> Self {
        Self {
            flags: Flags {
                retry_count: rc,
                parameters: Parameters {
                    retries_n2: n2,
                    ..self.flags.parameters
                },
                ..self.flags
            },
            ..self
        }
    }

    #[cfg(test)]
    pub fn queued(self, info: Info) -> Self {
        Self {
            queue: self
                .queue
                .into_iter()
                .chain(std::iter::once(info))
                .collect(),
            ..self
        }
    }

    pub fn queue_multiple_if_necessary(self, info: Info) -> Self {
        Self {
            queue: self
                .queue
                .into_iter()
                .chain(info.chunks(self.flags.parameters.packet_length_n1))
                .collect(),
            ..self
        }
    }

    pub fn discard_frame_queue(self) -> Self {
        Self {
            queue: Default::default(),
            ..self
        }
    }

    // Could this be prettier?
    pub fn pop_from_queue(self) -> (Self, Option<Info>) {
        let mut queue = self.queue;
        let popped = queue.pop_front();
        let s = Self { queue, ..self };

        (s, popped)
    }

    #[cfg(test)]
    pub fn reached_max_frames(self) -> Self {
        let max_frames_k = self.flags.parameters.max_frames_k;
        self.with_sequences(Sequences {
            va: Seq::new_modulo_8(1),
            vs: Seq::new_modulo_8(1 + max_frames_k),
            ..Default::default()
        })
    }

    pub fn increase_vr(self) -> Self {
        let sequences = self.sequences.clone();
        self.with_sequences(Sequences {
            vr: sequences.vr.add(1),
            ..sequences
        })
    }

    pub fn increase_vs(self) -> Self {
        let sequences = self.sequences.clone();
        self.with_sequences(Sequences {
            vs: sequences.vs.add(1),
            ..sequences
        })
    }

    #[cfg(test)]
    pub fn increase_va(self) -> Self {
        let sequences = self.sequences.clone();
        self.with_sequences(Sequences {
            va: sequences.va.add(1),
            ..sequences
        })
    }

    pub fn set_va(self, receive_sequence: Seq) -> Self {
        let mut pending_ack = self.pending_ack;
        let mut va = self.sequences.va;
        while va != receive_sequence {
            pending_ack.remove(&va);
            va = va.add(1);
        }

        Self {
            sequences: Sequences {
                va: receive_sequence,
                ..self.sequences
            },
            pending_ack,
            ..self
        }
    }

    pub fn clear_sequences(self) -> Self {
        Self {
            sequences: Default::default(),
            ..self
        }
    }

    #[cfg(test)]
    pub fn unacknowledged_data(self) -> Self {
        self.with_sequences(Sequences {
            va: Seq::new_modulo_8(1),
            vs: Seq::new_modulo_8(2),
            ..Default::default()
        })
    }

    pub fn all_data_acknowledged(&self) -> bool {
        self.sequences.all_data_acknowledged()
    }

    pub fn receive_sequence_ok(&self, receive_sequence: impl Into<Seq>) -> bool {
        self.sequences.receive_sequence_ok(receive_sequence)
    }

    pub fn set_srt_and_timer_1_value(self) -> Self {
        Self {
            flags: Flags { ..self.flags },
            ..self
        }
    }

    #[cfg(test)]
    pub fn timer_1_value(self, value: impl Into<TimerValue>) -> Self {
        Self {
            flags: Flags {
                parameters: Parameters {
                    timer_1_value: value.into(),
                    ..self.flags.parameters
                },
                ..self.flags
            },
            ..self
        }
    }

    #[cfg(test)]
    pub fn set_packet_length_n1(self, value: usize) -> Self {
        Self {
            flags: Flags {
                parameters: Parameters {
                    packet_length_n1: value,
                    ..self.flags.parameters
                },
                ..self.flags
            },
            ..self
        }
    }

    #[cfg(test)]
    pub fn set_immediate_ack_on_information_poll(self, value: bool) -> Self {
        Self {
            flags: Flags {
                parameters: Parameters {
                    immediate_ack_on_information_poll: value,
                    ..self.flags.parameters
                },
                ..self.flags
            },
            ..self
        }
    }

    pub fn select_timer_1_value(self) -> Self {
        Self {
            flags: Flags { ..self.flags },
            ..self
        }
    }

    pub fn start_timer_3(self) -> Self {
        if self.timers.timer_3.is_started() {
            trace!("t3:started {:?}", self.flags.parameters.timer_3_value);
        } else {
            debug!("t3:started {:?}", self.flags.parameters.timer_3_value);
        }
        Self {
            timers: Timers {
                timer_3: Timer::Started(self.flags.parameters.timer_3_value),
                ..self.timers
            },
            ..self
        }
    }

    pub fn stop_timer_3(self) -> Self {
        if self.timers.timer_3.is_started() {
            debug!("t3:stopped");
        }
        Self {
            timers: Timers {
                timer_3: Timer::Stopped,
                ..self.timers
            },
            ..self
        }
    }

    pub fn start_timer_1(self) -> Self {
        let value = self.flags.parameters.timer_1_value;
        debug!("t1:started {:?}", value);
        Self {
            timers: Timers {
                timer_1: Timer::Started(value),
                ..self.timers
            },
            ..self
        }
    }

    pub fn stop_timer_1(self) -> Self {
        if self.timers.timer_1.is_started() {
            debug!("t1:stopped");
        }
        Self {
            timers: Timers {
                timer_1: Timer::Stopped,
                ..self.timers
            },
            ..self
        }
    }

    pub fn start_t1_if_stopped(self) -> Self {
        if self.timers.timer_1.is_started() {
            self
        } else {
            self.start_timer_1()
        }
    }

    #[cfg(test)]
    pub fn set_own_busy(self) -> Self {
        Self {
            flags: Flags {
                own_receiver_busy: true,
                ..self.flags
            },
            ..self
        }
    }

    pub fn set_peer_busy(self) -> Self {
        Self {
            flags: Flags {
                peer_receiver_busy: true,
                ..self.flags
            },
            ..self
        }
    }

    pub fn clear_peer_busy(self) -> Self {
        Self {
            flags: Flags {
                peer_receiver_busy: false,
                ..self.flags
            },
            ..self
        }
    }

    pub fn clear_exception_conditions(self) -> Self {
        Self {
            flags: Flags {
                peer_receiver_busy: false,
                reject_exception: false,
                own_receiver_busy: false,
                acknowledge_pending: false,
                ..self.flags
            },
            ..self
        }
    }

    pub fn set_acknowledge_pending(self) -> Self {
        Self {
            flags: Flags {
                acknowledge_pending: true,
                ..self.flags
            },
            ..self
        }
    }

    #[cfg(test)]
    pub fn clear_acknowledge_pending(self) -> Self {
        Self {
            flags: Flags {
                acknowledge_pending: false,
                ..self.flags
            },
            ..self
        }
    }

    #[cfg(test)]
    pub fn set_selective_reject_enabled(self, value: bool) -> Self {
        Self {
            flags: Flags {
                selective_reject_enabled: value,
                ..self.flags
            },
            ..self
        }
    }

    pub fn increase_selective_reject_exception(self) -> Self {
        let selective_reject_exception = self.flags.selective_reject_exception + 1;
        Self {
            flags: Flags {
                selective_reject_exception,
                ..self.flags
            },
            ..self
        }
    }

    #[cfg(test)]
    pub fn set_selective_reject_exception(self, value: usize) -> Self {
        Self {
            flags: Flags {
                selective_reject_exception: value,
                ..self.flags
            },
            ..self
        }
    }

    pub fn set_reject_exception(self) -> Self {
        Self {
            flags: Flags {
                reject_exception: true,
                ..self.flags
            },
            ..self
        }
    }

    pub fn clear_reject_exception(self) -> Self {
        Self {
            flags: Flags {
                reject_exception: false,
                ..self.flags
            },
            ..self
        }
    }

    /// Page 107 Figure C4.7
    pub fn check_i_frame_ackd(self, receive_sequence: Seq) -> Self {
        let peer_busy = self.flags.peer_receiver_busy;
        if peer_busy {
            self.set_va(receive_sequence)
                .start_timer_3()
                .start_t1_if_stopped()
        } else if receive_sequence == self.sequences.vs {
            if self.sequences.vs != self.sequences.va {
                debug!("ack-all ({:?}) -> t3:start, t1:stop", receive_sequence);
            }
            self.set_va(receive_sequence)
                .stop_timer_1()
                .start_timer_3()
                .select_timer_1_value()
        } else if receive_sequence != self.sequences.va {
            debug!("ack-semi ({:?}) -> t1:restart", receive_sequence);
            self.set_va(receive_sequence).start_timer_1()
        } else {
            self
        }
    }

    /// Page 97
    pub fn save_incoming(self, sequence: Seq, info: Info) -> Self {
        Self {
            incoming: self
                .incoming
                .into_iter()
                .chain(std::iter::once((sequence, info)))
                .collect(),
            ..self
        }
    }

    pub fn remove_incoming(self, sequence: Seq) -> (Self, Option<Info>) {
        let mut incoming = self.incoming;
        let removed = incoming.remove_entry(&sequence);
        (Self { incoming, ..self }, removed.map(|v| v.1))
    }

    fn try_incoming(self) -> (Self, Option<Info>) {
        let vr = self.sequences.vr;
        match self.remove_incoming(vr) {
            (new_self, Some(removed)) => (new_self.increase_vr(), Some(removed)),
            (new_self, None) => (new_self, None),
        }
    }

    pub fn drain_incoming(self) -> (Self, Vec<Info>) {
        let mut returning = Vec::new();
        let mut new_self = self;

        // Unhappy with this loop.
        loop {
            let (new, removed) = new_self.try_incoming();
            new_self = new;

            if let Some(removed) = removed {
                returning.push(removed);
            } else {
                break;
            }
        }

        (new_self, returning)
    }

    pub fn save_pending_ack(self, sequence: Seq, info: Info) -> Self {
        Self {
            pending_ack: self
                .pending_ack
                .into_iter()
                .chain(std::iter::once((sequence, info)))
                .collect(),
            ..self
        }
    }

    #[cfg(test)]
    pub fn remove_pending_ack(self, sequence: Seq) -> (Self, Option<Info>) {
        let mut pending_ack = self.pending_ack;
        let removed = pending_ack.remove_entry(&sequence);
        (
            Self {
                pending_ack,
                ..self
            },
            removed.map(|v| v.1),
        )
    }

    /// Invoke retransmission
    pub fn requeue_from_pending_ack(self, receive_sequence: Seq) -> Self {
        let mut queuing = receive_sequence;
        let mut pending_ack = self.pending_ack;
        let end = self.sequences.vs;
        let mut queue = self.queue;
        while queuing != end {
            if let Some(info) = pending_ack.remove(&queuing) {
                info!("requeue {:?}", queuing);
                queue.push_back(info);
            } else {
                panic!("no pending frame");
            }
            queuing = queuing.add(1);
        }
        Self {
            queue,
            pending_ack,
            ..self
        }
    }

    pub fn requeue_selected_from_pending_ack(self, receive_sequence: Seq) -> Self {
        match self.pending_ack.get(&receive_sequence) {
            Some(info) => Self {
                queue: self
                    .queue
                    .into_iter()
                    .chain(std::iter::once(info.clone()))
                    .collect(),
                ..self
            },
            None => {
                panic!("no pending frame");
            }
        }
    }

    pub fn transition(self) -> Transition<Inner> {
        Transition::new(self)
    }
}

#[derive(Debug, PartialEq)]
pub enum StandardTimer {
    /// 6.7.1.1. Acknowledgment Timer T1
    /// T1, the Acknowledgement Timer, ensures that a TNC does not wait
    /// indefinitely for a response to a frame it sends. This timer cannot be
    /// expressed in absolute time; the time required to send frames varies
    /// greatly with the signaling rate used at Layer 1. T1 should take at least
    /// twice the amount of time it would take to send maximum length frame to
    /// the distant TNC and get the proper response frame back from the distant
    /// TNC. This allows time for the distant TNC to do some processing before
    /// responding.  If Layer 2 repeaters are used, the value of T1 should be
    /// adjusted according to the number of repeaters through which the frame is
    /// being transferred.
    T1,
    /// 6.7.1.3. Inactive Link Timer T3
    /// T3, the Inactive Link Timer, maintains link integrity whenever T1 is not
    /// running. It is recommended that whenever there are no outstanding
    /// unacknowledged I frames or P-bit frames (during the information-transfer
    /// state), an RR or RNR frame with the P bit set to “1” be sent every T3
    /// time units to query the status of the other TNC. The period of T3 is
    /// locally defined, and depends greatly on Layer 1 operation. T3 should be
    /// greater than T1; it may be very large on channels of high integrity.
    T3,
}
