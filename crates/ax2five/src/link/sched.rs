use ax25::frame::Ax25Frame;
use chrono::{DateTime, Utc};
use itertools::Itertools;
use std::{collections::HashMap, hash::Hash};
use thiserror::Error;
use tracing::*;

use crate::{
    callsign::{Callsign, Pair},
    Parameters,
};

use super::{
    frames::{AddressedFrame, Frame},
    inner::{Inner, TimerValue},
    states::{Generated, In, Out, Received, State},
};

pub trait TimerExpiration {
    fn timer_expiration(&self) -> Option<std::time::Duration> {
        None
    }
}

#[derive(Debug)]
pub struct DataLink {
    source: Callsign,
    destination: Callsign,
    state: Option<State>,
    signals: Vec<Received>,
    rx_buffer: Vec<u8>,
}

impl DataLink {
    fn new(
        source: Callsign,
        destination: Callsign,
        parameters: Parameters,
        signals: Vec<Received>,
    ) -> Self {
        Self {
            source,
            destination,
            state: Some(State::Disconnected(
                Inner::default().with_flags(parameters.into()).into(),
            )),
            signals,
            rx_buffer: Default::default(),
        }
    }

    fn tick(&mut self, elapsed: TimerValue) -> Result<Vec<TickOut>, Error> {
        let (after_tick, timer_signals) = self.state.take().unwrap().tick(elapsed);
        self.state = Some(after_tick);

        let received = timer_signals
            .into_iter()
            .chain(self.signals.drain(0..))
            .collect_vec();

        let mut outgoing = Vec::default();
        for received in received.into_iter() {
            debug!("I {:?}", &received);
            let transition = self.state.take().unwrap().handle(received);
            let (state, generated) = transition.split();
            trace!(target: "ax2five::states", "{:#?}", &state);
            self.state = Some(state);

            for generated in generated.into_iter() {
                debug!("O {:?}", &generated);
                match generated {
                    Generated::Frame(frame) => {
                        outgoing.push(TickOut::Send(self.make_frame(frame)));
                    }
                    Generated::Primitive(primitive) => match primitive {
                        Out::DataIndication(info) => self.rx_buffer.extend(info.into_inner()),
                        Out::UnitDataIndication(_) => {}
                        Out::ErrorIndication(_) => {}
                        Out::ConnectIndication => {
                            // outgoing.push(TickOut::Connected);
                        }
                        Out::ConnectConfirm => {
                            outgoing.push(TickOut::Connected);
                        }
                        Out::DisconnectIndication => {
                            // outgoing.push(TickOut::Disconnected);
                        }
                        Out::DisconnectConfirm => {
                            outgoing.push(TickOut::Disconnected);
                        }
                        Out::AcknowledgePending(pending) => {
                            outgoing.push(TickOut::AwaitingAck(pending));
                        }
                    },
                }
            }
        }

        Ok(outgoing)
    }

    fn add(&mut self, received: Received) {
        self.signals.push(received)
    }

    fn write(&mut self, info: Vec<u8>) -> Result<(), Error> {
        self.add(Received::Primitive(In::DataRequest(info.into())));
        self.add(Received::Primitive(In::Flush));

        Ok(())
    }

    fn read(&mut self) -> Result<Vec<u8>, Error> {
        Ok(self.rx_buffer.drain(0..).collect())
    }

    fn stop(&mut self) -> Result<(), Error> {
        self.add(Received::Primitive(In::DisconnectRequest));

        Ok(())
    }

    fn close(&mut self) -> Result<(), Error> {
        self.add(Received::Primitive(In::DisconnectRequest));

        Ok(())
    }

    fn pair(&self) -> Pair {
        Pair::new(self.source.clone(), self.destination.clone())
    }

    fn make_frame(&self, frame: Frame) -> ax25::frame::Ax25Frame {
        AddressedFrame::new(self.pair(), frame).into()
    }

    fn is_connected(&self) -> bool {
        self.state().is_connected()
    }

    fn is_busy(&self) -> bool {
        self.state().is_busy()
    }

    fn any_pending_signals(&self) -> bool {
        !self.signals.is_empty()
    }

    fn any_pending_data(&self) -> bool {
        !self.rx_buffer.is_empty()
    }

    fn timer_expiration(&self) -> Option<TimerValue> {
        self.state().timer_expiration()
    }

    fn state(&self) -> &State {
        self.state.as_ref().unwrap()
    }
}

#[derive(Error, Debug)]
pub enum Error {
    #[error("socket not open")]
    SocketNotOpen,
    #[error("no link for frame")]
    NoLinkForFrame,
}

#[derive(Debug, Clone)]
pub enum SchedulerIn {
    Received(AddressedFrame),
    Acknowledge,
}

#[derive(Debug, Clone)]
pub enum TickOut {
    Send(Ax25Frame),
    AwaitingAck(bool),
    Data,
    Connected,
    Disconnected,
}

impl TickOut {
    fn for_socket(self, socket: Socket) -> SocketEvent {
        SocketEvent::new(socket, self)
    }
}

#[derive(Debug, Clone)]
pub struct SocketEvent(Socket, TickOut);

impl SocketEvent {
    pub fn new(socket: Socket, event: TickOut) -> Self {
        Self(socket, event)
    }

    pub fn socket(&self) -> Socket {
        self.0
    }

    pub fn event(self) -> TickOut {
        self.1
    }
}

#[derive(Debug, Default)]
pub(crate) struct Clock {
    started: Option<DateTime<Utc>>,
    now: Option<DateTime<Utc>>,
}

impl Clock {
    pub fn tick(&mut self) -> std::time::Duration {
        let now = Utc::now();
        let elapsed = match self.now {
            Some(previous) => now - previous,
            None => {
                self.started = Some(now);
                Default::default()
            }
        };
        self.now = Some(now);
        elapsed.to_std().expect("clock tick overflow")
    }

    fn uptime(&self) -> Uptime {
        Uptime::new(self.started, self.now)
    }
}

#[derive(Debug, Clone)]
struct Incoming(Callsign, Callsign);

#[derive(Default)]
pub struct Scheduler {
    counter: u64,
    parameters: Parameters,
    links: HashMap<Socket, DataLink>,
    listener: HashMap<Callsign, Vec<Incoming>>,
    clock: Clock,
}

#[derive(Debug, PartialEq, Eq, Hash, Copy, Clone)]
pub struct Socket(u64);

impl Scheduler {
    pub fn new(parameters: Parameters) -> Self {
        Self {
            parameters,
            ..Default::default()
        }
    }

    pub fn connect(
        &mut self,
        source: impl Into<Callsign>,
        destination: impl Into<Callsign>,
    ) -> Socket {
        self.add_data_link(DataLink::new(
            source.into(),
            destination.into(),
            self.parameters.clone(),
            vec![Received::Primitive(In::ConnectRequest)],
        ))
    }

    pub fn listen(&mut self, callsign: Callsign) -> Result<(), Error> {
        self.listener.entry(callsign.clone()).or_insert_with(|| {
            debug!("listening on {:?}", callsign);
            Vec::default()
        });

        Ok(())
    }

    pub fn accept(&mut self, callsign: Callsign) -> Option<Socket> {
        match self.listener.get_mut(&callsign) {
            Some(incoming) => match incoming.pop() {
                Some(incoming) => Some(self.add(incoming.0, incoming.1)),
                None => None,
            },
            None => None,
        }
    }

    pub fn add(&mut self, source: impl Into<Callsign>, destination: impl Into<Callsign>) -> Socket {
        self.add_data_link(DataLink::new(
            source.into(),
            destination.into(),
            self.parameters.clone(),
            vec![],
        ))
    }

    pub fn tick(&mut self, elapsed: TimerValue) -> Result<Vec<SocketEvent>, Error> {
        let mut out = Vec::new();

        for (socket, link) in self.links.iter_mut() {
            let _span = span!(
                Level::INFO,
                "L",
                s = link.source.to_string(),
                d = link.destination.to_string(),
            )
            .entered();

            out.extend(
                link.tick(elapsed)?
                    .into_iter()
                    .map(|to| to.for_socket(*socket)),
            );
        }

        Ok(out)
    }

    pub fn loopback(&mut self, elapsed: TimerValue) -> Result<(), Error> {
        let mut counter = 0;

        loop {
            let _span = span!(Level::INFO, "loopback", c = counter).entered();

            let ticked = self.tick(if counter == 0 { elapsed } else { 0.into() })?;
            if ticked.is_empty() && !self.any_pending_signals() {
                break;
            }
            for out in ticked.into_iter() {
                match out.1 {
                    TickOut::Send(frame) => self.received(SchedulerIn::Received(frame.into()))?,
                    TickOut::AwaitingAck(_) => warn!("{:?}", out),
                    TickOut::Data => {}
                    TickOut::Connected => {}
                    TickOut::Disconnected => {}
                }
            }

            counter += 1;
        }

        Ok(())
    }

    pub fn received(&mut self, received: SchedulerIn) -> Result<(), Error> {
        match received {
            SchedulerIn::Received(ref frame) => {
                for link in self.links.values_mut() {
                    if &link.source == frame.destination() {
                        link.add(Received::Frame(frame.clone().into()));
                        return Ok(());
                    }
                }

                Err(Error::NoLinkForFrame)
            }
            SchedulerIn::Acknowledge => {
                for link in self.links.values_mut() {
                    link.add(Received::Primitive(In::Acknowledge));
                }

                Ok(())
            }
        }
    }

    pub fn write(&mut self, socket: Socket, info: Vec<u8>) -> Result<(), Error> {
        self.get_link_mut(socket)?.write(info)
    }

    pub fn read(&mut self, socket: Socket) -> Result<Vec<u8>, Error> {
        self.get_link_mut(socket)?.read()
    }

    pub fn close(&mut self, socket: Socket) -> Result<(), Error> {
        self.get_link_mut(socket)?.close()
    }

    pub fn is_connected(&self, socket: Socket) -> Result<bool, Error> {
        Ok(self.get_link(socket)?.is_connected())
    }

    pub fn busy(&self) -> bool {
        self.links.values().any(|l| l.is_busy())
    }

    pub fn stop(&mut self) -> Result<(), Error> {
        for link in self.links.values_mut() {
            link.stop()?;
        }

        Ok(())
    }

    pub fn pending_data(&self) -> Vec<Socket> {
        self.links
            .iter()
            .flat_map(|(socket, link)| {
                if link.any_pending_data() {
                    Some(*socket)
                } else {
                    None
                }
            })
            .collect_vec()
    }

    pub(crate) fn update_clock(&mut self) -> std::time::Duration {
        self.clock.tick()
    }

    fn allocate_socket(&mut self) -> Socket {
        let socket = Socket(self.counter);
        self.counter += 1;
        socket
    }

    fn add_data_link(&mut self, link: DataLink) -> Socket {
        let socket = self.allocate_socket();
        self.links.insert(socket, link);
        socket
    }

    fn get_link_mut(&mut self, socket: Socket) -> Result<&mut DataLink, Error> {
        match self.links.get_mut(&socket) {
            Some(link) => Ok(link),
            None => Err(Error::SocketNotOpen),
        }
    }

    fn get_link(&self, socket: Socket) -> Result<&DataLink, Error> {
        match self.links.get(&socket) {
            Some(link) => Ok(link),
            None => Err(Error::SocketNotOpen),
        }
    }

    fn any_pending_signals(&self) -> bool {
        self.links.values().any(|l| l.any_pending_signals())
    }

    pub(crate) fn uptime(&self) -> Uptime {
        self.clock.uptime()
    }
}

impl TimerExpiration for Scheduler {
    fn timer_expiration(&self) -> Option<std::time::Duration> {
        self.links
            .values()
            .filter_map(|l| l.timer_expiration())
            .sorted()
            .map(|t| t.into())
            .next()
    }
}

pub(crate) struct Uptime {
    started: Option<DateTime<Utc>>,
    now: Option<DateTime<Utc>>,
}

impl Uptime {
    fn new(started: Option<DateTime<Utc>>, now: Option<DateTime<Utc>>) -> Self {
        Self { started, now }
    }
}

impl std::fmt::Debug for Uptime {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match (self.started, self.now) {
            (Some(started), Some(now)) => {
                let ms = (now - started).num_milliseconds();
                f.write_fmt(format_args!("{:06?}", ms))
            }
            (None, None) => f.write_fmt(format_args!("{:06}", 0)),
            (Some(_), None) => f.write_str("error"),
            (None, Some(_)) => f.write_str("error"),
        }
    }
}
