use convert_case::{Case, Casing};
use proc_macro::TokenStream;
use quote::format_ident;
use quote::quote;
use syn::{parse_macro_input, DeriveInput};

#[proc_macro_attribute]
pub fn ax25_spec(_attr: TokenStream, item: TokenStream) -> TokenStream {
    item
}

#[proc_macro_derive(SpecCase)]
pub fn spec_case_test(input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as DeriveInput);
    let name = input.ident;
    let test_name = format_ident!("test_{}", name.to_string().to_case(Case::Snake));

    let done = quote! {
        #[test]
        #[traced_test]
        fn #test_name() {
            let _span = tracing::span!(Level::INFO, stringify!(#name));
            let case = #name::default();
            let target = case.before();
            let inner_before = target.inner().clone();
            debug!("handling");
            let transition = target.handle(case.signal());
            let (after, generated) = transition.split();
            debug!("after");
            self::assert_eq!(after, case.after(inner_before));
            self::assert_eq!(generated, case.generated());
        }
    };

    done.into()
}
