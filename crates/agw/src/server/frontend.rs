use std::{collections::HashMap, marker::PhantomData};
use thiserror::Error;
use tracing::*;

use ax2five::{Adapter, Callsign, Parameters, Scheduler, Service, Socket, TickOut, Transport};

use crate::{Packet, PacketKind};

#[derive(Debug, Copy, Clone, Hash, PartialEq, Eq)]
pub struct ClientId(u64);

pub struct AgwServer<TValue, T, A> {
    transport: T,
    adapter: A,
    message_type: PhantomData<TValue>,
    client_sequence: u64,
    scheduler: Scheduler,
    sockets: HashMap<Callsign, Socket>,
}

impl<TValue, T, A> AgwServer<TValue, T, A> {
    pub fn new(parameters: Parameters, transport: T, adapter: A) -> Self {
        Self {
            transport,
            adapter,
            client_sequence: 0,
            message_type: Default::default(),
            scheduler: Scheduler::new(parameters),
            sockets: Default::default(),
        }
    }

    pub fn add_client(&mut self) -> ClientId {
        let id = ClientId(self.client_sequence);
        self.client_sequence += 1;
        id
    }

    pub fn remove_client(&mut self, _id: ClientId) {}
}

impl<TValue, TPort, TAdapt> AgwServer<TValue, TPort, TAdapt>
where
    TPort: Transport<TValue>,
    TAdapt: Adapter<TValue>,
    TPort::Error: std::error::Error + Send + Sync + 'static,
{
    pub async fn service(&mut self) -> Result<Vec<Packet>, Error> {
        let events = self
            .scheduler
            .service(&mut self.transport, &mut self.adapter)
            .await
            .map_err(|_| Error::Tick)?;

        for event in events.into_iter() {
            info!("{:?}", event);
            match event.event() {
                TickOut::Send(_) => {}
                TickOut::AwaitingAck(_) => {}
                TickOut::Data => {}
                TickOut::Connected => {}
                TickOut::Disconnected => {}
            }
        }

        Ok(vec![])
    }
}

impl<TValue, T, A> AgwServer<TValue, T, A> {
    fn handle(&mut self, packet: Packet) -> Result<(), Error> {
        match packet.kind() {
            PacketKind::Login => todo!(),
            PacketKind::Version => todo!(),
            PacketKind::RegisterCallsign => todo!(),
            PacketKind::UnregisterCallsign => todo!(),
            PacketKind::PortInformation => todo!(),
            PacketKind::PortCapabilities => todo!(),
            PacketKind::CallsignHeardOnPort => todo!(),
            PacketKind::FramesOutstandingOnPort => todo!(),
            PacketKind::FramesOutstandingOnConnection => todo!(),
            PacketKind::Connection => {
                let remote = packet.to().ok_or(Error::CallsignRequired)?;
                if !self.sockets.contains_key(remote) {
                    let socket = self.scheduler.connect(
                        packet.from().ok_or(Error::CallsignRequired)?.clone(),
                        remote.clone(),
                    );

                    self.sockets.insert(remote.clone(), socket);
                }

                Ok(())
            }
            PacketKind::Disconnection => {
                if let Some(socket) = self.get_socket(&packet)? {
                    self.scheduler.close(*socket)?;
                } else {
                    warn!("no connection to {:?}", &packet.to());
                }

                Ok(())
            }
            PacketKind::Data => {
                if let Some(socket) = self.get_socket(&packet)? {
                    self.scheduler
                        .write(*socket, packet.data().bytes().into())?;
                } else {
                    warn!("no connection to {:?}", &packet.to());
                }

                Ok(())
            }
            PacketKind::DataRaw => todo!(),
            PacketKind::UnprotoData => todo!(),
            PacketKind::UnprotoDataVia => todo!(),
            PacketKind::MonitoredFrameInformation => todo!(),
            PacketKind::MonitoredRawInformation => todo!(),
            PacketKind::MonitoredConnectedInformation => todo!(),
            PacketKind::MonitoredSupervisoryInformation => todo!(),
            PacketKind::MonitoredUnprotoInformation => todo!(),
            PacketKind::MonitoredOwnInformation => todo!(),
        }
    }

    fn get_socket(&self, packet: &Packet) -> Result<Option<&Socket>, Error> {
        let remote: Callsign = packet.to().ok_or(Error::CallsignRequired)?.to_owned();
        Ok(self.sockets.get(&remote))
    }
}

#[derive(Debug, Error)]
pub enum Error {
    #[error("callsign required")]
    CallsignRequired,
    #[error("io error")]
    Io(#[from] std::io::Error),
    #[error("ax25 error")]
    Ax25(#[from] ax2five::Error),
    #[error("ax25 tick error")]
    Tick,
}
