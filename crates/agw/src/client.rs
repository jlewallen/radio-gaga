#![allow(dead_code)]

use ax2five::Callsign;
use futures_util::{SinkExt, StreamExt};
use std::{collections::HashMap, sync::Arc, time::Duration};
use thiserror::Error;
use tokio::{
    net::TcpStream,
    sync::{
        mpsc::{Receiver, Sender},
        Mutex,
    },
    task::JoinHandle,
};
use tokio_util::codec::Framed;
use tracing::*;

use crate::{AgwCodec, Packet, PacketKind};

#[derive(Debug, Clone, Hash, PartialEq, Eq)]
struct SessionKey {
    port: u32,
    from: Callsign,
    to: Callsign,
}

impl SessionKey {
    fn new_received(value: &Packet) -> Option<Self> {
        match (value.from(), value.to()) {
            (Some(from), Some(to)) => Some(Self {
                port: value.port(),
                from: to.clone(),
                to: from.clone(),
            }),
            _ => None,
        }
    }

    fn new_sending(value: &Packet) -> Option<Self> {
        match (value.from(), value.to()) {
            (Some(from), Some(to)) => Some(Self {
                port: value.port(),
                from: from.clone(),
                to: to.clone(),
            }),
            _ => None,
        }
    }
}

#[derive(Default, Clone)]
struct SessionMap {
    sessions: Arc<Mutex<HashMap<SessionKey, Sender<Packet>>>>,
}

impl SessionMap {
    async fn insert(&self, key: SessionKey, sender: Sender<Packet>) {
        let mut sessions = self.sessions.lock().await;
        sessions.insert(key, sender);
    }

    async fn forward(&self, packet: Packet) -> Result<bool, Error> {
        let sessions = self.sessions.lock().await;
        if let Some(key) = SessionKey::new_received(&packet) {
            if let Some(forward) = sessions.get(&key) {
                forward.send(packet).await?;

                Ok(true)
            } else {
                Ok(false)
            }
        } else {
            Ok(false)
        }
    }
}

pub struct AgwClient {
    sessions: SessionMap,
    outgoing: Sender<Option<Packet>>,
    servicer: JoinHandle<()>,
}

impl AgwClient {
    pub async fn new(addr: &str) -> Result<Self, Error> {
        let sessions = SessionMap::default();
        let tcp = TcpStream::connect(addr).await?;
        let tcp = Framed::new(tcp, AgwCodec::new());
        let (outgoing, sending) = tokio::sync::mpsc::channel(100);

        let servicer = tokio::spawn({
            let mut servicer = Servicer {
                sessions: sessions.clone(),
                sending,
                tcp,
            };

            async move {
                loop {
                    match servicer.service().await {
                        Ok(done) => {
                            if done {
                                break;
                            }
                        }
                        Err(e) => warn!("{:?}", e),
                    }
                }
            }
        });

        Ok(Self {
            outgoing,
            sessions,
            servicer,
        })
    }

    pub async fn listen(&mut self, port: u32, call: impl Into<Callsign>) -> Result<(), Error> {
        self.send(Packet::new_register_callsign(port, call)).await
    }

    pub async fn start(
        &mut self,
        port: u32,
        from: impl Into<Callsign>,
        to: impl Into<Callsign>,
    ) -> Result<AgwClientSession, Error> {
        let from = from.into();
        let to = to.into();
        let session = self.new_session(port, from.clone(), to.clone()).await;

        self.send(Packet::new_connection(port, from, to)).await?;

        Ok(session)
    }

    pub async fn close(self) -> Result<(), Error> {
        self.outgoing.send(None).await?;

        match self.servicer.await {
            Ok(_) => {}
            Err(e) => warn!("servicer: {:?}", e),
        }

        Ok(())
    }

    async fn send(&mut self, packet: Packet) -> Result<(), Error> {
        Ok(self.outgoing.send(Some(packet)).await?)
    }

    async fn new_session(
        &mut self,
        port: u32,
        from: impl Into<Callsign>,
        to: impl Into<Callsign>,
    ) -> AgwClientSession {
        let (tx, rx) = tokio::sync::mpsc::channel(20);

        let from = from.into();
        let to = to.into();

        let key = SessionKey {
            port,
            to: to.clone(),
            from: from.clone(),
        };

        self.sessions.insert(key, tx).await;

        AgwClientSession {
            port,
            from,
            to,
            tx: self.outgoing.clone(),
            rx,
            buffer: Vec::new(),
        }
    }
}

struct Servicer {
    tcp: Framed<TcpStream, AgwCodec>,
    sending: Receiver<Option<Packet>>,
    sessions: SessionMap,
}

impl Servicer {
    async fn service(&mut self) -> Result<bool, Error> {
        tokio::select! {
            send = self.sending.recv() => {
                match send {
                    Some(Some(send)) => {
                        self.tcp.send(send).await?;

                        Ok(false)
                    },
                    Some(None) => {
                        self.tcp.close().await?;

                        Ok(true)
                    }
                    None => Ok(false),
                }
            }
            recv = self.tcp.next() => {
                match recv {
                    Some(packet) => match packet {
                        Ok(packet) => {
                            if !self.sessions.forward(packet).await? {
                                debug!("unforwarded");
                                // self.received.push(packet);
                            }

                            Ok(false)
                        }
                        Err(e) => {
                            warn!("{:?}", e);

                            Ok(false)
                        }
                    },
                    None => Ok(false),
                }
            }
        }
    }
}

pub struct AgwClientSession {
    port: u32,
    from: Callsign,
    to: Callsign,
    tx: Sender<Option<Packet>>,
    rx: Receiver<Packet>,
    buffer: Vec<u8>,
}

impl AgwClientSession {
    fn key(&self) -> SessionKey {
        SessionKey {
            port: self.port,
            from: self.from.clone(),
            to: self.to.clone(),
        }
    }

    pub async fn stop(self) -> Result<(), Error> {
        Ok(self
            .tx
            .send(Some(Packet::new_disconnection(
                self.port,
                self.from.clone(),
                self.to.clone(),
            )))
            .await?)
    }

    pub async fn connected(&mut self, to: Duration) -> Result<bool, Error> {
        Ok(matches!(
            self.service(ConnectionStatus::default(), to).await?,
            Exited::Early(_)
        ))
    }

    pub async fn service<A>(&mut self, accumulator: A, to: Duration) -> Result<Exited<A>, Error>
    where
        A: Accumulator,
    {
        let accumulator = Arc::new(Mutex::new(accumulator));

        let task = {
            let accumulator = Arc::clone(&accumulator);

            async move {
                loop {
                    if let Some(packet) = self.rx.recv().await {
                        if let Some(received) = SessionKey::new_received(&packet) {
                            if received == self.key() {
                                match packet.kind() {
                                    PacketKind::Data => {
                                        self.buffer.extend(packet.data().bytes());
                                    }
                                    PacketKind::Disconnection => {
                                        debug!("disconnected!");
                                    }
                                    _ => {}
                                }

                                let mut accumulator = accumulator.lock().await;
                                match accumulator.accumulate(packet)? {
                                    Some(_) => return Ok(()),
                                    None => {}
                                }
                            }
                        }
                    }
                }
            }
        };

        let res = tokio::time::timeout(to, task).await;

        let accumulator = Arc::into_inner(accumulator).unwrap();

        let accumulator = accumulator.into_inner();

        match res {
            Ok(Ok(_)) => Ok(Exited::Early(accumulator)),
            Ok(Err(e)) => Err(e),
            Err(_) => Ok(Exited::Elapsed(accumulator)),
        }
    }
}

#[derive(Debug)]
pub enum Exited<A> {
    Early(A),
    Elapsed(A),
}

pub trait Accumulator {
    type Output;

    fn accumulate(&mut self, packet: Packet) -> Result<Option<Self::Output>, Error>;
}

#[derive(Debug, Default)]
pub struct ConnectionStatus {
    connected: bool,
}

impl Into<bool> for ConnectionStatus {
    fn into(self) -> bool {
        self.connected
    }
}

impl Accumulator for ConnectionStatus {
    type Output = bool;

    fn accumulate(&mut self, packet: Packet) -> Result<Option<Self::Output>, Error> {
        match packet.kind() {
            PacketKind::Connection => {
                info!("connected!");
                self.connected = true;

                Ok(Some(self.connected))
            }
            PacketKind::Disconnection => Ok(Some(self.connected)),
            _ => Ok(None),
        }
    }
}

#[derive(Debug, Default)]
pub struct ReceivedData {
    text: String,
}

impl Into<String> for ReceivedData {
    fn into(self) -> String {
        self.text
    }
}

impl Accumulator for ReceivedData {
    type Output = String;

    fn accumulate(&mut self, packet: Packet) -> Result<Option<Self::Output>, Error> {
        match packet.kind() {
            PacketKind::Data => match packet.data().as_string() {
                Some(data) => {
                    self.text.push_str(&data);

                    Ok(None)
                }
                None => Ok(None),
            },
            _ => Ok(None),
        }
    }
}

#[derive(Debug, Error)]
pub enum Error {
    #[error("IO: {0}")]
    Io(#[from] std::io::Error),
    #[error("Send")]
    Send,
}

impl<T> From<tokio::sync::mpsc::error::SendError<T>> for Error {
    fn from(_value: tokio::sync::mpsc::error::SendError<T>) -> Self {
        Self::Send
    }
}
