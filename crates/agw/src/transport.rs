use tokio::net::TcpStream;
use tokio_util::codec::Framed;
use tracing::*;

use ax2five::{Ax25Frame, Pretty, Transport};

use crate::{AgwCodec, AgwError, AgwProtocol, Packet};

impl Transport<Ax25Frame> for AgwProtocol<Framed<TcpStream, AgwCodec>> {
    type Error = AgwError;

    async fn send(&mut self, frame: Ax25Frame) -> Result<(), AgwError> {
        let data = frame.to_bytes();
        let packet = Packet::new_raw_with_leading_null(data);
        trace!("tx {:?}", Pretty::new(&frame));
        self.send_packet(packet).await.map_err(AgwError::Io)
    }

    async fn receive(&mut self) -> Result<Option<Ax25Frame>, AgwError> {
        let received = self.receive_packet().await?;

        if let Some(received) = received {
            if let Some(received) = received.to_ax25() {
                let received = received?;
                trace!("rx {:?}", Pretty::new(&received));
                Ok(Some(received))
            } else {
                Ok(None)
            }
        } else {
            Err(std::io::Error::new(std::io::ErrorKind::ConnectionAborted, "Aborted").into())
        }
    }
}
