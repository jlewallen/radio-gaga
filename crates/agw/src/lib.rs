use std::collections::{HashMap, VecDeque};
use tracing::*;

use ax2five::{
    AcknowledgeAfterT2, AcknowledgePending, Adapter, FrameFilter, SchedulerIn, Socket, SocketEvent,
    TickOut, TimerExpiration,
};

mod client;
mod codec;
mod packets;
mod proto;
mod server;
mod transport;

pub use ax2five::Callsign;
pub use client::{Accumulator, AgwClient, ConnectionStatus, Error, Exited, ReceivedData};
pub use codec::AgwCodec;
pub use packets::{
    DecodeError, Header, Packet, PacketKind, PortCapabilities, Ports, ProtocolIdentifier, Version,
};
pub use proto::{AgwError, AgwProtocol};

pub struct RawAgwAdapter<F> {
    filter: F,
}

impl<F: FrameFilter> RawAgwAdapter<F> {
    pub fn new(filter: F) -> Self {
        Self { filter }
    }
}

impl<F> TimerExpiration for RawAgwAdapter<F> {}

impl<F: FrameFilter> Adapter<Packet> for RawAgwAdapter<F> {
    fn send(&mut self, value: SocketEvent) -> Option<Packet> {
        match value.event() {
            TickOut::Send(frame) => Some(Packet::new_raw_with_leading_null(frame.to_bytes())),
            _ => None,
        }
    }

    fn receive(&mut self, value: Packet) -> Option<SchedulerIn> {
        if let Some(frame) = value.get_ax25().ok().flatten() {
            self.filter
                .handle(frame)
                .map(|frame| SchedulerIn::Received(frame.into()))
        } else {
            None
        }
    }
}

pub struct AckAfterOutgoingQueueEmpty<F> {
    filter: F,
    queue: VecDeque<()>,
    pending: HashMap<Socket, AcknowledgeAfterT2>,
}

impl<F: FrameFilter> AckAfterOutgoingQueueEmpty<F> {
    pub fn new(filter: F) -> Self {
        Self {
            filter,
            queue: Default::default(),
            pending: Default::default(),
        }
    }
}

impl<F> TimerExpiration for AckAfterOutgoingQueueEmpty<F> {}

impl<F: FrameFilter> Adapter<Packet> for AckAfterOutgoingQueueEmpty<F> {
    fn elapsed(&mut self, elapsed: std::time::Duration) -> Vec<SchedulerIn> {
        self.pending
            .values_mut()
            .flat_map(|timer| timer.elapsed(elapsed))
            .collect()
    }

    fn send(&mut self, value: SocketEvent) -> Option<Packet> {
        let socket = value.socket();
        match value.event() {
            TickOut::Send(frame) => {
                self.queue.push_front(());

                debug!("ack-outgoing[{}]:push", self.queue.len());

                Some(Packet::new_raw_with_leading_null(frame.to_bytes()))
            }
            TickOut::AwaitingAck(pending) => {
                if pending {
                    self.pending.insert(socket, AcknowledgeAfterT2::started());
                } else if self.pending.remove_entry(&socket).is_some() {
                    debug!("t2:stopped");
                }
                None
            }
            _ => None,
        }
    }

    fn receive(&mut self, value: Packet) -> Option<SchedulerIn> {
        match value.kind() {
            PacketKind::MonitoredOwnInformation => {
                self.queue.pop_back();

                if self.queue.is_empty() {
                    if self.pending.is_empty() {
                        debug!("ack-outgoing[0]:empty-noop");
                        None
                    } else {
                        self.pending.clear();
                        debug!("ack-outgoing[0]:ack!");
                        Some(SchedulerIn::Acknowledge)
                    }
                } else {
                    debug!("ack-outgoing[{}]:pop", self.queue.len());
                    None
                }
            }
            PacketKind::DataRaw => {
                if let Some(frame) = value.get_ax25().ok().flatten() {
                    if let Some(frame) = self.filter.handle(frame) {
                        for pending in self.pending.values_mut() {
                            pending.queue();
                        }

                        Some(SchedulerIn::Received(frame.into()))
                    } else {
                        None
                    }
                } else {
                    None
                }
            }
            _ => None,
        }
    }
}
