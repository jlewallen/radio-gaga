use bytes::BytesMut;
use tokio_util::codec::{Decoder, Encoder};
use tracing::*;

use crate::{packets::HEADER_SIZE, AgwError, Header, Packet};

#[derive(Default)]
pub struct AgwCodec {}

impl AgwCodec {
    pub fn new() -> Self {
        Self {}
    }
}

impl Decoder for AgwCodec {
    type Item = Packet;

    type Error = AgwError;

    fn decode(&mut self, buf: &mut BytesMut) -> Result<Option<Self::Item>, Self::Error> {
        if buf.len() >= HEADER_SIZE {
            let header = Header::decode(buf)?;

            if buf.len() >= HEADER_SIZE + header.len {
                let data = buf.split_to(HEADER_SIZE + header.len);

                trace!(
                    "decoded {} bytes ({} header, {} data)",
                    HEADER_SIZE + header.len,
                    HEADER_SIZE,
                    header.len
                );

                let data = data[HEADER_SIZE..HEADER_SIZE + header.len].to_owned();

                Ok(Some(Packet::new(header, data)))
            } else {
                Ok(None)
            }
        } else {
            Ok(None)
        }
    }
}

impl Encoder<Packet> for AgwCodec {
    type Error = std::io::Error;

    fn encode(&mut self, item: Packet, buf: &mut BytesMut) -> Result<(), Self::Error> {
        item.encode(buf)
    }
}
