use std::borrow::Cow;

use bytes::{BufMut, BytesMut};
use thiserror::Error;
use tracing::*;

use ax2five::{Ax25Frame, Callsign, FrameParseError, Pretty};

pub const HEADER_SIZE: usize = 36;

#[derive(PartialEq, Clone)]
pub struct Header {
    pub port: u32,
    pub kind: PacketKind,
    pub pid: ProtocolIdentifier,
    pub from: Callsign,
    pub to: Callsign,
    pub len: usize,
}

impl Header {
    pub fn new(kind: PacketKind) -> Self {
        Self {
            kind,
            ..Default::default()
        }
    }

    pub fn encode<W: std::io::Write>(&self, buffer: &mut W) -> Result<(), std::io::Error> {
        let kind: u8 = self.kind.into();
        buffer.write_all(&self.port.to_le_bytes())?;
        buffer.write_all(&[kind, 0, self.pid.to_byte(), 0])?;
        buffer.write_all(&self.from.to_field())?;
        buffer.write_all(&self.to.to_field())?;
        buffer.write_all(&(self.len as u32).to_le_bytes())?;
        buffer.write_all(&0u32.to_le_bytes())?;
        Ok(())
    }

    pub fn decode(buffer: &[u8]) -> Result<Self, DecodeError> {
        let port = buffer[0..4]
            .try_into()
            .map_err(|_| DecodeError::FieldSliceError)?;
        let port = u32::from_le_bytes(port);
        let kind = buffer[4].try_into().map_err(|_| DecodeError::Kind)?;
        let pid = ProtocolIdentifier::from_byte(buffer[6]);
        let from = Callsign::from_bytes(&buffer[8..18]);
        let to = Callsign::from_bytes(&buffer[18..28]);
        let len: [u8; 4] = buffer[28..32]
            .try_into()
            .map_err(|_| DecodeError::FieldSliceError)?;
        let len = u32::from_le_bytes(len) as usize;
        Ok(Self {
            port,
            kind,
            pid,
            from,
            to,
            len,
        })
    }

    fn with_call_to(self, call: impl Into<Callsign>) -> Self {
        Self {
            to: call.into(),
            ..self
        }
    }

    fn with_call_from(self, call: impl Into<Callsign>) -> Self {
        Self {
            from: call.into(),
            ..self
        }
    }

    fn with_data_length(self, len: usize) -> Self {
        Self { len, ..self }
    }
}

impl Default for Header {
    fn default() -> Self {
        Self {
            port: Default::default(),
            kind: PacketKind::Version,
            pid: ProtocolIdentifier::None,
            from: Default::default(),
            to: Default::default(),
            len: Default::default(),
        }
    }
}

#[derive(PartialEq, Clone)]
pub struct Packet {
    header: Header,
    data: Vec<u8>,
}

impl Packet {
    pub fn data(&self) -> AgwData {
        AgwData {
            data: Cow::from(&self.data),
        }
    }

    pub fn port(&self) -> u32 {
        self.header.port
    }

    pub fn pid(&self) -> &ProtocolIdentifier {
        &self.header.pid
    }

    pub fn kind(&self) -> PacketKind {
        self.header.kind
    }

    pub fn as_kind(&self, kind: PacketKind) -> Option<&Self> {
        if self.kind() == kind {
            Some(self)
        } else {
            None
        }
    }

    pub fn as_connection(&self) -> Option<&Self> {
        self.as_kind(PacketKind::Connection)
    }

    pub fn from(&self) -> Option<&Callsign> {
        self.header.from.none_if_empty()
    }

    pub fn to(&self) -> Option<&Callsign> {
        self.header.to.none_if_empty()
    }

    // TODO This is so ugly.
    pub fn with_call_to(self, call: impl Into<Callsign>) -> Self {
        let previous = self.to().unwrap().to_owned();
        let (header, data) = self.split();
        let call = call.into();
        let data = data.replace(previous.as_str(), call.as_str());
        let header = header.with_call_to(call).with_data_length(data.len());
        Self {
            header,
            data: data.to_vec(),
        }
    }

    // TODO This is so ugly.
    pub fn with_call_from(self, call: impl Into<Callsign>) -> Self {
        let previous = self.from().unwrap().to_owned();
        let (header, data) = self.split();
        let call = call.into();
        let data = data.replace(previous.as_str(), call.as_str());
        let header = header.with_call_from(call).with_data_length(data.len());
        Self {
            header,
            data: data.to_vec(),
        }
    }

    fn split<'d>(self) -> (Header, AgwData<'d>) {
        (
            self.header,
            AgwData {
                data: Cow::from(self.data),
            },
        )
    }
}

pub struct AgwData<'d> {
    data: Cow<'d, [u8]>,
}

impl<'d> AgwData<'d> {
    pub fn len(&self) -> usize {
        self.data.len()
    }

    pub fn is_empty(&self) -> bool {
        self.data.is_empty()
    }

    pub fn bytes(&self) -> &[u8] {
        &self.data
    }

    pub fn to_vec(&self) -> Vec<u8> {
        self.data.to_vec()
    }

    pub fn as_string(&self) -> Option<String> {
        // For ax25 frames, Agw servers append a text 'summary' of the frame and
        // then append the data itself, with various null delimiters. Since most
        // data that goes over the wire is text, this isn't usually a huge
        // problem and just needs a little care. Some frames are binary, though,
        // for example Xid.
        if let Some(index) = self.data.iter().position(|&v| v == 0) {
            let (first, _) = self.data.split_at(index);
            if let Some(index) = first.iter().position(|&c| c == b'\r') {
                let (first, _left) = self.data.split_at(index);
                String::from_utf8(first.to_owned()).ok()
            } else {
                None
            }
        } else if self.data.is_empty() {
            None
        } else {
            String::from_utf8(self.data.to_vec()).ok()
        }
    }

    pub fn replace<'m>(self, from: &str, to: &str) -> AgwData<'m>
    where
        'd: 'm,
    {
        match self.as_string() {
            Some(s) => AgwData {
                data: Cow::from(s.replace(from, to).into_bytes()),
            },
            None => AgwData {
                data: self.data.clone(),
            },
        }
    }
}

impl Packet {
    pub fn decode(buffer: &[u8]) -> Result<Self, DecodeError> {
        let header = Header::decode(buffer)?;
        let data_length = header.len;

        // This *would* be ==, although I have a feeling BPQ and friends end up
        // writing larger buffers than nececssary.
        assert!(data_length + HEADER_SIZE <= buffer.len());

        let data = buffer
            .get(HEADER_SIZE..HEADER_SIZE + data_length)
            .map(|t| t.to_owned())
            .ok_or(DecodeError::DataSliceError)?;

        Ok(Self { header, data })
    }

    pub fn encode(&self, buf: &mut BytesMut) -> Result<(), std::io::Error> {
        assert!(self.header.len == self.data().len());

        let mut header = Vec::new();
        self.header.encode(&mut header)?;

        buf.reserve(HEADER_SIZE + self.data().len());
        buf.put(header.as_ref());
        buf.put(self.data().bytes());

        Ok(())
    }
}

impl Packet {
    pub fn get_ax25(&self) -> Result<Option<Ax25Frame>, FrameParseError> {
        match self.kind() {
            PacketKind::DataRaw => Ok(Some(Ax25Frame::from_bytes(&self.data)?)),
            _ => Ok(None),
        }
    }

    pub fn new_raw_with_leading_null(data: Vec<u8>) -> Self {
        let mut prefixed = vec![0u8];
        prefixed.extend(data);
        Self {
            header: Header {
                kind: PacketKind::DataRaw,
                len: prefixed.len(),
                ..Default::default()
            },
            data: prefixed,
        }
    }

    pub fn to_ax25(self) -> Option<Result<ax2five::Ax25Frame, FrameParseError>> {
        if self.kind() != PacketKind::DataRaw {
            return None;
        }

        // According to the from_bytes documentation this is something that the
        // Linux Ax25 stack will do. I have no idea why. What's annoying is that
        // BPQ seems to be sensitive to this, had has a very strange check in
        // L2Code.c of the form:
        //
        // // Check for invalid length (< 22 7Header + 7Addr + 7Addr + CTL
        // if (Buffer->LENGTH < (18 + sizeof(void *)))
        // {
        //   Debugprintf("BPQ32 Bad L2 Msg Port %d Len %d", PORT->PORTNUMBER, Buffer->LENGTH);
        //   ReleaseBuffer(Buffer);
        //   return;
        // }
        //
        // I've got my eye on 'CTL' up there and, well. Here we are. This is
        // merely here to let future developers have a way of knowing this is
        // happening. When our ax25 frame parser sees these, it'll drop them.
        let leading_nulls = self.data().bytes().iter().position(|&c| c != 0);
        if let Some(first_non_null) = leading_nulls {
            if first_non_null != 0 {
                trace!("received packet with {} leading 0x00", first_non_null);
            }
        } else {
            // If we end up here, then the call below will fail with
            // FrameParseError::OnlyNullBytes, just a heads up.
        }

        Some(ax2five::Ax25Frame::from_bytes(self.data().bytes()))
    }
}

impl Packet {
    pub fn new(header: Header, data: impl Into<Vec<u8>>) -> Self {
        Self {
            header,
            data: data.into(),
        }
    }

    pub fn new_empty(
        kind: PacketKind,
        port: u32,
        from: impl Into<Callsign>,
        to: impl Into<Callsign>,
    ) -> Self {
        Self::new(
            Header {
                kind,
                port,
                from: from.into(),
                to: to.into(),
                ..Default::default()
            },
            [],
        )
    }

    pub fn new_register_callsign(port: u32, call: impl Into<Callsign>) -> Self {
        Self::new_empty(PacketKind::RegisterCallsign, port, call, "")
    }

    pub fn new_connection(port: u32, from: impl Into<Callsign>, to: impl Into<Callsign>) -> Self {
        Self::new_empty(PacketKind::Connection, port, from, to)
    }

    pub fn new_disconnection(
        port: u32,
        from: impl Into<Callsign>,
        to: impl Into<Callsign>,
    ) -> Self {
        Self::new_empty(PacketKind::Disconnection, port, from, to)
    }

    pub fn new_data(
        port: u32,
        from: impl Into<Callsign>,
        to: impl Into<Callsign>,
        data: &[u8],
    ) -> Self {
        Self::new(
            Header {
                kind: PacketKind::Data,
                from: from.into(),
                to: to.into(),
                pid: ProtocolIdentifier::None,
                port,
                ..Default::default()
            },
            data,
        )
    }

    pub fn to_application(self) -> Packet {
        match self.kind() {
            PacketKind::DataRaw => Packet::new_raw(
                self.header.from.as_str(),
                self.header.to.as_str(),
                self.data,
            ),
            _ => todo!(),
        }
    }

    pub fn new_raw(
        from: impl Into<Callsign>,
        to: impl Into<Callsign>,
        data: impl Into<Vec<u8>>,
    ) -> Self {
        Self::new(
            Header {
                kind: PacketKind::DataRaw,
                from: from.into(),
                to: to.into(),
                ..Default::default()
            },
            data,
        )
    }

    pub fn new_application_data(
        from: impl Into<Callsign>,
        to: impl Into<Callsign>,
        data: &[u8],
    ) -> Self {
        Self::new(
            Header {
                kind: PacketKind::Data,
                from: from.into(),
                to: to.into(),
                pid: ProtocolIdentifier::None,
                ..Default::default()
            },
            data,
        )
    }

    pub fn new_ports() -> Self {
        let ports = Ports {
            ports: vec![PortInformation {
                number: 0,
                name: "Port 1".to_owned(),
            }],
        };
        let data = ports.encode();
        Self::new(
            Header {
                kind: PacketKind::PortInformation,
                ..Default::default()
            },
            data,
        )
    }

    pub fn new_arbitrary_own_information(packet: &Packet) -> Option<Self> {
        if let Some(frame) = packet.get_ax25().ok().flatten() {
            let source = format!("{}", frame.source);
            let destination = format!("{}", frame.destination);
            Some(Self::new(
                Header {
                    kind: PacketKind::MonitoredOwnInformation,
                    from: source.into(),
                    to: destination.into(),
                    ..Default::default()
                },
                [],
            ))
        } else {
            None
        }
    }
}

impl std::fmt::Debug for Packet {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        use itertools::*;

        let callsigns = [self.from(), self.to()].into_iter().flatten().join(":");
        let data = match self.get_ax25().ok().flatten() {
            Some(ax25) => format!("{:?})", Pretty::new(&ax25)),
            None => {
                if let Some(data) = self.data().as_string() {
                    format!("`{}`", data.replace("\r", "<10>").replace("\n", "<13>"))
                } else {
                    "".to_owned()
                }
            }
        };

        if callsigns.is_empty() {
            f.write_fmt(format_args!(
                "P{} Pid={:?} {:?} {}",
                self.port(),
                self.pid(),
                self.kind(),
                data,
            ))
        } else {
            f.write_fmt(format_args!(
                "P{} Pid={:?} {} {:?} {}",
                self.port(),
                self.pid(),
                callsigns,
                self.kind(),
                data,
            ))
        }
    }
}

#[derive(Debug, PartialEq)]
pub struct Ports {
    pub ports: Vec<PortInformation>,
}

impl Ports {
    pub fn encode(&self) -> Vec<u8> {
        let names: String = self
            .ports
            .iter()
            .map(|p| p.name.as_str())
            .collect::<Vec<&str>>()
            .join(";");
        let buffer = format!("{};{}", self.ports.len(), names);
        buffer.as_bytes().to_vec()
    }
}

#[derive(Debug, PartialEq)]
pub struct PortInformation {
    pub number: usize,
    pub name: String,
}

impl TryInto<Ports> for Packet {
    type Error = DecodeError;

    fn try_into(self) -> Result<Ports, Self::Error> {
        let payload = String::from_utf8(self.data).map_err(|_| DecodeError::Utf8Error)?;

        let ports = payload
            .split(';')
            .skip(1)
            .map(|name| name.to_owned())
            .enumerate()
            .map(|(number, name)| PortInformation { number, name })
            .collect();

        Ok(Ports { ports })
    }
}

#[derive(Debug, PartialEq)]
pub struct PortCapabilities {
    pub air_baud: u32,
    pub traffic_level: u8,
    pub tx_delay: u8,
    pub tx_tail: u8,
    pub persist: u8,
    pub slot_time: u8,
    pub max_frame: u8,
    pub active: u8,
    pub received: u32,
}

impl PortCapabilities {
    pub fn decode(buffer: &[u8]) -> Result<Self, DecodeError> {
        let air_baud = buffer[0];
        let traffic_level = buffer[1];
        let tx_delay = buffer[2];
        let tx_tail = buffer[3];
        let persist = buffer[4];
        let slot_time = buffer[5];
        let max_frame = buffer[6];
        let active = buffer[7];
        let received = buffer[8..12]
            .try_into()
            .map_err(|_| DecodeError::FieldSliceError)?;
        let received = u32::from_le_bytes(received);
        Ok(Self {
            air_baud: (air_baud as u32 + 1) * 1200,
            traffic_level,
            tx_delay,
            tx_tail,
            persist,
            slot_time,
            max_frame,
            active,
            received,
        })
    }
}

impl TryFrom<Packet> for PortCapabilities {
    type Error = DecodeError;

    fn try_from(value: Packet) -> Result<Self, Self::Error> {
        PortCapabilities::decode(&value.data)
    }
}

#[derive(Debug, PartialEq)]
pub struct Version {
    pub minor: u32,
    pub major: u32,
}

impl Version {
    pub fn decode(buffer: &[u8]) -> Result<Self, DecodeError> {
        let minor: [u8; 4] = buffer[0..4]
            .try_into()
            .map_err(|_| DecodeError::FieldSliceError)?;
        let minor = u32::from_le_bytes(minor);
        let major: [u8; 4] = buffer[4..8]
            .try_into()
            .map_err(|_| DecodeError::FieldSliceError)?;
        let major = u32::from_le_bytes(major);

        Ok(Self { minor, major })
    }
}

impl TryFrom<Packet> for Version {
    type Error = DecodeError;

    fn try_from(value: Packet) -> Result<Self, Self::Error> {
        Version::decode(&value.data)
    }
}

#[derive(Debug, Error)]
pub enum DecodeError {
    #[error("field slice error")]
    FieldSliceError,
    #[error("data slice error")]
    DataSliceError,
    #[error("utf8 error")]
    Utf8Error,
    #[error("invalid packet kind")]
    Kind,
}

#[derive(Debug, PartialEq, Copy, Clone)]
pub enum PacketKind {
    Login,
    Version,
    RegisterCallsign,
    UnregisterCallsign,
    PortInformation,
    PortCapabilities,
    CallsignHeardOnPort,
    FramesOutstandingOnPort,
    FramesOutstandingOnConnection,
    Connection,
    Disconnection,
    Data,
    DataRaw,
    UnprotoData,
    UnprotoDataVia,
    MonitoredFrameInformation,
    MonitoredRawInformation,
    MonitoredConnectedInformation,
    MonitoredSupervisoryInformation,
    MonitoredUnprotoInformation,
    MonitoredOwnInformation,
}

impl From<PacketKind> for u8 {
    fn from(val: PacketKind) -> Self {
        match val {
            PacketKind::Login => b'P',
            PacketKind::Version => b'R',
            PacketKind::RegisterCallsign => b'X',
            PacketKind::UnregisterCallsign => b'x',
            PacketKind::PortInformation => b'G',
            PacketKind::PortCapabilities => b'g',
            PacketKind::CallsignHeardOnPort => b'H',
            PacketKind::FramesOutstandingOnPort => b'y',
            PacketKind::FramesOutstandingOnConnection => b'Y',
            PacketKind::Connection => b'C',
            PacketKind::Disconnection => b'd',
            PacketKind::Data => b'D',
            PacketKind::DataRaw => b'K',
            PacketKind::UnprotoData => b'M',
            PacketKind::UnprotoDataVia => b'V',
            PacketKind::MonitoredFrameInformation => b'm',
            PacketKind::MonitoredRawInformation => b'k',
            PacketKind::MonitoredConnectedInformation => b'I',
            PacketKind::MonitoredSupervisoryInformation => b'S',
            PacketKind::MonitoredUnprotoInformation => b'U',
            PacketKind::MonitoredOwnInformation => b'T',
        }
    }
}

impl TryFrom<u8> for PacketKind {
    type Error = InvalidPacketKind;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            b'P' => Ok(PacketKind::Login),
            b'R' => Ok(PacketKind::Version),
            b'X' => Ok(PacketKind::RegisterCallsign),
            b'x' => Ok(PacketKind::UnregisterCallsign),
            b'G' => Ok(PacketKind::PortInformation),
            b'g' => Ok(PacketKind::PortCapabilities),
            b'H' => Ok(PacketKind::CallsignHeardOnPort),
            b'y' => Ok(PacketKind::FramesOutstandingOnPort),
            b'Y' => Ok(PacketKind::FramesOutstandingOnConnection),
            b'C' => Ok(PacketKind::Connection),
            b'd' => Ok(PacketKind::Disconnection),
            b'D' => Ok(PacketKind::Data),
            b'K' => Ok(PacketKind::DataRaw),
            b'M' => Ok(PacketKind::UnprotoData),
            b'V' => Ok(PacketKind::UnprotoDataVia),
            b'm' => Ok(PacketKind::MonitoredFrameInformation),
            b'k' => Ok(PacketKind::MonitoredRawInformation),
            b'I' => Ok(PacketKind::MonitoredConnectedInformation),
            b'S' => Ok(PacketKind::MonitoredSupervisoryInformation),
            b'U' => Ok(PacketKind::MonitoredUnprotoInformation),
            b'T' => Ok(PacketKind::MonitoredOwnInformation),
            _ => Err(InvalidPacketKind { value }),
        }
    }
}

#[derive(Debug, Error)]
#[error("invalid packet kind")]
pub struct InvalidPacketKind {
    value: u8,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum ProtocolIdentifier {
    None,
    NetRom,
    Zero,
    Unknown(u8),
}

impl ProtocolIdentifier {
    pub fn to_byte(&self) -> u8 {
        match self {
            ProtocolIdentifier::Zero => 0x00,
            ProtocolIdentifier::None => 0xF0,
            ProtocolIdentifier::NetRom => 0xCF,
            ProtocolIdentifier::Unknown(value) => *value,
        }
    }

    pub fn from_byte(value: u8) -> Self {
        match value {
            0x00 => ProtocolIdentifier::Zero,
            0xF0 => ProtocolIdentifier::None,
            0xCF => ProtocolIdentifier::NetRom,
            _ => ProtocolIdentifier::Unknown(value),
        }
    }
}
