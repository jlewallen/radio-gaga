use futures_util::{Sink, SinkExt, Stream, StreamExt};
use thiserror::Error;
use tokio::io::{AsyncRead, AsyncWrite};
use tokio_util::codec::Framed;
use tracing::*;

use ax2five::{Callsign, FrameParseError, Transport};

use crate::codec::AgwCodec;
use crate::{DecodeError, Header, Packet, PacketKind};

pub struct AgwProtocol<T> {
    stream: T,
}

impl<T> AgwProtocol<T> {
    pub fn new(stream: T) -> Self {
        Self { stream }
    }
}

impl<S: AsyncRead + AsyncWrite> AgwProtocol<Framed<S, AgwCodec>> {
    pub fn new_framed(stream: S) -> Self {
        Self {
            stream: Framed::new(stream, AgwCodec::new()),
        }
    }
}

impl<T: Stream<Item = Result<Packet, AgwError>> + Unpin> AgwProtocol<T> {
    pub async fn receive_packet(&mut self) -> Result<Option<Packet>, AgwError> {
        match self.stream.next().await {
            Some(packet) => {
                let packet = packet?;
                debug!("rx {:?}", &packet);
                Ok(Some(packet))
            }
            None => Ok(None),
        }
    }
}

impl<T: Sink<Packet> + Unpin> AgwProtocol<T> {
    pub async fn send_packet(&mut self, packet: Packet) -> Result<(), T::Error> {
        debug!("tx {:?}", &packet);
        self.stream.send(packet).await
    }

    pub async fn login(&mut self) -> Result<(), T::Error> {
        let header = Header {
            kind: PacketKind::Login,
            ..Default::default()
        };

        self.send_header(header).await
    }

    pub async fn ask_version(&mut self) -> Result<(), T::Error> {
        let version = Header {
            kind: PacketKind::Version,
            ..Default::default()
        };
        self.send_header(version).await
    }

    pub async fn ask_ports(&mut self) -> Result<(), T::Error> {
        let version = Header {
            kind: PacketKind::PortInformation,
            ..Default::default()
        };
        self.send_header(version).await
    }

    pub async fn register_callsign(
        &mut self,
        callsign: impl Into<Callsign>,
    ) -> Result<(), T::Error> {
        let version = Header {
            kind: PacketKind::RegisterCallsign,
            from: callsign.into(),
            ..Default::default()
        };
        self.send_header(version).await
    }

    pub async fn ask_heard_on_port(&mut self, port: u32) -> Result<(), T::Error> {
        let sending = Header {
            port,
            kind: PacketKind::CallsignHeardOnPort,
            ..Default::default()
        };
        self.send_header(sending).await
    }

    pub async fn ask_port_capabilities(&mut self, port: u32) -> Result<(), T::Error> {
        let sending = Header {
            port,
            kind: PacketKind::PortCapabilities,
            ..Default::default()
        };
        self.send_header(sending).await
    }

    pub async fn enable_monitoring(&mut self) -> Result<(), T::Error> {
        let header = Header {
            kind: PacketKind::MonitoredFrameInformation,
            ..Default::default()
        };

        self.send_header(header).await
    }

    pub async fn enable_raw(&mut self) -> Result<(), T::Error> {
        let header = Header {
            kind: PacketKind::MonitoredRawInformation,
            ..Default::default()
        };

        self.send_header(header).await
    }

    pub async fn connection(
        &mut self,
        source: impl Into<Callsign>,
        destination: impl Into<Callsign>,
    ) -> Result<(), T::Error> {
        let header = Header {
            kind: PacketKind::Connection,
            from: source.into(),
            to: destination.into(),
            ..Default::default()
        };

        self.send_header(header).await
    }

    pub async fn disconnection(
        &mut self,
        source: impl Into<Callsign>,
        destination: impl Into<Callsign>,
    ) -> Result<(), T::Error> {
        let header = Header {
            kind: PacketKind::Disconnection,
            from: source.into(),
            to: destination.into(),
            ..Default::default()
        };

        self.send_header(header).await
    }

    pub async fn data(
        &mut self,
        source: impl Into<Callsign>,
        destination: impl Into<Callsign>,
        data: &[u8],
    ) -> Result<(), T::Error> {
        let header = Header {
            kind: PacketKind::Data,
            from: source.into(),
            to: destination.into(),
            len: data.len(),
            ..Default::default()
        };

        self.send_packet(Packet::new(header, data)).await
    }

    async fn send_header(&mut self, header: Header) -> Result<(), T::Error> {
        self.send_packet(Packet::new(header, Vec::new())).await?;

        Ok(())
    }
}

impl<T: AsyncRead + AsyncWrite + Unpin> Transport<Packet> for AgwProtocol<Framed<T, AgwCodec>> {
    type Error = AgwError;

    async fn send(&mut self, packet: Packet) -> Result<(), Self::Error> {
        self.send_packet(packet).await.map_err(AgwError::Io)
    }

    async fn receive(&mut self) -> Result<Option<Packet>, Self::Error> {
        self.receive_packet().await
    }
}

#[derive(Error, Debug)]
pub enum AgwError {
    #[error("io error")]
    Io(#[from] std::io::Error),
    #[error("decode error")]
    Decode(#[from] DecodeError),
    #[error("frame parse error")]
    FrameParse(#[from] FrameParseError),
}
