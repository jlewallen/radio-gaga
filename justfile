export RUST_LOG := "info"

default: test build

setup:
    cp -n .env.default .env || true

build: setup
    cargo build --workspace

test: setup
    cargo test --workspace

testall: setup
    cargo test --workspace --no-fail-fast

clean:
    rm -rf target

artifacts:
    echo rustup target add aarch64-unknown-linux-gnu
    cargo build -p gaterm --release --target aarch64-unknown-linux-gnu
    cargo build -p ax25-monitor --release --target aarch64-unknown-linux-gnu
    cargo build -p agw-proxy --release --target aarch64-unknown-linux-gnu
    cargo build -p gaterm --release
    cargo build -p ax25-monitor --release
    cargo build -p agw-proxy --release
    cp target/release/gaterm artifacts/gaterm-x86-64
    cp target/release/ax25-monitor artifacts/ax25-monitor-x86-64
    cp target/release/agw-proxy artifacts/agw-proxy-x86-64
    cp target/aarch64-unknown-linux-gnu/release/gaterm artifacts/gaterm-aarch64
    cp target/aarch64-unknown-linux-gnu/release/ax25-monitor artifacts/ax25-monitor-aarch64
    cp target/aarch64-unknown-linux-gnu/release/agw-proxy artifacts/agw-proxy-aarch64