# What is this?

This is very much a rough work in progress. I'm very new to packet radio and one
of the best ways for me to learn is by building projects like this. So, for the
time being this is meant purely as a learning exercise.

My goal is to first work up some kind of text based AGW capable terminal. Then,
I would love to be able to build up a simple BBS sytem and grow that feature
set.

Primarily, I'd just like things to work and then I'll be sweeping through and
cleaning up the code. For example, it would be great if the terminal session
code was generic over the transport, but that's now how I'll be doing things
first. There are so many ways to mix, match and combine things I've gotta start
small.
