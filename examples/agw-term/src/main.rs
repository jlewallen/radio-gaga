use anyhow::Result;
use clap::Parser;
use futures_util::FutureExt;
use rustyline_async::{Readline, ReadlineEvent};
use std::{io::Write, time::Duration};
use tokio::net::TcpStream;
use tokio::time::timeout;
use tokio_util::codec::Framed;
use tracing::*;
use tracing_appender::rolling::{RollingFileAppender, Rotation};
use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt};

use agw::{AgwCodec, AgwProtocol, Callsign, PacketKind};

#[derive(Parser)]
struct Options {
    #[arg(long)]
    server: String,
    #[arg(short, long)]
    our_call: String,
    #[arg(short, long)]
    their_call: String,
}

pub struct Session {
    source: Callsign,
    destination: Callsign,
    protocol: AgwProtocol<Framed<TcpStream, AgwCodec>>,
    buffer: Vec<u8>,
}

impl Session {
    pub async fn new(
        address: &str,
        source: impl Into<Callsign>,
        destination: impl Into<Callsign>,
    ) -> Result<Self> {
        let source = source.into();
        let destination = destination.into();

        let mut protocol = AgwProtocol::new_framed(TcpStream::connect(address).await?);

        protocol.enable_monitoring().await?;

        protocol.register_callsign(source.clone()).await?;

        protocol
            .connection(source.clone(), destination.clone())
            .await?;

        Ok(Session {
            source,
            destination,
            protocol,
            buffer: Vec::new(),
        })
    }

    pub async fn service(&mut self) -> Result<()> {
        if let Some(received) = self.protocol.receive_packet().await? {
            match received.kind() {
                PacketKind::Data => {
                    self.buffer.extend(received.data().bytes());
                }
                _ => {}
            }
        }

        Ok(())
    }

    pub async fn write(&mut self, data: &[u8]) -> Result<()> {
        Ok(self
            .protocol
            .data(self.source.clone(), self.destination.clone(), data)
            .await?)
    }

    pub async fn stop(&mut self) -> Result<()> {
        Ok(self
            .protocol
            .disconnection(self.source.clone(), self.destination.clone())
            .await?)
    }

    pub fn read(&mut self) -> Vec<u8> {
        self.buffer.drain(0..).collect()
    }
}

#[tokio::main]
async fn main() -> Result<()> {
    let options = Options::parse();

    let (mut rl, mut stdout) = Readline::new("> ".to_owned())?;

    let file_appender = RollingFileAppender::new(Rotation::NEVER, ".", "agw-term.log");
    let (non_blocking, _guard) = tracing_appender::non_blocking(file_appender);

    tracing_subscriber::registry()
        .with(tracing_subscriber::EnvFilter::new(get_rust_log()))
        .with(tracing_subscriber::fmt::layer().with_writer({
            let logging_to = stdout.clone();
            move || logging_to.clone()
        }))
        .with(tracing_subscriber::fmt::layer().with_writer(non_blocking))
        .init();

    info!(
        "connection between {:?} and {:?}",
        options.our_call, options.their_call
    );

    let mut stopped = false;
    let mut session = Session::new(&options.server, options.our_call, options.their_call).await?;

    while !stopped {
        tokio::select! {
            tick = timeout(Duration::from_millis(100), session.service()) => {
                match tick {
                    Ok(_) => {},
                    Err(_) => {},
                }
            }
            command = rl.readline().fuse() => match command {
                Ok(ReadlineEvent::Line(line)) => {
                    if !line.is_empty() {
                        let data = (line + "\r").as_bytes().to_vec();
                        session.write(&data).await?;
                    }
                }
                Ok(ReadlineEvent::Eof) => {
                    info!("eof");
                    session.stop().await?;
                    stopped = true;
                },
                Ok(ReadlineEvent::Interrupted) => {
                    info!("interrupted");
                    break
                },
                Err(err) => {
                    error!("error: {:?}", err);
                    break
                },
            }
        }

        let incoming = session.read();
        if !incoming.is_empty() {
            trace!("read {} bytes", incoming.len());
            let text = String::from_utf8(incoming)?;
            let text = text.replace('\r', "\n");
            write!(stdout, "{}", text)?;
        }
    }

    rl.flush()?;

    Ok(())
}

fn get_rust_log() -> String {
    let mut original = std::env::var("RUST_LOG").unwrap_or_else(|_| "agw-term=debug".into());

    /*
    if !original.contains("agw::transport=") {
        original.push_str(",agw::transport=info");
    }
    */

    if !original.contains("rustyline=") {
        original.push_str(",rustyline=info");
    }

    if !original.contains("mio=") {
        original.push_str(",mio=info");
    }

    if !original.contains("tokio_util=") {
        original.push_str(",tokio_util=info");
    }

    original
}
