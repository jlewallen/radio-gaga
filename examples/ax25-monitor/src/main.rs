use anyhow::Result;
use clap::Parser;
use std::time::Duration;
use tracing::*;
use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt};

use ax2five::{Pretty, Transport};
use common::TransportOptions;

#[derive(Parser)]
pub struct Options {
    #[arg(long)]
    agw: Option<String>,
    #[arg(long)]
    tnc: Option<String>,
}

impl Options {
    fn transport_options(&self) -> TransportOptions {
        TransportOptions::new()
            .agw(self.agw.clone())
            .tnc(self.tnc.clone())
    }
}

#[tokio::main]
pub async fn main() -> Result<()> {
    let options = Options::parse();

    tracing_subscriber::registry()
        .with(tracing_subscriber::EnvFilter::new(get_rust_log()))
        .with(tracing_subscriber::fmt::layer())
        .init();

    let mut transport = options.transport_options().build().await?;

    loop {
        tokio::select! {
            received = tokio::time::timeout(Duration::from_secs(1), transport.receive()) => {
                if let Ok(received) = received {
                    match received {
                        Ok(frame) => {
                            if let Some(frame) = frame {
                                info!("{:?}", Pretty::new(&frame));
                            }
                        }
                        Err(e) => {
                            error!("upstream error {:?}", e);
                            break;
                        }
                    }
                }
            }
        }
    }

    Ok(())
}

fn get_rust_log() -> String {
    let mut original = std::env::var("RUST_LOG").unwrap_or_else(|_| "ax25_monitor=debug".into());

    if !original.contains("agw::transport=") {
        original.push_str(",agw::transport=info");
    }

    if !original.contains("mio=") {
        original.push_str(",mio=info");
    }

    if !original.contains("tokio_util=") {
        original.push_str(",tokio_util=info");
    }

    if !original.contains("hyper=") {
        original.push_str(",hyper=info");
    }

    if !original.contains("parse_headers=") {
        original.push_str(",parse_headers=info");
    }

    original
}
