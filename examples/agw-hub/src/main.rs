use anyhow::Result;
use clap::Parser;
use futures_util::{SinkExt, StreamExt};
use tokio::net::TcpListener;
use tokio_util::codec::Framed;
use tracing::*;
use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt};

use agw::{AgwCodec, Packet};

use common::Hub;

#[derive(Parser, Clone)]
pub struct Options {
    #[arg(long, default_value = "0.0.0.0:8010")]
    listen: String,
    #[arg(long)]
    loopback: bool,
}

#[tokio::main]
pub async fn main() -> Result<()> {
    let options = Options::parse();

    tracing_subscriber::registry()
        .with(tracing_subscriber::EnvFilter::new(get_rust_log()))
        .with(tracing_subscriber::fmt::layer())
        .init();

    let listener = TcpListener::bind(&options.listen).await?;

    info!("listening on {}", options.listen);

    let hub = Hub::default();

    loop {
        let (serving, remote) = listener.accept().await?;
        let mut serving = Framed::new(serving, AgwCodec::new());

        let (sender, mut receiver) = tokio::sync::mpsc::channel::<Packet>(10);

        info!("{} serving", remote);

        hub.reset_chaos().await;

        hub.register(&remote, sender).await;

        tokio::spawn({
            let hub = hub.clone();

            async move {
                loop {
                    tokio::select! {
                        sending = receiver.recv() => {
                            if let Some(sending) = sending {
                                // trace!("{} sending", &remote);
                                match serving.send(sending.to_application()).await {
                                    Ok(_) => {}
                                    Err(e) => warn!("{:?}", e),
                                }
                            }
                        }
                        received = serving.next() => {
                            match received {
                                Some(Ok(received)) => {
                                    match hub.route(&remote, received).await {
                                        Ok(replies) => {
                                            for reply in replies.into_iter() {
                                                // trace!("{} reply {:?}", &remote, reply);
                                                match serving.send(reply).await {
                                                    Ok(_) => {}
                                                    Err(e) => warn!("{:?}", e),
                                                }
                                            }
                                        },
                                        Err(e) => warn!("{:?}", e),
                                    }
                                }
                                None => {
                                    info!("{} disconnected", &remote);
                                    hub.unregister(&remote).await;
                                    break;
                                }
                                value => warn!("{} {:?}", &remote, value),

                            }
                        }
                    }
                }
            }
        });
    }
}

fn get_rust_log() -> String {
    let mut original = std::env::var("RUST_LOG").unwrap_or_else(|_| "agw_hub=debug".into());

    if !original.contains("agw::transport=") {
        original.push_str(",agw::transport=info");
    }

    if !original.contains("mio=") {
        original.push_str(",mio=info");
    }

    if !original.contains("tokio_util=") {
        original.push_str(",tokio_util=info");
    }

    original
}
