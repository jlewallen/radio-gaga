use anyhow::Result;
use clap::Parser;
use switchboard::Router;
use tracing::*;
use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt};

mod influx;
mod proxy;
mod switchboard;

use proxy::Proxy;

#[derive(Parser)]
pub struct Options {
    #[arg(long)]
    agw: String,
    #[arg(long)]
    listen: String,
}

#[tokio::main]
pub async fn main() -> Result<()> {
    let options = Options::parse();

    tracing_subscriber::registry()
        .with(tracing_subscriber::EnvFilter::new(get_rust_log()))
        .with(tracing_subscriber::fmt::layer().without_time())
        .init();

    let proxy = Proxy::new(&options.listen, options.agw.parse()?);

    tokio::select! {
        _ = tokio::signal::ctrl_c() => {
            info!("ctrl-c");
        }
        res = proxy.service(Router::default()) => {
            match res {
                Ok(_) => {},
                Err(e) => warn!("{:?}", e),
            }
        }
    }

    Ok(())
}

fn get_rust_log() -> String {
    let mut original = std::env::var("RUST_LOG").unwrap_or_else(|_| "debug".into());

    if !original.contains("mio=") {
        original.push_str(",mio=info");
    }

    if !original.contains("tokio_util=") {
        original.push_str(",tokio_util=info");
    }

    if !original.contains("hyper=") {
        original.push_str(",hyper=info");
    }

    if !original.contains("parse_headers=") {
        original.push_str(",parse_headers=info");
    }

    original
}
