#![allow(dead_code)]

use agw::{Packet, PacketKind};
use anyhow::Result;
use ax2five::{Ax25Frame, FrameContent};
use futures_util::stream;
use influxdb2::{models::DataPoint, Client};
use tracing::*;

#[derive(Default)]
pub struct InfluxDb {
    db: Option<Client>,
}

impl InfluxDb {
    pub fn new() -> Self {
        let Some(host) = std::env::var("INFLUXDB_HOST").ok() else {
            return Self::default();
        };
        let Some(org) = std::env::var("INFLUXDB_ORG").ok() else {
            return Self::default();
        };
        let Some(token) = std::env::var("INFLUXDB_TOKEN").ok() else {
            return Self::default();
        };

        info!(target: "influxdb", "have configuration");

        Self {
            db: Some(Client::new(host, org, token)),
        }
    }

    pub async fn write(&self, frame: &Packet) -> Result<()> {
        match frame.kind() {
            PacketKind::DataRaw => {
                let frame = Ax25Frame::from_bytes(frame.data().bytes())?;
                self.write_frame(&frame).await?;
                Ok(())
            }
            _ => Ok(()),
        }
    }

    pub async fn write_frame(&self, frame: &Ax25Frame) -> Result<()> {
        if let Some(db) = self.db.as_ref() {
            let bytes = frame.to_bytes().len();

            let dp = DataPoint::builder("ax25")
                .tag("station", "home")
                .tag("source", frame.source.to_string())
                .tag("destination", frame.destination.to_string())
                .tag("freq", "145.050MHz")
                .field("frame_bytes", bytes as i64);

            let dp = match &frame.content {
                FrameContent::Information(c) => dp
                    .field("information", c.info.len() as i64)
                    .tag("pid", format!("{:?}", c.pid)),
                FrameContent::UnnumberedInformation(c) => dp
                    .field("information", c.info.len() as i64)
                    .tag("pid", format!("{:?}", c.pid)),
                _ => dp,
            };

            let fc = match &frame.content {
                FrameContent::Information(_) => "I",
                FrameContent::ReceiveReady(_) => "RR",
                FrameContent::ReceiveNotReady(_) => "RNR",
                FrameContent::Reject(_) => "REJ",
                FrameContent::SelectiveReject(_) => "SREJ",
                FrameContent::SetAsynchronousBalancedMode(_) => "SABM",
                FrameContent::SetAsynchronousBalancedModeExtended(_) => "SABME",
                FrameContent::Disconnect(_) => "D",
                FrameContent::DisconnectedMode(_) => "DM",
                FrameContent::UnnumberedAcknowledge(_) => "UA",
                FrameContent::FrameReject(_) => "FREJ",
                FrameContent::UnnumberedInformation(_) => "UI",
                FrameContent::UnknownContent(_) => "UC",
                FrameContent::ExchangeIdentification(_) => "XID",
                FrameContent::Test(_) => "TEST",
            };

            let dp = dp.tag("frame_kind", fc).build()?;

            db.write("home", stream::iter(vec![dp])).await?;
        }

        Ok(())
    }
}
