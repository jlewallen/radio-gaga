#![allow(dead_code)]

use agw::{AgwClient, Packet, ReceivedData};
use std::time::Duration;
use tokio::{sync::mpsc::Sender, task::JoinHandle};
use tracing::*;

use crate::proxy::{Error, Middleware, Pending, Traffic};

#[derive(Debug, Clone)]
pub struct MapCalls {
    map: (String, String),
}

impl MapCalls {
    pub fn new(lhs: impl Into<String>, rhs: impl Into<String>) -> Self {
        Self {
            map: (lhs.into(), rhs.into()),
        }
    }

    pub fn apply(&self, packet: Traffic) -> Packet {
        match packet {
            Traffic::Up(packet) => {
                if packet
                    .to()
                    .map(|s| s.as_str() == self.map.0)
                    .unwrap_or_default()
                {
                    packet.with_call_to(self.map.1.clone())
                } else {
                    packet
                }
            }
            Traffic::Down(packet) => {
                if packet
                    .from()
                    .map(|s| s.as_str() == self.map.0)
                    .unwrap_or_default()
                {
                    packet.with_call_from(self.map.0.clone())
                } else {
                    packet
                }
            }
        }
    }
}

#[derive(Default)]
pub struct Router {
    pending: Vec<JoinHandle<()>>,
}

impl Clone for Router {
    fn clone(&self) -> Self {
        Self {
            pending: Default::default(),
        }
    }
}

impl Middleware for Router {
    async fn traffic(
        &mut self,
        pending: Pending,
        tx: Sender<Packet>,
        traffic: Traffic,
    ) -> Result<Option<Packet>, Error> {
        match traffic.packet().as_connection() {
            Some(packet) => {
                if packet.to().map(|s| s.as_str() == "T").unwrap_or_default() {
                    let tunnel = Tunnel::default();
                    let handle = tokio::spawn({
                        let span = span!(Level::DEBUG, "tunnel");
                        let packet = packet.clone();

                        async move {
                            match tunnel.service(traffic).await {
                                Ok(_) => {}
                                Err(e) => warn!("Tunnel: {:?}", e),
                            }

                            match tx
                                .send(Packet::new_disconnection(
                                    packet.port(),
                                    packet.to().unwrap().clone(),
                                    packet.from().unwrap().clone(),
                                ))
                                .await
                            {
                                Ok(_) => {}
                                Err(e) => warn!("{:?}", e),
                            };

                            ()
                        }
                        .instrument(span)
                    });

                    pending.cleanup_when_finished(handle).await;

                    Ok(None)
                } else {
                    Ok(Some(traffic.into()))
                }
            }
            None => Ok(Some(traffic.into())),
        }
    }
}

impl Drop for Router {
    fn drop(&mut self) {
        debug!("dropped!");
    }
}

#[derive(Default)]
struct Tunnel;

impl Tunnel {
    async fn service(self, traffic: Traffic) -> Result<(), Error> {
        let Some(from) = traffic.from() else {
            return Ok(());
        };

        let mut conn = AgwClient::new("127.0.0.1:7100").await?;

        let mut session = conn.start(2, from.clone(), "HECT-1").await?;

        if session.connected(Duration::from_secs(5)).await? {
            info!("connected!");

            let data = session
                .service(ReceivedData::default(), Duration::from_secs(5))
                .await?;

            debug!("{:?}", data);
        }

        session.stop().await?;

        conn.close().await?;

        Ok(())
    }
}
