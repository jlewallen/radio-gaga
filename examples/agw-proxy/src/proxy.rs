use anyhow::Result;
use futures_util::{SinkExt, StreamExt};
use std::{net::SocketAddr, sync::Arc, time::Duration};
use thiserror::Error;
use tokio::{
    net::{TcpListener, TcpStream},
    sync::{mpsc::Sender, Mutex},
    task::JoinHandle,
};
use tokio_util::{
    bytes::BytesMut,
    codec::{Decoder, Encoder, Framed},
};
use tracing::*;

use agw::{AgwCodec, AgwError, Header, Packet, PacketKind, ProtocolIdentifier};
use ax2five::{Ax25Frame, Callsign};

struct Session<M> {
    remote: SocketAddr,
    upstream: SocketAddr,
    downstream: Framed<TcpStream, AgwCodec>,
    middleware: M,
}

impl<M> Session<M>
where
    M: Middleware,
{
    async fn service(mut self) -> Result<(), Error> {
        info!("started {:?}", &self.remote);

        let (tx, mut rx) = tokio::sync::mpsc::channel::<Packet>(20);
        let upstream = TcpStream::connect(&self.upstream).await?;
        let mut upstream = Framed::new(upstream, AgwCodec::new());
        let pending = Pending::default();

        info!("connected {:?}", &self.upstream);

        loop {
            tokio::select! {
                frame = rx.recv() => {
                    match frame {
                        Some(frame) => {
                            info!("<< {:?}", frame);
                            match self.downstream.send(frame).await {
                                Ok(_) => {},
                                Err(e) => warn!("{:?}", e),
                            }
                        }
                        None => {},
                    }
                }
                frame = self.downstream.next() => {
                    match frame {
                        Some(Ok(frame)) => {
                            info!("-> {:?}", frame);
                            if let Some(frame) = self.middleware.traffic(pending.clone(), tx.clone(), Traffic::Up(frame)).await? {
                                match upstream.send(frame).await {
                                    Ok(_) => {},
                                    Err(e) => warn!("{:?}", e),
                                }
                            }
                        },
                        Some(Err(e)) => {
                            warn!("{:?}", e);
                        }
                        None => {
                            info!("-> downstream disconnected");
                            break;
                        },
                    }
                }
                frame = upstream.next() => {
                    match frame {
                        Some(Ok(frame)) => {
                            if let Some(frame) = self.middleware.traffic(pending.clone(), tx.clone(), Traffic::Down(frame)).await? {
                                info!("<- {:?}", frame);
                                match self.downstream.send(frame).await {
                                    Ok(_) => {},
                                    Err(e) => warn!("{:?}", e),
                                }
                            }
                        },
                        Some(Err(e)) => {
                            warn!("{:?}", e);
                        }
                        None => {
                            info!("-> upstream disconnected");
                            break;
                        },
                    }
                }
                _ = pending.service() => {
                }
            }
        }

        Ok(())
    }
}

pub struct Proxy {
    bind: String,
    upstream: SocketAddr,
}

impl Proxy {
    pub fn new(bind: &str, upstream: SocketAddr) -> Self {
        Self {
            bind: bind.to_string(),
            upstream,
        }
    }

    pub async fn service<M>(self, middleware: M) -> Result<(), Error>
    where
        M: Middleware + Clone + Send + 'static,
    {
        info!("listening on {:?}", self.bind);

        let listener = TcpListener::bind(&self.bind).await?;
        let pending = Pending::default();

        loop {
            match tokio::time::timeout(Duration::from_millis(100), listener.accept()).await {
                Ok(Ok((tcp, remote))) => {
                    let middleware = middleware.clone();
                    let handle = tokio::spawn({
                        let downstream = Framed::new(tcp, AgwCodec::default());

                        let session = Session {
                            remote,
                            middleware,
                            upstream: self.upstream,
                            downstream,
                        };

                        let span = span!(Level::DEBUG, "session", r = ?remote.port());

                        async move {
                            match session.service().await {
                                Ok(_) => info!("finished"),
                                Err(e) => warn!("error: {:?}", e),
                            }
                        }
                        .instrument(span)
                    });

                    pending.cleanup_when_finished(handle).await;
                }
                Ok(Err(e)) => warn!("error: {:?}", e),
                Err(_) => {}
            }

            pending.service().await;
        }
    }
}

#[derive(Debug)]
pub enum Traffic {
    Up(Packet),
    Down(Packet),
}

#[allow(dead_code)]
impl Traffic {
    pub fn packet(&self) -> &Packet {
        match self {
            Traffic::Down(packet) => packet,
            Traffic::Up(packet) => packet,
        }
    }

    pub fn from(&self) -> Option<&Callsign> {
        self.packet().from()
    }

    pub fn to(&self) -> Option<&Callsign> {
        self.packet().from()
    }

    pub fn up(&self) -> Option<&Packet> {
        match self {
            Traffic::Up(packet) => Some(packet),
            _ => None,
        }
    }

    pub fn down(&self) -> Option<&Packet> {
        match self {
            Traffic::Down(packet) => Some(packet),
            _ => None,
        }
    }

    pub fn port(&self) -> u32 {
        self.packet().port()
    }
}

impl Into<Packet> for Traffic {
    fn into(self) -> Packet {
        match self {
            Traffic::Up(packet) => packet,
            Traffic::Down(packet) => packet,
        }
    }
}

#[trait_variant::make(Middleware: Send)]
#[allow(dead_code)]
pub trait LocalMiddleware {
    async fn traffic(
        &mut self,
        pending: Pending,
        tx: Sender<Packet>,
        traffic: Traffic,
    ) -> Result<Option<Packet>, Error>;
}

#[derive(Default, Clone)]
pub struct Pending {
    pending: Arc<Mutex<Vec<JoinHandle<()>>>>,
}

impl Pending {
    pub async fn cleanup_when_finished(&self, handle: JoinHandle<()>) {
        let mut pending = self.pending.lock().await;
        pending.push(handle);
    }

    async fn service(&self) {
        let mut pending = self.pending.lock().await;

        if pending.iter().any(|h| h.is_finished()) {
            // This is a little messier than I'd like.
            let mut keeping = Vec::new();
            for handle in pending.drain(0..) {
                if handle.is_finished() {
                    match handle.await {
                        Ok(_) => info!("finished"),
                        Err(e) => warn!("{:?}", e),
                    }
                } else {
                    keeping.push(handle);
                }
            }
            pending.extend(keeping.drain(0..));
        }
    }
}

#[derive(Default, Clone)]
pub struct Noop {}

impl Middleware for Noop {
    async fn traffic(
        &mut self,
        _pending: Pending,
        _tx: Sender<Packet>,
        traffic: Traffic,
    ) -> Result<Option<Packet>, Error> {
        Ok(Some(traffic.into()))
    }
}

#[derive(Debug, Error)]
pub enum Error {
    #[error("IO: {0}")]
    Io(#[from] std::io::Error),
    #[error("Send")]
    Send,
    #[error("Agw: {0}")]
    Agw(#[from] agw::Error),
}

impl<T> From<tokio::sync::mpsc::error::SendError<T>> for Error {
    fn from(_value: tokio::sync::mpsc::error::SendError<T>) -> Self {
        Self::Send
    }
}

#[derive(Default)]
pub struct AgwRawAx25 {
    agw: AgwCodec,
    nulls_to_prepend: usize,
}

impl AgwRawAx25 {
    // According to the from_bytes documentation this is something that the
    // Linux Ax25 stack will do. I have no idea why. What's annoying is that
    // BPQ seems to be sensitive to this, had has a very strange check in
    // L2Code.c of the form:
    //
    // // Check for invalid length (< 22 7Header + 7Addr + 7Addr + CTL
    // if (Buffer->LENGTH < (18 + sizeof(void *)))
    // {
    //   Debugprintf("BPQ32 Bad L2 Msg Port %d Len %d", PORT->PORTNUMBER, Buffer->LENGTH);
    //   ReleaseBuffer(Buffer);
    //   return;
    // }
    //
    // I've got my eye on 'CTL' up there and, well. Here we are. This is
    // merely here to let future developers have a way of knowing this is
    // happening. When our ax25 frame parser sees these, it'll drop them.
    #[allow(dead_code)]
    pub fn new_prepend_nulls() -> Self {
        Self {
            agw: Default::default(),
            nulls_to_prepend: 1,
        }
    }
}

impl Decoder for AgwRawAx25 {
    type Item = Ax25Frame;

    type Error = AgwError;

    fn decode(&mut self, src: &mut BytesMut) -> Result<Option<Self::Item>, Self::Error> {
        if let Some(packet) = self.agw.decode(src)? {
            match packet.kind() {
                PacketKind::DataRaw => match Ax25Frame::from_bytes(packet.data().bytes()) {
                    Ok(frame) => {
                        trace!("decoded {} bytes", packet.data().bytes().len());
                        Ok(Some(frame))
                    }
                    Err(e) => {
                        warn!("{:?}", e);
                        Ok(None)
                    }
                },
                _ => Ok(None),
            }
        } else {
            Ok(None)
        }
    }
}

impl Encoder<Ax25Frame> for AgwRawAx25 {
    type Error = std::io::Error;

    fn encode(&mut self, item: Ax25Frame, buf: &mut BytesMut) -> Result<(), Self::Error> {
        let from: Callsign = item.source.to_string().into();
        let to: Callsign = item.destination.to_string().into();
        let mut data = Vec::new();
        data.extend([0..self.nulls_to_prepend].map(|_| 0u8));
        data.extend(item.to_bytes());
        let packet = Packet::new(
            Header {
                port: 0,
                kind: PacketKind::DataRaw,
                pid: ProtocolIdentifier::None,
                from,
                to,
                len: data.len(),
            },
            data,
        );

        self.agw.encode(packet, buf)
    }
}
