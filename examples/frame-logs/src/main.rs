use agw::{Packet, PacketKind};
use ax2five::{Ax25Frame, FrameContent};
use clap::Parser;
use std::{
    fs::File,
    io::{BufRead, BufReader},
    path::PathBuf,
};

#[derive(Parser)]
pub struct Options {
    #[arg(short, long)]
    path: PathBuf,
}

fn main() -> anyhow::Result<()> {
    let options = Options::parse();

    let file = File::open(&options.path)?;

    let lines = BufReader::new(file).lines().map_while(Result::ok);

    let lines = lines
        .map(|l| l.trim().to_owned())
        .enumerate()
        .filter(|(_, l)| !l.is_empty())
        .flat_map(|(row, l)| {
            let parts = l.split(' ').map(|v| v.to_string()).collect::<Vec<_>>();
            match parts.as_slice() {
                [time, dir, data] => Some((row, Some(time.clone()), dir.clone(), data.clone())),
                [dir, data] => Some((row, None, dir.clone(), data.clone())),
                _ => None,
            }
        })
        .map(|(row, time, d, s)| {
            (
                row + 1, // Line numbers start at 1.
                time,
                d,
                s.split(':')
                    .map(|s| u8::from_str_radix(s, 16).unwrap())
                    .collect::<Vec<_>>(),
            )
        });

    for (_line, _time, _dir, data) in lines {
        if let Ok(agw) = Packet::decode(&data) {
            match agw.kind() {
                PacketKind::DataRaw => {
                    if let Ok(Some(frame)) = agw.get_ax25() {
                        match frame.content {
                            FrameContent::ExchangeIdentification(xid) => println!("{:#?}", xid),
                            // FrameContent::UnknownContent(uc) => println!("{:#?}", uc),
                            _ => {}
                        }
                    } else {
                        println!("Non-Ax25 K {:?}", agw);
                    }
                }
                _ => {}
            }
        } else {
            match Ax25Frame::from_bytes(&data) {
                Ok(frame) => {
                    println!("{:?}", frame);
                    println!("{:?}", data);
                    println!("{:?}", frame.to_bytes());
                    println!();
                }
                Err(e) => println!("{:?}", e),
            }
        }
    }

    Ok(())
}
