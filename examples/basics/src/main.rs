#![allow(unused_variables)]

use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt};

use ax2five::Scheduler;

fn main() -> anyhow::Result<()> {
    tracing_subscriber::registry()
        .with(tracing_subscriber::EnvFilter::new(get_rust_log()))
        .with(tracing_subscriber::fmt::layer())
        .init();

    let mut scheduler = Scheduler::default();
    let a = scheduler.add("HECTOR", "DESI");
    let b = scheduler.connect("DESI", "HECTOR");

    scheduler.loopback(100.into())?;

    scheduler.write(a, vec![b'j'; 8])?;

    scheduler.loopback(100.into())?;

    scheduler.write(b, vec![b'j'; 8])?;

    scheduler.loopback(100.into())?;

    Ok(())
}

fn get_rust_log() -> String {
    let mut original = std::env::var("RUST_LOG").unwrap_or_else(|_| "trace".into());

    if !original.contains("ax2five::states=") {
        original.push_str(",ax2five::states=info");
    }

    if !original.contains("mio=") {
        original.push_str(",mio=info");
    }

    if !original.contains("tokio_util=") {
        original.push_str(",tokio_util=info");
    }

    if !original.contains("hyper=") {
        original.push_str(",hyper=info");
    }

    if !original.contains("parse_headers=") {
        original.push_str(",parse_headers=info");
    }

    original
}
