use anyhow::Result;
use clap::Parser;
use tokio::net::TcpStream;
use tracing::*;
use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt};

use agw::AgwProtocol;
use ax2five::{AllowAll, FramesOnlyAdapter, Scheduler, Service};

#[derive(Parser)]
pub struct Options {
    #[arg(short, long)]
    address: String,
    #[arg(short, long)]
    our_call: String,
    #[arg(short, long)]
    their_call: String,
}

#[tokio::main]
pub async fn main() -> Result<()> {
    let options = Options::parse();

    tracing_subscriber::registry()
        .with(tracing_subscriber::EnvFilter::new(get_rust_log()))
        .with(tracing_subscriber::fmt::layer().with_thread_ids(false))
        .init();

    let stream = TcpStream::connect(&options.address).await?;
    let mut conn = AgwProtocol::new_framed(stream);
    conn.enable_raw().await?;

    let mut sends = 0;
    let mut stopped = false;
    let mut scheduler = Scheduler::default();
    let mut adapter = FramesOnlyAdapter::new(AllowAll::default());
    let socket = scheduler.connect(options.our_call.clone(), options.their_call.clone());

    while !stopped || scheduler.busy() {
        tokio::select! {
            _ = tokio::signal::ctrl_c() => {
                println!();
                info!("ctrl-c");
                scheduler.stop()?;
                stopped = true;
            }
            tick = scheduler.service(&mut conn, &mut adapter) => {
                match tick {
                    Ok(_) => {},
                    Err(e) => warn!("{:?}", e),
                }
            }
        }

        let incoming = scheduler.read(socket)?;
        if !incoming.is_empty() && sends == 0 {
            println!("\n{}\n", String::from_utf8(incoming)?);

            sends += 1;
        }
    }

    Ok(())
}

fn get_rust_log() -> String {
    let mut original = std::env::var("RUST_LOG").unwrap_or_else(|_| "sabm_send=debug".into());

    if !original.contains("agw::transport=") {
        original.push_str(",agw::transport=info");
    }

    if !original.contains("mio=") {
        original.push_str(",mio=info");
    }

    if !original.contains("tokio_util=") {
        original.push_str(",tokio_util=info");
    }

    original
}
