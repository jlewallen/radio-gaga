use agw::{AckAfterOutgoingQueueEmpty, AgwCodec, AgwProtocol};
use anyhow::Result;
use clap::Parser;
use futures_util::FutureExt;
use rustyline_async::{Readline, ReadlineEvent};
use std::io::Write;
use tokio::net::TcpStream;
use tokio_util::codec::Framed;
use tracing::*;
use tracing_appender::rolling::{RollingFileAppender, Rotation};
use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt};

use ax2five::{
    FramesOnlyAdapter, Parameters, Scheduler, Service, SingleDestination, SocketEvent, TncTransport,
};
use common::{CommonTransport, TransportOptions};

#[derive(Parser)]
struct Options {
    #[arg(long)]
    agw: Option<String>,
    #[arg(long)]
    tnc: Option<String>,
    #[arg(short, long)]
    our_call: String,
    #[arg(short, long)]
    their_call: String,
    #[arg(long)]
    disable_agw_ack: bool,
}

impl Options {
    fn transport_options(&self) -> TransportOptions {
        TransportOptions::new()
            .agw(self.agw.clone())
            .tnc(self.tnc.clone())
    }
}

struct AgwComms {
    transport: AgwProtocol<Framed<TcpStream, AgwCodec>>,
    // adapter: RawAgwAdapter<SingleDestination>,
    adapter: AckAfterOutgoingQueueEmpty<SingleDestination>,
}

struct TncComms {
    transport: TncTransport,
    adapter: FramesOnlyAdapter<SingleDestination>,
}

enum Comms {
    Agw(AgwComms),
    Tnc(TncComms),
}

impl Comms {
    pub fn new(transport: CommonTransport, options: &Options) -> Result<Self> {
        let filter = SingleDestination::new(&options.our_call)?;
        match transport {
            CommonTransport::Agw(transport) => {
                // let adapter = RawAgwAdapter::new(filter);
                let adapter = AckAfterOutgoingQueueEmpty::new(filter);
                Ok(Comms::Agw(AgwComms { transport, adapter }))
            }
            CommonTransport::Tnc(transport) => {
                let adapter = FramesOnlyAdapter::new(filter);
                Ok(Comms::Tnc(TncComms { transport, adapter }))
            }
        }
    }

    async fn service(&mut self, scheduler: &mut Scheduler) -> Result<Vec<SocketEvent>> {
        match self {
            Comms::Agw(c) => Ok(scheduler.service(&mut c.transport, &mut c.adapter).await?),
            Comms::Tnc(c) => Ok(scheduler.service(&mut c.transport, &mut c.adapter).await?),
        }
    }
}

#[tokio::main]
async fn main() -> Result<()> {
    let options = Options::parse();

    let (mut rl, mut stdout) = Readline::new("> ".to_owned())?;

    let file_appender = RollingFileAppender::new(Rotation::NEVER, ".", "gaterm.log");
    let (non_blocking, _guard) = tracing_appender::non_blocking(file_appender);

    tracing_subscriber::registry()
        .with(tracing_subscriber::EnvFilter::new(get_rust_log()))
        .with(tracing_subscriber::fmt::layer().with_writer({
            let logging_to = stdout.clone();
            move || logging_to.clone()
        }))
        .with(tracing_subscriber::fmt::layer().with_writer(non_blocking))
        .init();

    let transport = match options.transport_options().build().await {
        Ok(tranport) => tranport,
        Err(common::Error::AmbiguousTransport) | Err(common::Error::MissingTransport) => {
            use clap::CommandFactory;
            let mut cmd = Options::command();
            cmd.print_help()?;
            return Ok(());
        }
        Err(e) => {
            return Err(e.into());
        }
    };

    info!(
        "connection between {:?} and {:?}",
        options.our_call, options.their_call
    );

    let mut stopped = false;
    let mut comms = Comms::new(transport, &options)?;
    let mut scheduler = Scheduler::new(Parameters {
        timer_3_value: 300_000.into(),
        immediate_ack_on_information_poll: false,
        ..Default::default()
    });
    let socket = scheduler.connect(options.our_call.clone(), options.their_call.clone());

    while !stopped || scheduler.busy() {
        tokio::select! {
            tick = comms.service(&mut scheduler) => {
                match tick {
                    Ok(_) => {},
                    Err(e) => warn!("{:?}", e),
                }
            }
            command = rl.readline().fuse() => match command {
                Ok(ReadlineEvent::Line(line)) => {
                    if !line.is_empty() {
                        scheduler.write(socket, (line + "\r").as_bytes().to_vec())?;
                    }
                }
                Ok(ReadlineEvent::Eof) => {
                    info!("eof");
                    scheduler.stop()?;
                    stopped = true;
                },
                Ok(ReadlineEvent::Interrupted) => {
                    info!("interrupted");
                    break
                },
                Err(err) => {
                    error!("error: {:?}", err);
                    break
                },
            }
        }

        let incoming = scheduler.read(socket)?;
        if !incoming.is_empty() {
            trace!("read {} bytes", incoming.len());
            let text = String::from_utf8(incoming)?;
            let text = text.replace('\r', "\n");
            write!(stdout, "{}", text)?;
        }
    }

    rl.flush()?;

    Ok(())
}

fn get_rust_log() -> String {
    let mut original = std::env::var("RUST_LOG").unwrap_or_else(|_| "gaterm=debug".into());

    if !original.contains("agw::transport=") {
        original.push_str(",agw::transport=info");
    }

    if !original.contains("rustyline=") {
        original.push_str(",rustyline=info");
    }

    if !original.contains("mio=") {
        original.push_str(",mio=info");
    }

    if !original.contains("tokio_util=") {
        original.push_str(",tokio_util=info");
    }

    original
}
